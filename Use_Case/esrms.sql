-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2019 at 11:35 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esrms`
--
CREATE DATABASE IF NOT EXISTS `esrms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `esrms`;

-- --------------------------------------------------------

--
-- Table structure for table `absences_presences`
--

DROP TABLE IF EXISTS `absences_presences`;
CREATE TABLE `absences_presences` (
  `abs_id` int(11) NOT NULL,
  `ssn_s` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(64) NOT NULL,
  `state` varchar(16) NOT NULL,
  `description` varchar(64) NOT NULL,
  `ssn_t` varchar(255) NOT NULL,
  `cid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `ssn` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(16) DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`ssn`, `name`, `surname`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('11111', 'Pietro Cilluffo', 'Cilluffo', 'via stradella cartiera grande,5', '3270105447', NULL, 'piero.cilluffo8@libero.it', '0bfd1601a7fa2ab9e5356c75e02f00a3da04f4bf680c7f3332de4faf13c1f323d4b5a52ce25054dc49896d141825bec89a02a2e2d877b491873d095f55c7453b', '146621'),
('1234', 'lala', 'lala', 'lala', '1', NULL, 'superpiero23@hotmail.it', 'fb07963dbe8cdf2293247afc4fc2a05bfef915555a7ce4af438da084f2e066f10d7847e4452120e8754749effab5d3348bde8fd841d8bfd056015f7e15b32058', '194595'),
('12345', 'cac', 'cac', 'cac', '1', NULL, 'superpiero23@hotmail.it', 'd7896c7a83058f998b9053b60ca45c6ef9c00538cf3a93ae9eacacfc3f95230ecbceced4b76b42d6c4fd8d7d003428fec358286878e8662c44d8e20c0f478143', '601263'),
('123456', 'Pietro Cilluffo', 'Cilluffo', 'via stradella cartiera grande,5', '3270105447', NULL, 'piero.cilluffo8@libero.it', '0489076076c45b2ced443e2da9bf49f6a79569fbb83c3cbf98a0977e97b0a551320a8020c49aaa4c6d55b492d84c24fedcc487773feca3b2cc477845aac2ad3d', '821429'),
('4AAAAAAAAAAAAAAA', 'Lucia', 'Guiso', 'Corso Cristoforo Colombo 12', NULL, NULL, 'adminA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('fefe', 'efe', 'fefe', 'fefe', 'ee', NULL, 'fefe@dwd.it', 'c152aafc5c1f36770da4b1c749887328a0a8b3bc174e73b978c99ce02e3a69cdcea0725cfab1580c742538e11ee2a80138c45d9a82f763420a9e54ad8bcb09e4', '509145'),
('g34g34ss', '34gg34g43', '4g34g34', '43g43g', '34g34', NULL, 'superpiero23@hotmail.it', '8b1a4fbdf50a91861ee33b61e951529a5495ccdfcba83492052fa51446eda89c6c9c445a91547abe500a4dd0704bce959d515eeec8d7f30d38b5cb8baa4d437b', '58088');

-- --------------------------------------------------------

--
-- Table structure for table `bridge_class_coordinator`
--

DROP TABLE IF EXISTS `bridge_class_coordinator`;
CREATE TABLE `bridge_class_coordinator` (
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bridge_class_coordinator`
--

INSERT INTO `bridge_class_coordinator` (`cid`, `ssn_t`) VALUES
('C1', '3AAAAAAAAAAAAAAA');

-- --------------------------------------------------------

--
-- Table structure for table `bridge_class_students`
--

DROP TABLE IF EXISTS `bridge_class_students`;
CREATE TABLE `bridge_class_students` (
  `cid` varchar(64) NOT NULL,
  `ssn_s` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bridge_class_students`
--

INSERT INTO `bridge_class_students` (`cid`, `ssn_s`) VALUES
('C1', '1AAAAAAAAAAAAAAA'),
('C1', '1CCCCCCCCCCCCCCC'),
('C1', '1DDDDDDDDDDDDDDD'),
('C1', '1EEEEEEEEEEEEEEE'),
('C2', '1BBBBBBBBBBBBBBB'),
('C3', '1FFFFFFFFFFFFFFF'),
('ClassTest', 'testtest');

-- --------------------------------------------------------

--
-- Table structure for table `bridge_class_teachers`
--

DROP TABLE IF EXISTS `bridge_class_teachers`;
CREATE TABLE `bridge_class_teachers` (
  `ssn_t` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bridge_class_teachers`
--

INSERT INTO `bridge_class_teachers` (`ssn_t`, `cid`, `subject`) VALUES
('3AAAAAAAAAAAAAAA', 'C1', 'matematica'),
('3AAAAAAAAAAAAAAA', 'C2', 'fisica'),
('3AAAAAAAAAAAAAAA', 'C3', 'apa'),
('3BBBBBBBBBBBBBBB', 'C2', 'filosofia'),
('3BBBBBBBBBBBBBBB', 'C1', 'italiano'),
('3CCCCCCCCCCCCCC', 'C1', 'storia'),
('3DDDDDDDDDDDDDD', 'C1', 'fisica'),
('3EEEEEEEEEEEEEE', 'C1', 'arte'),
('3FFFFFFFFFFFFFF', 'C1', 'diritto'),
('testtest', 'testcid', 'storia');

-- --------------------------------------------------------

--
-- Table structure for table `bridge_parents_students`
--

DROP TABLE IF EXISTS `bridge_parents_students`;
CREATE TABLE `bridge_parents_students` (
  `ssn_p` varchar(16) NOT NULL,
  `ssn_s` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bridge_parents_students`
--

INSERT INTO `bridge_parents_students` (`ssn_p`, `ssn_s`) VALUES
('2AAAAAAAAAAAAAAA', '1AAAAAAAAAAAAAAA'),
('2AAAAAAAAAAAAAAA', '1BBBBBBBBBBBBBBB'),
('2AAAAAAAAAAAAAAA', '1DDDDDDDDDDDDDDD');

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `cid` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`cid`, `name`) VALUES
('C1', '1A'),
('C2', '3B'),
('C3', '5C'),
('testcid', 'testclassname');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

DROP TABLE IF EXISTS `communications`;
CREATE TABLE `communications` (
  `cid` varchar(64) NOT NULL,
  `ssn_a` varchar(64) NOT NULL,
  `object` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `publication_date` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communications`
--

INSERT INTO `communications` (`cid`, `ssn_a`, `object`, `description`, `publication_date`) VALUES
('C1', '4AAAAAAAAAAAAAAA', 'Urgent', 'Happy Holiday', '2019-12-17'),
('C1', '4AAAAAAAAAAAAAAA', 'Urgent', 'Class exit early', '2019-12-17');

-- --------------------------------------------------------

--
-- Table structure for table `final_marks`
--

DROP TABLE IF EXISTS `final_marks`;
CREATE TABLE `final_marks` (
  `m_id` int(11) NOT NULL,
  `ssn_s` varchar(16) NOT NULL,
  `subject` varchar(32) NOT NULL,
  `mark` int(11) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `term` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `homeworks`
--

DROP TABLE IF EXISTS `homeworks`;
CREATE TABLE `homeworks` (
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `aid` int(11) NOT NULL,
  `aname` varchar(255) NOT NULL,
  `atype` varchar(255) NOT NULL,
  `atachment` blob NOT NULL,
  `description` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `deadline` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homeworks`
--

INSERT INTO `homeworks` (`date`, `aid`, `aname`, `atype`, `atachment`, `description`, `cid`, `subject`, `ssn_t`, `deadline`) VALUES
('2019-12-16 20:05:57', 1, 'integral.pdf', 'application/pdf', '', 'integral exercise 1 added!!!!', 'C1', 'matematica', '3AAAAAAAAAAAAAAA', '2019-12-19'),
('2019-12-16 20:09:17', 2, 'integral-2.docx', 'application/octet-stream', '', 'integral 2 exercises - please do your solution to the paper ', 'C1', 'matematica', '3AAAAAAAAAAAAAAA', '2019-12-23'),
('2019-12-17 12:56:52', 5, 'cl_sv.txt', 'text/plain', '', '', 'C1', 'matematica', '3AAAAAAAAAAAAAAA', '2019-12-18'),
('2019-12-17 13:01:02', 6, 'integral-2.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '', 'integral exercise do it on paper', 'C1', 'matematica', '3AAAAAAAAAAAAAAA', '2019-12-20');

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
CREATE TABLE `lessons` (
  `lid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(16) NOT NULL,
  `description` varchar(512) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `timestamp` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
CREATE TABLE `marks` (
  `m_id` int(11) NOT NULL,
  `ssn_s` varchar(16) NOT NULL,
  `subject` varchar(32) NOT NULL,
  `mark` decimal(4,2) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`m_id`, `ssn_s`, `subject`, `mark`, `ssn_t`, `date`, `time`, `description`) VALUES
(0, '1AAAAAAAAAAAAAAA', 'Natural science', '7.50', '3AAAAAAAAAAAAAAA', '2019-11-03', '14:22:34', ''),
(2, '1DDDDDDDDDDDDDDD', 'matematica', '5.00', '3AAAAAAAAAAAAAAA', '2019-11-29', '15:20:00', ''),
(3, '1DDDDDDDDDDDDDDD', 'matematica', '11.00', '3AAAAAAAAAAAAAAA', '2019-11-29', '15:21:00', ''),
(4, '1DDDDDDDDDDDDDDD', 'matematica', '11.00', '3AAAAAAAAAAAAAAA', '2019-11-29', '15:26:00', ''),
(5, '1DDDDDDDDDDDDDDD', 'matematica', '5.00', '3AAAAAAAAAAAAAAA', '2019-11-29', '13:57:00', 'dsdsa'),
(6, '1DDDDDDDDDDDDDDD', 'matematica', '3.00', '3AAAAAAAAAAAAAAA', '2019-11-29', '13:58:00', ''),
(7, '1BBBBBBBBBBBBBBB', 'fisica', '2.25', '3AAAAAAAAAAAAAAA', '2019-12-02', '10:36:00', 'bravissima');

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

DROP TABLE IF EXISTS `materials`;
CREATE TABLE `materials` (
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `aid` int(11) NOT NULL,
  `aname` varchar(255) NOT NULL,
  `atype` varchar(255) NOT NULL,
  `atachment` blob NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`date`, `aid`, `aname`, `atype`, `atachment`, `cid`, `subject`, `ssn_t`) VALUES
('2019-12-17 13:01:22', 5, 'topics.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '', 'C1', 'matematica', '3AAAAAAAAAAAAAAA');

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
CREATE TABLE `meetings` (
  `metID` int(11) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(64) NOT NULL,
  `hour` varchar(6) NOT NULL,
  `day` enum('Mon','Tue','Wed','Thu','Fri','Sat') NOT NULL,
  `ssn` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `note_id` int(11) NOT NULL,
  `ssn_s` varchar(255) NOT NULL,
  `ssn_t` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `date` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`note_id`, `ssn_s`, `ssn_t`, `description`, `date`) VALUES
(1, '1DDDDDDDDDDDDDDD', '3AAAAAAAAAAAAAAA', 'Cattivone', '2019-12-03'),
(2, '1EEEEEEEEEEEEEEE', '3AAAAAAAAAAAAAAA', 'infernale', '2019-12-04'),
(3, '1AAAAAAAAAAAAAAA', '3AAAAAAAAAAAAAAA', 'Oggi il ragazzo si e\' comportato molto male a lezione.                                    Nonostante i continui rimproveri, ha continuato a giocare con il compagno di banco disturbando tutta la classe. Pertanto ritengo necessario un incontro durante l\'orario disponibile.', '2019-12-05'),
(4, '1BBBBBBBBBBBBBBB', '3AAAAAAAAAAAAAAA', 'denunciatela vi prego', '2019-12-06'),
(5, '1FFFFFFFFFFFFFFF', '3AAAAAAAAAAAAAAA', 'licenziata', '2019-12-07'),
(6, '1AAAAAAAAAAAAAAA', '3AAAAAAAAAAAAAAA', 'Il ragazzo continua a comportarsi male.', '2019-12-06');

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

DROP TABLE IF EXISTS `parents`;
CREATE TABLE `parents` (
  `ssn` varchar(16) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(16) DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`ssn`, `surname`, `name`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('1234', 'luca', 'luca', 'luca', '1', NULL, 'superpiero23@hotmail.it', 'c506afb8bd268a9c8924f14a92e6d2ad45567f882a7572bc42e0a1b53dcae8bc09bd6a8abfa864277de5ef41aaba54add1806973d84295ae062fae802d9b0f17', '356047'),
('123456789', 'Cilluffo', 'Pietro', 'via stradella cartiera grande,5', '3270105447', NULL, 'piero.cilluffo8@libero.it', 'd3c0734ddec81c53f88969189301a73b5a28041d70cd53e4d1afc3bc9bbe377e84b491d4c647dc4a241d3893793a8cfb8255f1255f31213061e82e8c25d061f2', '209376'),
('12345678910', 'Cilluffo', 'Pietro', 'via stradella cartiera grande,5', '3270105447', NULL, 'piero.cilluffo8@libero.it', '54a331ef9fc94ab57a66e6fddfda2e0fbb4e5b8993a20d5587e6d9c0596d3fc51d7afad5332dd5d0c924392109f2770df692453d78c8aeee282ceefab9477e43', '369185'),
('2AAAAAAAAAAAAAAA', 'Rossi', 'Mario', 'Via Giulio Cesare 21', '0123456789', '0123456789', 'parA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('2BBBBBBBBBBBBBBB', 'Verdi', 'Luigi', 'Corso Garibaldi 34', '9876543210', '9876543210', 'parB@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('2ZZZZZZZZZZZZZZZ', 'Buffo', 'Matteo', '', '3317668592', NULL, 'matteobuf@gmail.com', 'f9c19cd7aedc76600f781d785fb25be893dd3f446dd1be1237f2196fbe26dee2614a41a683e14bf1964d1d263d26eda8b5581990604a12564ca64aa6abadf3aa', '313128');

-- --------------------------------------------------------

--
-- Table structure for table `principals`
--

DROP TABLE IF EXISTS `principals`;
CREATE TABLE `principals` (
  `ssn` varchar(64) NOT NULL,
  `surname` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `homephone` varchar(64) NOT NULL,
  `cellphone` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `principals`
--

INSERT INTO `principals` (`ssn`, `surname`, `name`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('123456', 'gangemi', 'piero', 'via 2', '1', '', 'superpiero23@hotmail.it', 'fa127092450d644dc5f29e2d37a7b4ea24dba101d3f87a4fd4e26dd72b0ec84eb40e2f21ee53dd900670550305e22bcd480948d668cebdd874a66d05b31fcd91', '178064'),
('22', 'fefe', 'efe', 'fefe', 'ee', '', 'superpiero23@hotmail.it', '04227d05849b854e194eb4678e00536e061e5bfb4a3cef62b6fec632efc60b762c809d004ca3e4d16aa2992e6b3f9d22647de3e5093279ba76d9d1fb1ba6227b', '159411'),
('fefe', 'fefe', 'efe', 'fefe', 'ee', '', 'fefe@dwd.it', 'b2b66f5063abf2ce4e68d057d4c348a1cf29323c6a69672a91aea9c109a25e9ad04a918fa272afcb6bbfef3b2346b245d1729e76c98535fceb3e80c04743edc4', '697650');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `ssn` varchar(16) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` varchar(128) NOT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `gender` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`ssn`, `surname`, `name`, `address`, `cellphone`, `gender`) VALUES
('123456622', 'testname', 'testname', 'via dei test', '333999444', 'f'),
('1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'Via Giulio Cesare 21', NULL, 'm'),
('1BBBBBBBBBBBBBBB', 'Rossi', 'Angela', 'Via Giulio Cesare 21', NULL, 'f'),
('1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'Corso Garibaldi 34', NULL, 'm'),
('1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'Via Leonardo 102', NULL, 'm'),
('1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', '', NULL, 'm'),
('1FFFFFFFFFFFFFFF', 'Neri', 'Marina', '', NULL, 'f');

-- --------------------------------------------------------

--
-- Table structure for table `sysadmins`
--

DROP TABLE IF EXISTS `sysadmins`;
CREATE TABLE `sysadmins` (
  `ssn` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sysadmins`
--

INSERT INTO `sysadmins` (`ssn`, `name`, `surname`, `email`, `password`, `salt`) VALUES
('1A', 'Amministratore', 'Supremo', 'sysadmin@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `ssn` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(512) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`ssn`, `email`, `password`, `name`, `surname`, `salt`, `address`, `homephone`, `cellphone`) VALUES
('3AAAAAAAAAAAAAAA', 'teachA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Gianpiero', 'Cabodi', '123456', 'Via Dijkstra 10', '', ''),
('3BBBBBBBBBBBBBBB', 'teachB@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Paolo Enrico', 'Camurati', '123456', 'Via Kernighan 70', '', ''),
('3CCCCCCCCCCCCCC', 'teachC@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Lucio', 'Massa', '123456', 'via asd 25', '123456', '123456'),
('3DDDDDDDDDDDDDD', 'teachD@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Diego', 'Catto', '123456', 'via coseno 314', '123456', '123456'),
('3EEEEEEEEEEEEEE', 'teachE@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Clara', 'Dadda', '123456', 'p.zza Castello 15', '123456', '123456'),
('3FFFFFFFFFFFFFFF', 'teachF@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Paola', 'Lanza', '123456', 'via qwerty 123', '123', '123'),
('testtest', 'test@test.it', 'e5f97a20489ceff4716ebce4263d4315819ce9397a5d373471c4be0ac7f25ee410a75a9c1ea34f2e1534d838b6cce50cf3fe1038a5d733d1fee1340501c782c1', 'ADELAIDETESTINGNAME', 'parolini', 'abd', 'via 2', '30000933', '33399933');

-- --------------------------------------------------------

--
-- Table structure for table `timetables`
--

DROP TABLE IF EXISTS `timetables`;
CREATE TABLE `timetables` (
  `ttid` int(11) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `hour` varchar(6) NOT NULL,
  `day` enum('Mon','Tue','Wed','Thu','Fri','Sat') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timetables`
--

INSERT INTO `timetables` (`ttid`, `cid`, `ssn_t`, `subject`, `hour`, `day`) VALUES
(1, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Mon'),
(2, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Mon'),
(3, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Mon'),
(4, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Mon'),
(5, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Mon'),
(6, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Mon'),
(7, '', '', '', '', ''),
(8, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Tue'),
(9, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Tue'),
(10, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Tue'),
(11, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Tue'),
(12, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Tue'),
(13, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Tue'),
(14, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Wed'),
(15, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Wed'),
(16, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Wed'),
(17, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Wed'),
(18, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Wed'),
(19, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Wed'),
(20, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Thu'),
(21, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Thu'),
(22, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Thu'),
(23, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Thu'),
(24, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Thu'),
(25, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Thu'),
(26, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Fri'),
(27, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Fri'),
(28, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Fri'),
(29, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Fri'),
(30, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Fri'),
(31, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Fri'),
(32, 'C2', '3FFFFFFFFFFFFFFF', 'diritto', '13:00', 'Mon'),
(33, 'C2', '3BBBBBBBBBBBBBBB', 'italiano', '11:00', 'Mon'),
(34, 'C2', '3BBBBBBBBBBBBBBB', 'italiano', '12:00', 'Mon'),
(35, 'C2', '3CCCCCCCCCCCCCC', 'storia', '09:00', 'Mon'),
(36, 'C2', '3DDDDDDDDDDDDDD', 'fisica', '10:00', 'Mon'),
(37, 'C2', '3AAAAAAAAAAAAAAA', 'fisica', '08:00', 'Mon'),
(38, '# [SSN]', '[Surname]', '[Name]', '[Arith', ''),
(39, '1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'EMPTY', ''),
(40, '1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', 'EMPTY', ''),
(41, '1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'EMPTY', ''),
(42, '1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'EMPTY', ''),
(43, '1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'EMPTY', ''),
(44, '1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', 'EMPTY', ''),
(45, '1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'EMPTY', ''),
(46, '1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'EMPTY', ''),
(47, '1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'EMPTY', ''),
(48, '1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', 'EMPTY', ''),
(49, '1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'EMPTY', ''),
(50, '1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'EMPTY', ''),
(51, '1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'EMPTY', ''),
(52, '1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', 'EMPTY', ''),
(53, '1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'EMPTY', ''),
(54, '1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'EMPTY', ''),
(55, '1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', '7.000', ''),
(56, '1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', 'EMPTY', ''),
(57, '1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'EMPTY', ''),
(58, '1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'EMPTY', ''),
(59, '1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'EMPTY', ''),
(60, '1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', 'EMPTY', ''),
(61, '1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'EMPTY', ''),
(62, '1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'EMPTY', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absences_presences`
--
ALTER TABLE `absences_presences`
  ADD PRIMARY KEY (`abs_id`);

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`ssn`);

--
-- Indexes for table `bridge_class_students`
--
ALTER TABLE `bridge_class_students`
  ADD PRIMARY KEY (`cid`,`ssn_s`),
  ADD KEY `ssn_s` (`ssn_s`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `final_marks`
--
ALTER TABLE `final_marks`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `homeworks`
--
ALTER TABLE `homeworks`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`lid`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`metID`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`ssn`);

--
-- Indexes for table `principals`
--
ALTER TABLE `principals`
  ADD PRIMARY KEY (`ssn`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`ssn`);

--
-- Indexes for table `sysadmins`
--
ALTER TABLE `sysadmins`
  ADD PRIMARY KEY (`ssn`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`ssn`);

--
-- Indexes for table `timetables`
--
ALTER TABLE `timetables`
  ADD PRIMARY KEY (`ttid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homeworks`
--
ALTER TABLE `homeworks`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `metID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `timetables`
--
ALTER TABLE `timetables`
  MODIFY `ttid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
