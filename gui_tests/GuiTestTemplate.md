# Gui Testing Documentation template

Authors: Team B

Date: 15/11/2019

Version: 1

# Contents

- [Scenarios](#scenarios)

# Scenarios


| Scenario ID: SC1 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The parent wants to login to the system        |
| Precondition     | Parent must open the login page                |
| Postcondition    | Parent panel page is appeared                  |
| Step#            | Step description                               |
| 1                | Parent enter his/her username                  |
| 2                | Parent enter his/her password                  |
| 3                | Parent selects the role type                   |
| 4                | Parent clicks the login button                 |

| Scenario ID: SC2 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to login to the system       |
| Precondition     | Teacher must open the login page               |
| Postcondition    | Teacher panel page is appeared                 |
| Step#            | Step description                               |
| 1                | Teacher enter his/her username                 |
| 2                | Teacher enter his/her password                 |
| 3                | Tearent selects the role type                  |
| 4                | Teacher clicks the login button                |

| Scenario ID: SC3 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The admin officer wants to login to the system       |
| Precondition     | Admin officer must open the login page               |
| Postcondition    | Admin officer panel page is appeared                 |
| Step#            | Step description                                     |
| 1                | Admin officer enter his/her username                 |
| 2                | Admin officer enter his/her password                 |
| 3                | Admin officer selects the role type                  |
| 4                | Admin officer clicks the login button                |

| Scenario ID: SC4 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to login to the system but enters wrong password |
| Precondition     | Teacher must open the login page               |
| Postcondition    | Teacher panel page is appeared                 |
| Step#            | Step description                               |
| 1                | Teacher enter his/her username                 |
| 2                | Teacher enter his/her password                 |
| 3                | Tearent selects the role type                  |
| 4                | Teacher clicks the login button                | 

| Scenario ID: SC5 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The parent wants to select his/her child       |
| Precondition     | Parent has to already login to the system      |
| Postcondition    | Main parent panel page is appeared             |
| Step#            | Step description                               |
| 1                | Parent clicks to select his/her child          |
| 2                | Main parent panel page is appeared             |

| Scenario ID: SC6 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The parent wants to view his/her child marks   |
| Precondition     | Parent has to already login to the system      |
| Postcondition    | Marks list is appeared                         |
| Step#            | Step description                               |
| 1                | Parent clicks to marks section in the navigation bar |
| 2                | Marks list is appeared                         |

| Scenario ID: SC7 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The admin officer wants enter class composition to the system    |
| Precondition     | Admin officer must login to the system                           |
| Postcondition    | The list of student is appeared                                  |
| Step#            | Step description                                                 |
| 1                | Admin officer clicks to add parent section in the navigation bar |
| 2                | Admin officer fills the form                                     |
| 3                | Admin officer clicks the add new parent button                   |

| Scenario ID: SC8 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The admin officer wants add new student to the system          |
| Precondition     | Admin officer must login to the system                         |
| Postcondition    | The success message is appeared                                |
| Step#            | Step description                                               |
| 1                | Admin officer clicks to enter class composition section in the navigation bar |
| 2                | Admin officer selects the class                                |
| 3                | Admin officer clicks the enter button                          |

| Scenario ID: SC9 |                                                |
| ---------------- | ---------------------------------------------- |
| Description      | The admin officer wants add new parent to the system             |
| Precondition     | Admin officer must login to the system                           |
| Postcondition    | The success message is appeared, Email is sent by system to the parent which includes username and password |
| Step#            | Step description                                                 |
| 1                | Admin officer clicks to add parent section in the navigation bar |
| 2                | Admin officer fills the form                                     |
| 3                | Admin officer clicks the add new parent button                   |

| Scenario ID: SC10 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record lecture            |
| Precondition     | Teacher must login to the system               |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to record lecture section in the navigation bar |
| 2                | Teacher fills the form                         |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC11 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The parent wants to view his/her child homeworks |
| Precondition     | Parent has to already login to the system      |
| Postcondition    | Homeworks list is appeared                     |
| Step#            | Step description                               |
| 1                | Parent clicks to homeworks section in the navigation bar |
| 2                | Marks list is appeared                         |

| Scenario ID: SC12 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record marks              |
| Precondition     | Teacher must login to the system               |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to record marks section in the navigation bar|
| 2                | Teacher fills the form                         |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC13 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The parent wants to check previous day attendance of his/her child |
| Precondition     | Parent has to already login to the system      |
| Postcondition    | Attendance list is appeared                    |
| Step#            | Step description                               |
| 1                | Parent clicks to check attendance section in the navigation bar|
| 2                | Check attendance view  is appeared             |
| 3                | Parent clicks to previous day button           |

| Scenario ID: SC14 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The parent wants to check specific day attendance of his/her child |
| Precondition     | Parent has to already login to the system      |
| Postcondition    | Attendance list is appeared                    |
| Step#            | Step description                               |
| 1                | Parent clicks to check attendance section in the navigation bar |
| 2                | Check attendance view  is appeared              |
| 3                | Parent enters to specific day to the date input |

| Scenario ID: SC15 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants setup the admin officer account |
| Precondition     | System administrator has to already logged in to the system    |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Officer" section in the navigation bar |
| 2                | System administrator fills the form            |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC16 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants setup the admin officer account |
| Precondition     | System administrator has to already logged in to the system    |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Officer" section in the navigation bar |
| 2                | System administrator fills the form not complete     |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC17 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants setup the teacher account    |
| Precondition     | System administrator has to already logged in to the system |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Teacher" section in the navigation bar |
| 2                | System administrator fills the form            |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC18 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants setup the teacher account    |
| Precondition     | System administrator has to already logged in to the system |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Teacher" section in the navigation bar |
| 2                | System administrator fills the form not complete     |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC19 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants setup the principal account  |
| Precondition     | System administrator has to already logged in to the system |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Principal" section in the navigation bar |
| 2                | System administrator fills the form            |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC20 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants setup the principal account  |
| Precondition     | System administrator has to already logged in to the system |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Principal" section in the navigation bar |
| 2                | System administrator fills the form not complete       |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC21 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants to add a new teaching        |
| Precondition     | System administrator has to already logged in to the system |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Teaching" section in the navigation bar |
| 2                | System administrator fills the form            |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC22 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The system administrator wants to add a new teaching        |
| Precondition     | System administrator has to already logged in to the system |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | System administrator clicks to "Add Teaching" section in the navigation bar |
| 2                | System administrator fills the form form not complete |
| 3                | System administrator clicks "Submit" button |

| Scenario ID: SC23 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record absence note       |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Record Absence" section in the navigation bar |
| 2                | Teacher fills the form                         |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC24 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record absence note       |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Record Absence" section in the navigation bar |
| 2                | Teacher fills the form                         |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC25 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record when a student enters the class late |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Record Absence" section in the navigation bar   |
| 2                | Teacher fills the form and selects "Enter late" option in the list |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC26 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record when a student enters the class late |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Record Absence" section in the navigation bar   |
| 2                | Teacher fills the form and selects "Enter late" option in the list |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC27 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record when a student leaves the class earlier |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Record Absence" section in the navigation bar   |
| 2                | Teacher fills the form and selects "Exit early" option in the list |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC28 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record when a student leaves the class earlier |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Record Absence" section in the navigation bar   |
| 2                | Teacher fills the form and selects "Exit early" option in the list |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC29 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record the assignments for the students       |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Add Homeworks" section in the navigation bar    |
| 2                | Teacher fills the form                         |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC30 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to record the assignments for the students       |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Add Homeworks" section in the navigation bar    |
| 2                | Teacher fills the form                         |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC31 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to publish support material for the stydents     |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Add Homeworks" section in the navigation bar    |
| 2                | Teacher fills the form and attach support material                 |
| 3                | Teacher clicks the submit button               |

| Scenario ID: SC32 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to check timetables of the classes |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The timetable is shown to the teacher          |
| Step#            | Step description                               |
| 1                | Teacher clicks to "View Timetable" section in the navigation bar   |
| 2                | Teacher selects a class and subject            |

| Scenario ID: SC33 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The administrative officer wants publish official communications   |
| Precondition     | The administrative officer has already needs to be logged in       |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | The administrative officer clicks to "Add Communication" section in the navigation bar |
| 2                | The administrative officer fills the form           |
| 3                | The administrative officer clicks the submit button |

| Scenario ID: SC34 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The administrative officer wants publish the timetable of classes  |
| Precondition     | The administrative officer has already needs to be logged in       |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | The administrative officer clicks to "Manage Timetables" section in the navigation bar |
| 2                | The administrative officer selects timetable file   |
| 3                | The administrative officer submits new timetable    |

| Scenario ID: SC35 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The administrative officer wants publish the timetable of classes  |
| Precondition     | The administrative officer has already needs to be logged in       |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | The administrative officer clicks to "Manage Timetables" section in the navigation bar |
| 2                | The administrative officer selects timetable file   |
| 3                | The administrative officer submits new timetable    |

| Scenario ID: SC36 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to download support material of the subject |
| Precondition     | Parent has already needs to be logged in   |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Parent clicks to "Support Material" section in the navigation bar |
| 2                | Parent selects subject of his/her child        |
| 3                | Parent downloads file by clicking on the name of the file         |

| Scenario ID: SC37 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to download support material of the subject |
| Precondition     | Parent has already needs to be logged in   |
| Postcondition    | The error message is appeared              |
| Step#            | Step description                               |
| 1                | Parent clicks to "Support Material" section in the navigation bar    |
| 2                | Parent selects subject of his/her child        |
| 3                | Parent can not download the file by clicking on the name of the file |

| Scenario ID: SC38 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to read official communications   |
| Precondition     | Parent has already needs to be logged in       |
| Postcondition    | The communication message is appeared          |
| Step#            | Step description                               |
| 1                | Parent clicks to "Home" section in the navigation bar |
| 2                | Parent can see official communication messages        |

| Scenario ID: SC39 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to see the timetables of the classes     |
| Precondition     | Parent has already needs to be logged in       |
| Postcondition    | The timetable is on the screen                 |
| Step#            | Step description                               |
| 1                | Parent clicks to "Timetable" section in the navigation bar |
| 2                | Timetable is visible to the parent             |

| Scenario ID: SC40 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to write a note to parents   |
| Precondition     | The teacher has already needs to be logged in  |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | The teacher clicks to "Write a note" section in the navigation bar |
| 2                | The teacher fills in the form and submit       |

| Scenario ID: SC41 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | The teacher wants to write a note to parents   |
| Precondition     | The teacher has already needs to be logged in  |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | The teacher clicks to "Write a note" section in the navigation bar |
| 2                | The teacher fills in the form and submit       |

| Scenario ID: SC42 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to access the notes to parents    |
| Precondition     | Parent has already needs to be logged in       |
| Postcondition    | The list of notes is available                 |
| Step#            | Step description                               |
| 1                | Parent clicks to "Notes" section in the navigation bar |
| 2                | Parent sees the filled (or empty) list of notes        |

| Scenario ID: SC43 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Class coordinating teacher wants to publish the final grades of the term |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Final Marks" section in the navigation bar |
| 2                | Teacher uploads template with final marks      |

| Scenario ID: SC44 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Class coordinating teacher wants to publish the final grades of the term |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The error message is appeared                  |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Final Marks" section in the navigation bar |
| 2                | Teacher uploads template with final marks      |

| Scenario ID: SC45 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to see the final grades of the term |
| Precondition     | Parent has already needs to be logged in       |
| Postcondition    | Marks table is available to the parent         |
| Step#            | Step description                               |
| 1                | Parent clicks to "Marks" section in the navigation bar |

| Scenario ID: SC46 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Teacher wants to provide a list time slots available for parent meetings |
| Precondition     | Teacher has already needs to be logged in      |
| Postcondition    | The success message is appeared                |
| Step#            | Step description                               |
| 1                | Teacher clicks to "Manage Meetings" section in the navigation bar   |
| 2                | Teacher clicks to free time slot to provide appointment for parents |

| Scenario ID: SC47 |                                               |
| ---------------- | ---------------------------------------------- |
| Description      | Parent wants to book a meeting with a teacher  |
| Precondition     | Parent has already needs to be logged in       |
| Postcondition    | The time slot is reserved                      |
| Step#            | Step description                               |
| 1                | Parent clicks to "Meetings" section in the navigation bar   |
| 2                | Parent clicks to available time slot to reserve a meeting   |