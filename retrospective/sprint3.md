# Retrospective - Sprint #3 (Team B)

## Process measures


### Macro statistics

+ \# of stories committed: **10**
+ \# of stories done: **9**
+ Definition of Done (DoD):
    + Unit Tests passing
    + Code review completed
    + Code present on VCS
    + End-to-End tests performed

### Detailed statistics

| Story ID | # of tasks | Points | Total hours estimated | Total hours spent |
|-|-|-|-|-|
| `story#10` | 2 | 2 | 4 | 3
| `story#11` | 4 | 3 | 6 | 8
| `story#12` | 4 | 3 | 6 | 5
| `story#13` | 2 | 1 | 1 | 2
| `story#14` | 7 | 8 | 14 | 10
| `story#15` | 5 | 5 | 8 | 8
| `story#16` | 2 | 2 | 4 | 5
| `story#17` | 3 | 2 | 6 | 6
| `story#25` | 4 | 2 | 4 | 3
| `story#28` | 6 | 3 | 6 | 4
| `story#0` | 3 | ... | 15 | 23

+ Hours per task: **1.7**
+ Total task estimation error ratio: **0.97**

## Quality measures

### Unit Testing
+ Total hours estimated: 25
+ Total hours spent: 32
+ \# of automated unit tests: 10
### E2E Testing
+ Total hours estimated: 12
+ Total hours spent: 4
### Code review
+ Total hours estimated: 12
+ Total hours spent: 5

## Assessment

### Causes of errors in estimation:
+ `story#11`: misunderstandings among team members.
+ `story#14`: lack of experience in Poker Estimation phase.

### Lessons learnt in this sprint:
+ Improve communication/explanation of ideas/issues among team members.
+ Start developing a story without a clear plan/representation of it (i.e. homework and support material concepts).
+ Avoid ending up doing last-minute fixing.

### Improvement goals set in the previous retrospective achieved:
+ Misinterpreted absence handling.
+ Related wrong test.

### Improvement goals we were not able to achieve:
+ Fixing of some bugs about GUI elements.

### Improvement goals for the next sprint:
+ Improvements in GUI.
+ Bug fixing.
+ Reduction of Technical Debt by means of SonarQube.

### One thing we are proud of:
+ Completed most of the stories and implemented a lot of new features.