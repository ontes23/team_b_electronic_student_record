# Estimation - Sprint #2

## Story #6

+ \# of tasks committed: **4**
+ \# of tasks completed: **4**
+ \# of members who worked on it: **2**
+ Average time on task per member: **1.5 hours**
+ Distribution of time:
    + Each member worked for **6 hours**, of which  
        + actual development of project: 5 h/member  
        + testing: 1 h/member
    + In total: 6 hours * 2 members = **12 hours**

## Story #7

+ \# of tasks committed: **4**
+ \# of tasks completed: **4**
+ \# of members who worked on it: **1**
+ Average time on task per member: **~1 hour**
+ Distribution of time:
    + The member worked for **5 hours**, of which
        + actual development of project: 4 h
        + testing: 0.5 h

## Story #30
+ \# of tasks committed: **5**
+ \# of tasks completed: **5**
+ \# of members who worked on it: **2**
+ Average time on task per member: **~2 hours**
+ Distribution of time:
    + Each member worked for **9 hours**, of which
        + actual development of project: 8 h/member
        + testing: 1 h/member 
    + In total: 9 hours * 2 members = **18 hours**

## Story #8
+ \# of tasks committed: **3**
+ \# of tasks completed: **3**
+ \# of members who worked on it: **2**
+ Average time on task per member: **2 hours**
+ Distribution of time:
    + Each member worked for **6 hours**, of which
        + actual development of project: 3.5 h/member
        + testing: 2.5 h/member
    + In total: 6 hours * 2 members = **12 hours**

## Story #9
+ \# of tasks committed: **4**
+ \# of tasks completed: **4**
+ \# of members who worked on it: **2**
+ Average time on task per member: **1.5 hour**
+ Distribution of time:
    + Each member worked for **6 hour**, of which
        + actual development of project: 4 h/member
        + testing: 2 h/member
    + In total:  6 hours * 2 members = **12 hours**

## Feedback from Sprint #1
+ About:
    + Responsiveness of pages
    + Reading from a CSV file
+ \# of tasks committed: **2**
+ \# of tasks completed: **2**
+ \# of members who worked on it: **1**
+ Time spent: **5 hours**

## Docker
+ (no task created)
+ \# of members who worked on it: **1**
+ Time spent: **5 hours**

## Overall improvements
+ (no task created)
+ \# of members who worked on it: **1**
+ Time spent: **4 hours**

## Final results

+ Time spent per member for the sprint: **73 hours**
+ Total time spent for the sprint: **~12 hours**
+ Percentage of delay: 1%

## Notes
+ Understimated stories:  
    + Story #6
    + Story #8
+ Overestimated stories:
    + Story #7

## Future improvements for this sprint
+ Misinterpreted absence handling
+ Related wrong test
