# Estimation - Sprint #1

## Story #1

+ \# of tasks committed: **8**
+ \# of tasks completed: **8**
+ \# of members who worked on it: **6**
+ Average time on task per member: **45 mins.**
+ Distribution of time:
    + 3 members worked on front end, the other 3 on back end
        + Each member worked for **5 hours**, of which
            + decision, learning and setup: 1.5 h/member
            + actual development of project: 3 h/member
            + testing: 0.5 h/member
    + In total: 5 hours * 6 members = **30 hours**

## Story #2

+ \# of tasks committed: **6**
+ \# of tasks completed: **6**
+ \# of members who worked on it: **2**
+ Average time on task per member: **50 mins.**
+ Distribution of time:
    + One member worked on front end and one for back end
        + Each member worked for **3.5 hours**, of which
            + actual development of project: 3 h/member
            + testing: 0.5 h/member
    + In total: 3.5 hours * 2 members = **7 hours**

## Story #3
+ \# of tasks committed: **7**
+ \# of tasks completed: **7**
+ \# of members who worked on it: **3**
+ Average time on task per member: **1.5 hours**
+ Distribution of time:
    + Each member worked for **5.5 hours**, of which
        + actual development of project: 5 h/member
        + testing: 0.5 h/member 
    + In total: 5.5 hours * 3 members = **16.5 hours**

## Story #4
+ \# of tasks committed: **3**
+ \# of tasks completed: **3**
+ \# of members who worked on it: **2**
+ Average time on task per member: **2 hours**
+ Distribution of time:
    + Each member worked for **2.5 hours**, of which
        + actual development of project: 2 h/member
        + testing: 0.5 h/member
    + In total: 2.5 hours * 2 members = **5 hours**

## Story #5
+ \# of tasks committed: **6**
+ \# of tasks completed: **6**
+ \# of members who worked on it: **6**
+ Average time on task per member: **1 hour**
+ Distribution of time:
    + Each member worked for **1.5 hour**, of which
        + actual development of project: 1 h/member
        + testing: 0.5 h/member
    + In total: 1.5 hours * 6 members = **9 hours**

## Use cases
+ (no task creation for this)
+ \# of members who worked on it: **1**
+ Time spent: **5 hours**


## Personas
+ (no task creation for this)
+ \# of members who worked on it: **6**
+ Each member worked for **0.5 hour**
+ In total: 0.5 hours * 6 members = **3 hours**

## Final results

+ Time spent per member for the sprint: **12.5 hours**
+ Total time spent for the sprint: **76 hours**
+ Percentage of delay: 5%

## Future improvements for this sprint

+ Responsiveness of webpages
+ Insertion of students/parents data by means of a file
