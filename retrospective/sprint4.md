# Retrospective - Sprint #4 (Team B)

## Process measures


### Macro statistics

+ \# of stories committed: **7**
+ \# of stories done: **7**
+ Definition of Done (DoD):
    + Unit Tests passing
    + Code review completed
    + Code present on VCS
    + End-to-End tests performed

### Detailed statistics

| Story ID | # of tasks | Points | Total hours estimated | Total hours spent |
|-|-|-|-|-|
| `story#14`* | 1 | 8 | 2 | 2
| `story#18` | 4 | 3 | 7 | 6
| `story#19` | 4 | 2 | 5 | 6
| `story#20` | 5 | 5 | 9 | 14
| `story#21` | 5 | 3 | 7 | 10
| `story#22` | 4 | 2 | 5 | 4
| `story#23` | 5 | 3 | 7 | 12
| `story#0` | 2 | ... | 25 | 19

\* This story was missing only a PHPUnit Test from Sprint #3.


+ Hours per task: **2.4**
+ Total task estimation error ratio: **0.91**

## Quality measures

### Unit Testing
+ Total hours estimated: 14
+ Total hours spent: 7
+ \# of automated unit tests: 7
### E2E Testing
+ Total hours estimated: 4
+ Total hours spent: 2.5
### Code review
+ Total hours estimated: 3
+ Total hours spent: 6
### Technical Debt management: 
+ Total hours estimated: 20
+ Total hours spent: 16

### Overall Technical Debt at the demo: 
+ Number of days: 3
+ Debt ratio: 0.5%
+ Rating: A

## Assessment

### Causes of errors in estimation:
+ The Code review phase took more time than estimated: we found some bugs that occurred only in particular situations that we had not tested before.
+ We spent less time than estimated in `story#0` (i.e. SonarQube/TD and Docker): since Docker overestimates TD days (we got a TD of 18 days before its decrase), we planned to spend 20 hours on it. In the end, we spent only 16 hours.

### Lessons learnt in this sprint:
+ It is important to reduce TD during the whole project, because handling it only at its end is more difficult.

### Improvement goals set in the previous retrospective achieved:
+ Bug fixing.
+ Reduction of Technical Debt by means of SonarQube.

### Improvement goals we were not able to achieve:
+ Improvements in GUIs.

### One thing we are proud of:
+ We completed the 86% of the original product backlog! :-)