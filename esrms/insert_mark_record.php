<?php

if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
    if (mysqli_connect_errno()) {
        echo "Connessione fallita: ".
            mysqli_connect_error();
        exit();
    }

 

    /* Fetch data from $POST */
    $ssn_t = $_SESSION['user_ssn'];     
    $cid = $_POST['cid'];
    $class = $_POST['class'];
    $subject = $_POST['subject'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $description = $_POST['description'];
	$description = str_replace("'", "\\'",$description);

		if(!isset($_SESSION["testusage"])){
			/* Check of valid date and time (e.g. today is 24/11/2019) */
			$now = time();
			$now_date = date('d-m-Y', $now);
			$now_date = strtotime($now_date);               // 24/11/2019, 00:00

			$input_date = strtotime($date);                 // [input_date], 00:00

			$input_time = strtotime($time);
			$input_time -= $now_date;                       // 01/01/1970, [input_time]

			$input_date_time = $input_date + $input_time;   // [input_date], [input_time]

			if($input_date_time > $now){                    // Date/time in future
					header("location:teacher_page_marks.php?class=".$cid."&error_future_date=Error: future date and/or time!");
					die();
			}
			
			$school_openingHour = strtotime("01/01/1970, 09:00:00");
			$school_closingHour = strtotime("01/01/1970, 16:00:00");
			if($input_time<$school_openingHour || $input_time>$school_closingHour){    // Not school hour
					header("location:teacher_page_marks.php?class=".$cid."&error_not_school_hour=Error: school classes last from 08:00 to 15:00!");
					die();
			}

			$date_diff = $now - $input_date_time;
			$date_diff = round($date_diff/(60*60*24));
			if($date_diff > 30){                            // More than a month passed since present date
					header("location:teacher_page_marks.php?class=".$cid."&error_expired_time_window=Error: more than a month has passed since this date!");
					die();
			}
		}
    /* Fetch keys */
    $keys = array_keys($_POST);

    /* Fill student and mark arrays */
    $students = array();
    $marks = array();
    $wrong_marks = 0;
    $wrong_marks_get = '';
    $at_least_one_mark = 0;
    for($i=0; $i<count($keys); $i++){
        if(substr($keys[$i], 0, 5) == "stud_"){
            $ssn_s = substr($keys[$i], 5);
            $mark = $_POST[$keys[$i]];
            if($mark==''){
                //NOTHING
            }
            else if(checkMark($mark)){
                array_push($students, $ssn_s);
                $j = $i+1;
                if($j<count($keys) && substr($keys[$j], 0, 6)=="laude_" && $_POST[$keys[$j]] == "on" ) {      // Check if laude has been checked or not
                   
                        $mark++;
                    
                }
                array_push($marks, $mark);
                $at_least_one_mark++;
            }
            else{
                $wrong_marks_get .= "&error_wrong_mark_".$ssn_s."=Error: the mark is not valid!";
                $wrong_marks++;
                $at_least_one_mark++;
            }
        }
    }

    if($at_least_one_mark==0){
        header("location:teacher_page_marks.php?class=".$cid."&error_no_marks=Error: no mark has been written!");
        die();
    }

  


    if($wrong_marks>0){
        header("location:teacher_page_marks.php?class=".$cid.$wrong_marks_get);
        die();
    }

    $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
    if (mysqli_connect_errno()) {
        echo "Connessione fallita: ". mysqli_connect_error();
        exit();
    }
    /* Select the last m_id */
    $sql = "SELECT *
                FROM marks";
    if(!$result =mysqli_query($conn,$sql)) {
        $msg = "Errore nell’inserimento del post, riprovare";
    }
    $temp = mysqli_num_rows($result);
    $temp++;

    for($i=0; count($students)>0 && $i<count($students); $i++){
        /* Insert marks into DB */
        $sql = "INSERT INTO marks(m_id, ssn_s, subject, mark, ssn_t, date, time,description)
                    VALUES ('".$temp."','".$students[$i]."','".$subject."','".$marks[$i]."','".$ssn_t."','".$date."','".$time."','".$description."')";
       
        if(!$result =mysqli_query($conn,$sql)) {
            die("Errore nell’inserimento del post, riprovare");
        }
        $temp++;



    }

    header("location:teacher_page.php?successful_op=The marks have been successfully inserted");


    function checkMark($mark){
        echo $mark;
        $mark_formatted = number_format((float)$mark, 2, '.', '');

        if(!is_numeric($mark_formatted)){
            return false;
        }
        if(($mark_formatted<0 || $mark_formatted>10) && $mark_formatted!=11){
            return false;
        }
        $whole = floor($mark_formatted);
        $fract = $mark_formatted-$whole;
        echo $fract;
        if($fract!=0 && $fract!=0.25 && $fract!=0.5 && $fract!=0.75){
            return false;
        }
        return true;
    }

?>
