var N = 7; //number of rows dikey olanlar saatler
var M = 7;

var dates = ["","Monday", "Tuesday", "Wednesday","Thursday","Friday","Saturday"];
var times = ["","8:00", "9:00", "10:00","11:00","12:00","13:00"];

var selectedTime = new Object(); //currently sellected time

var cellObjects = new Array(N);
var timearr = new Array(2);

for(var i=0 ; i< N; i++){
    cellObjects[i] = new Array(M);
    for(var j = 0; j<M; j++){
        cellObjects[i][j] = new Object();
    }
}

function addClass(cell, c){
    if(cell != undefined){
        cell.setAttribute("class",c);
    }
}

function removeClass(cell){
    if(cell != undefined){
        cell.removeAttribute();
    }
}

function assignCellId(i,j) {
    return "c-"+times[i]+"-"+dates[j];
}

function getCoordinate(cell) {
    if (cell != undefined)
        return cell.getAttribute("id").split("-").slice(1,3);
    else
        return undefined;
}

function assignColor(cell) {
    var cord = getCoordinate(cell);
    var time = cord[0];
    var date = cord[1];
    $(".output").load("assign_lecture_meeting.php", { time :time , date : date},function(response,status,xhr){
        if( response == 1){
            var free_slot = document.getElementById("c-"+cord[0]+"-"+cord[1]);
            free_slot.innerHTML="Free time";
            free_slot.style.backgroundColor = "#26c281";
            free_slot.style.color ="white";

        }
        else if(response == 2) {
            var meeting = document.getElementById("c-"+cord[0]+"-"+cord[1]);
            meeting.innerHTML="Meeting with parent..";
            meeting.style.backgroundColor = "red";
            meeting.style.color = "white";
        }
        else{
            cell.innerHTML =response;
            var lecture = document.getElementById("c-"+cord[0]+"-"+cord[1]);
            lecture.style.backgroundColor = "#00bfff";
           lecture.style.color = "white";
        }
    });
}

function createTimetable(){
    var arraylength = dates.length;
    var maindiv = document.getElementById("tablediv");
    var timeTable = document.createElement('table');
    timeTable.setAttribute("id","timeTable");
    maindiv.appendChild(timeTable);

    for (var i=0; i<N; i++) { // ilk basta 1.row icin

        var row=timeTable.insertRow(i);
        for (var j=0; j<M; j++) { //column sayisi kadar row a column ekle

            var cell=row.insertCell(j);
            if (i==0 ) {
                cell.innerHTML =dates[j];
            }
            else if (j==0 ) {
                cell.innerHTML=times[i];
            }

            else  {
                cell.setAttribute("onclick", "clickCell(this)");
                //cell.setAttribute("onMouseOver", "onMouseOverCell(this)");
                //cell.setAttribute("onMouseOut", "onMouseOutCell(this)");
                cell.setAttribute("id", assignCellId(i,j));
                assignColor(cell);


            }
        };
    };
};


function clickCell (cell) {
    var cord = getCoordinate(cell);
    var i = cord[0];
    var j = cord[1];

    var element = document.getElementById("c-"+cord[0]+"-"+cord[1]);
    if(element.style.backgroundColor == "rgb(38, 194, 129)"){
        element.innerHTML="Meeting";
        element.style.backgroundColor = "orange";
    }
    else if(element.style.backgroundColor == "rgb(0, 191, 255)"){
       alert("You can not choose this slot!!!");
    }
    else if(element.style.backgroundColor == "red"){
        if(element.innerHTML==""){
            element.innerHTML="Free time";
            element.style.backgroundColor = "rgb(38, 194, 129)";
        }else {
            alert("You can not choose this slot!!!");
        }
    }
    else if(element.style.backgroundColor == "orange"){
            element.innerHTML="Free time";
            element.style.backgroundColor = "rgb(38, 194, 129)";
    }

}