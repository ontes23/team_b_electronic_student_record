<?php

    session_start();
    $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
    if (mysqli_connect_errno()) {
        echo "Connessione fallita: ".
            mysqli_connect_error();
        exit();
    }

    define("ABSENT","Absent");
    define("LATE","Enter late");
    define("EARLY","Exit early");
    define("EIGHT","08:00");
    define("NINE","09:00");
    define("TEN","10:00");
    define("ELEVEN","11:00");
    define("TWELVE","12:00");
    define("THIRTEEN","13:00");
    define("FOURTEEN","14:00");
    define("GOOUT","goout");
    define("MSG", "Errore nell’inserimento del post, riprovare");
    /* Fetch data from T */
    $ssn_t = $_POST['ssn_t'];     
    $cid = $_POST['cid'];
    $class = $_POST['class'];
    $date = $_POST['date'];
    $time = $_POST['time'];

    /* Check of valid date and time (e.g. today is 24/11/2019) */
    $now = time();
    $now_date = date('d-m-Y', $now);
    $now_date = strtotime($now_date);               // 24/11/2019, 00:00


		$weekDay = date('w', strtotime($date));
		if($weekDay==0){
	       header("location:teacher_page_absences.php?class=".$cid."&error_future_date=Error: Sunday day!");
        die();		
		}

    $input_date = strtotime($date);                 // [input_date], 00:00

    $input_time = strtotime($time);
    $input_time -= $now_date;                       // 01/01/1970, [input_time]

    $input_date_time = $input_date + $input_time;   // [input_date], [input_time]

    if($input_date_time > $now){                    // Date/time in future
        header("location:teacher_page_absences.php?class=".$cid."&error_future_date=Error: future date and/or time!");
        die();
    }

    $school_openingHour = strtotime("01/01/1970, 09:00:00");
    $school_closingHour = strtotime("01/01/1970, 16:00:00");
		
    if($input_time<$school_openingHour || $input_time>$school_closingHour){    // Not school hour
        header("location:teacher_page_absences.php?class=".$cid."&error_not_school_hour=Error: school classes last from 08:00 to 15:00!");
        die();
    }

    $date_diff = $now - $input_date_time;
    $date_diff = round($date_diff/(60*60*24));
    if($date_diff > 30){                            // More than a month passed since present date
        header("location:teacher_page_absences.php?class=".$cid."&error_expired_time_window=Error: more than a month has passed since this date!");
        die();
    }

    if(strlen($time)!=5){
        $time = "0".$time[0].":00";
    }
    else{
        $time = $time[0].$time[1].":00";

    }

    /* Fetch keys */
    $keys = array_keys($_POST);

    /* Fill student, absence and description arrays */
    $students = array();
    $absences = array();
    $descriptions = array();
	
    $at_least_one_absence = 0;
    for($i=0; $i<count($keys); $i++){
        if(substr($keys[$i], 0, 12) == "absenceForm_"){
            $ssn_s = substr($keys[$i], 12);
            $absence = $_POST[$keys[$i]];
			

            if($absence == ABSENT){
                $description = ABSENT;
				$description = str_replace("'", "\\'",$description);
                array_push($students, $ssn_s);
                array_push($absences, $absence);
                array_push($descriptions, $description);
            }
            if($absence == LATE){
                $description = $_POST[$keys[$i+1]];
                $description = str_replace("'", "\\'",$description);
				if($description==''){
                    $description = LATE;
                }
                array_push($students, $ssn_s);
                array_push($absences, $absence);
                array_push($descriptions, $description);
            }
			if($absence == EARLY){
                $description = $_POST[$keys[$i+1]];
				$description = str_replace("'", "\\'",$description);
                if($description==''){
                    $description = EARLY;
                }
                array_push($students, $ssn_s);
                array_push($absences, $absence);
                array_push($descriptions, $description);
            }
            if($absence!="-"){
                $at_least_one_absence++;
            }
			
        }
    }

    if($at_least_one_absence==0){
        header("location:teacher_page_absences.php?class=".$cid."&error_no_absences=Error: no absence has been written!");
        die();
    }

    $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
    if (mysqli_connect_errno()) {
        echo "Connessione fallita: ". mysqli_connect_error();
        exit();
    }
	$temp=0;
    /* Select the last abs_id */
    $sql = "SELECT abs_id
                    FROM absences_presences 
					ORDER BY abs_id DESC";
    if(!$result =mysqli_query($conn,$sql)) {
        $msg = "error";
    }
	$row = mysqli_fetch_array($result);
	$temp = $row['abs_id'];
	
    $temp = $temp + 1 ;

	$temp_time = $time;
    for($i=0; count($students)>0 && $i<count($students); $i++){
		    $time =  $temp_time;

        /* Insert marks into DB */
        switch($absences[$i]){
            case EARLY:
                $state = 'PO';
                break;

            case LATE:
                $state = 'PI';
                break;

            case ABSENT:
                $state = 'A';
                break;
            default:
            break;
        }

	if(	$state == 'A'){
		$time = EIGHT;


		while(true){
        $sql = "INSERT INTO absences_presences(abs_id, ssn_s, date, time, state, description, ssn_t, cid)
                        VALUES ('".$temp."','".$students[$i]."','".$date."','".$time."','".$state."','".$descriptions[$i]."','".$ssn_t."','".$cid."')";
       
        if(!$result =mysqli_query($conn,$sql)) {
            die(MSG);
        }
        $temp++;
		switch ($time) {
			case EIGHT:
				$time = NINE;
				break;
			case NINE:
				$time = TEN;
				break;
			case TEN:
				$time = ELEVEN;
				break;
			case ELEVEN:
				$time = TWELVE;
				break;
			case TWELVE:
				$time = THIRTEEN;
				break;
			case THIRTEEN:
				$time = FOURTEEN;
				break;
			case FOURTEEN:
				$time = GOOUT;
                break;	
            default:
            break;
		}
		if($time == GOOUT){
            break;
        }
		}
	}
	else if ($state == 'PI'){
		
		//I delete all was present before
		$sql = "DELETE FROM absences_presences WHERE ssn_s = '".$students[$i]."' AND date='".$date."' AND cid='".$cid."';";
		 
        if(!$result =mysqli_query($conn,$sql)) {
            die(MSG);
        }
		
		//inserisco le assenze per le ore prima
		if($time != EIGHT){
			$time_absences = EIGHT;
			
			while(true){
			$sql = "INSERT INTO absences_presences(abs_id, ssn_s, date, time, state, description, ssn_t, cid)
							VALUES ('".$temp."','".$students[$i]."','".$date."','".$time_absences."','A','-','".$ssn_t."','".$cid."')";
			
			if(!$result =mysqli_query($conn,$sql)) {
            die("Errore nell’inserwefewwegewimento del post ora, riprovare");
			}
				switch ($time_absences) {
					case EIGHT:
						$time_absences = NINE;
						break;
					case NINE:
						$time_absences = TEN;
						break;
					case TEN:
						$time_absences = ELEVEN;
						break;
					case ELEVEN:
						$time_absences = TWELVE;
						break;
					case TWELVE:
						$time_absences = THIRTEEN;
						break;
					case THIRTEEN:
						$time_absences = FOURTEEN;
						break;
					case FOURTEEN:
						$time_absences = GOOUT;
                        break;	
                        default:
                    break;		
				}
				$temp = $temp + 1;
				if($time_absences == $time){
                    break;
                }
			}
		}
		$sql = "INSERT INTO absences_presences(abs_id, ssn_s, date, time, state, description, ssn_t, cid)
                        VALUES ('".$temp."','".$students[$i]."','".$date."','".$time."','".$state."','".$descriptions[$i]."','".$ssn_t."','".$cid."')";
       
        if(!$result =mysqli_query($conn,$sql)) {
            die(MSG);
        }
        $temp++;
		
		while(true){
		
        $sql = "DELETE FROM absences_presences WHERE ssn_s = '".$students[$i]."' AND date='".$date."' AND time='".$time."' AND state='A' AND cid='".$cid."';";
		switch ($time) {
			case EIGHT:
				$time = NINE;
				break;
			case NINE:
				$time = TEN;
				break;
			case TEN:
				$time = ELEVEN;
				break;
			case ELEVEN:
				$time = TWELVE;
				break;
			case TWELVE:
				$time = THIRTEEN;
				break;
			case THIRTEEN:
				$time = FOURTEEN;
				break;
			case FOURTEEN:
				$time = GOOUT;
                break;	
                default:
            break;		
		}
       
        if(!$result =mysqli_query($conn,$sql)) {
            die("Errore nell’inserimento del post, sdsd");
        }
        $temp++;
		
		if($time == GOOUT){
            break;
        }
		}
		
		
		
		
	}
	else if ($state == 'PO'){

		$sql = "INSERT INTO absences_presences(abs_id, ssn_s, date, time, state, description, ssn_t, cid)
                        VALUES ('".$temp."','".$students[$i]."','".$date."','".$time."','".$state."','".$descriptions[$i]."','".$ssn_t."','".$cid."')";
       
        if(!$result =mysqli_query($conn,$sql)) {
            die(MSG);
        }
		$temp = $temp + 1;
		while(true){
			switch ($time) {
					case EIGHT:
						$time = NINE;
						break;
					case NINE:
						$time = TEN;
						break;
					case TEN:
						$time = ELEVEN;
						break;
					case ELEVEN:
						$time = TWELVE;
						break;
					case TWELVE:
						$time = THIRTEEN;
						break;
					case THIRTEEN:
						$time = FOURTEEN;
						break;
					case FOURTEEN:
						$time = GOOUT;
                        break;
                        default:
                    break;			
				}
				if($time == GOOUT){
                    break;
                }
			$sql = "INSERT INTO absences_presences(abs_id, ssn_s, date, time, state, description, ssn_t, cid)
							VALUES ('".$temp."','".$students[$i]."','".$date."','".$time."','A','-','".$ssn_t."','".$cid."')";
			
			if(!$result =mysqli_query($conn,$sql)) {
            die("Errore nell’inserwefewwegewimento del post, riprovare");
			}
				
				$temp = $temp + 1;
				
			}
		
	}


    }
    header("location:teacher_page.php?successful_op=The absences have been successfully inserted");

?>
