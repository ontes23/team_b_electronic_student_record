﻿<?php
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	if($_SESSION["test_in_action"]!='1'){
	include("sidebars.php");
 }
 
 
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard Template · Bootstrap</title>



    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" >


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

  </head>
  <body>

	
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Parent Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
      <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>


<div class="container-fluid">
  <div class="row">
	
    <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen" >
        <ul class="nav flex-column">
		 <li class="nav-item">
			<div class="user-info">
					<div class="image"><img src="photos/user.png" alt="User"></div>
					<div class="detail">
							<h4><?php echo $_SESSION["name_s"]." ".$_SESSION["surname_s"]; ?></h4>
							<small><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></small>
					</div>
			</div>
     </li>
            <?php
					if($_SESSION["test_in_action"]!='1'){
					parent_print_sidebar("true");
				 }
 
            ?>
          </ul>
      </div>
    </nav>

    <main role="main" class="col">
		
      <div class="pt-3 pb-2 mb-3">
      <h2>Notes</h2>
      <div class="table-responsive">
	  
	  
        <table class="table table-striped table-sm">
		<caption></caption>
            <?php
           
				$ssn_child = $_SESSION['ssn_s'];
                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
                $sql = "SELECT * FROM notes WHERE ssn_s = '".$ssn_child."' ORDER BY date DESC ";
                if(!$result = mysqli_query($conn,$sql)) {
                    $msg = "Errore nell’inserimento del post, riprovare";
                    }
                    $temp = mysqli_num_rows($result);

         ?> <thead>
            <tr>
              <th id="col">Teacher</th>
              <th id="col">Note</th>
              <th id="col">Date</th>
            </tr>
          </thead>

        <tbody><?php
           while($row = $result->fetch_assoc()) {
            $ssn_t = $row["ssn_t"];
            $sql = "SELECT * FROM teachers WHERE ssn = '".$ssn_t."'  ";

            
            if(!$result2 = mysqli_query($conn,$sql)) {
              $msg = "Errore nell’inserimento del post, riprovare";
              }
              $r =  $result2->fetch_assoc()["surname"];
      
            echo "<tr>";
	        	echo"<td>" . $r ."</td><td style='color: red'>". $row["description"] ."</td><td>" .$row["date"]."</td>";
            echo "</tr>";    


        }
            
            
        echo"  </tbody>";
          ?>
        </table>
      </div>
	   </div>
    </main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
