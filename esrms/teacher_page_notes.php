<?php
include("sidebars.php");
session_start();
define("CLASS","class");
global $class_id;
if(isset($_GET['class'])) {
    $class_id = $_GET['class'];
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Teacher Write a note</title>

    <!-- Bootstrap core CSS -->
    <link href="css/add_absence_form.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet" >


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	

</head>
<body>

<script>

    function reloadPage_classSelected(){
        var classElement = document.getElementById("inlineFormCustomSelect");
        var className = classElement.options[classElement.selectedIndex].id;
        window.location.replace("teacher_page_notes.php?class=" + className);
    }




</script>


<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Teacher Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-md-block bg-light sidebar">
            <div class="small_screen" >
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"]." ".$_SESSION["surname_u"]; ?></h4>
                            </div>
                        </div>
                    </li>
                    <?php
                    teacher_print_sidebar("true");
                    ?>

                </ul>

            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		 <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Write a note</h2>
                </div>
                <div class="card-body">
                    <form action = "insert_note.php" method = "post">
                        <div class="form-row">
                            <div class="name" >Select Classroom:*</div>
                            <div class="value">
                                 <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                           <select class="custom-select mr-sm-2" name = "class" id="inlineFormCustomSelect" onchange="reloadPage_classSelected()">
                                <?php
                                if($class_id == ""){
                                    echo "<option selected hidden></option>";
                                }
                                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                                $sql = "SELECT DISTINCT c.name, c.cid
																		FROM class c, bridge_class_teachers bct
																		WHERE c.cid = bct.cid
																		AND bct.ssn_t = '" . $_SESSION["user_ssn"] . "'";
                           

                                if (!$result = mysqli_query($conn, $sql)) {
                                    $msg = "Errore nell’inserimento del post, riprovare";
                                }
                                $temp = mysqli_num_rows($result);

                                while ($row = $result->fetch_assoc()) {
                                   
                                    echo "<option id=" . $row['cid'];
                                    if ($row['cid'] == $class_id){
                                        echo " selected";
                                    }
                                    echo ">" . $row['name'] . "</option>";
                                }
                                echo "<input type='hidden' name='cid' value='".$class_id."'>";
                                ?>
                            </select>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        
	

						
						
						 <div><input type="hidden" name="ssn_t" value=<?php print $_SESSION["user_ssn"]; ?>></div>

                    <!-- LIST STUDENT -->
                    <div class="form-group align-items-center">
                        <?php
                        if($class_id != "") {
                            $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                            $sql = "SELECT s.name, s.surname, s.ssn
                                                            FROM students s, bridge_class_students bcs
                                                            WHERE s.ssn = bcs.ssn_s
                                                            AND bcs.cid = '" . $class_id . "'
                                                            ORDER BY s.surname";

                            if (!$result = mysqli_query($conn, $sql)) {
                                $msg = "Errore nell’inserimento del post, riprovare";
                                echo $msg;
                            } else {
                                $temp = mysqli_num_rows($result);

                                echo "<table>";
                                echo "<tr>
                                                                <th>Surname</th>
                                                                <th>Name</th>
                                                                <th>Description</th>
                                                            </tr>";
                                $i = 0;
                                while ($row = $result->fetch_assoc()) {
                                    $ssn = $row['ssn'];
                                    echo "<tr>
                                                                        <td>" . $row['surname'] . "</td>
                                                                        <td>" . $row['name'] . "</td>
                                                                        
                                                                        <td>
                                                                            <input name='description_".$ssn ."' id='description_" . $ssn . "'></td>
                                                                    </tr>";
                                    $i++;
                                }
                                echo "</table>";
                            }
                        }
                        ?>

                        <h7 style="color:red">
                            <?php
                            if(isset($_GET['error_no_notes'])){
                                echo $_GET['error_no_notes'];
                            }
                            ?>
                        </h7>
                    </div>


                    <div class="form-group row " >
                        <?php
                        if(isset($_GET['class'])){
                            echo "<button type=\"submit\" class=\"btn btn-dark align-right\"  >Submit</button>";
                        }
                        ?>
                    </div>
                    
                    </form>
                </div>
            </div>
        </div>
    
   
     
    </main>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="js/dashboard.js"></script></body>
</html>
