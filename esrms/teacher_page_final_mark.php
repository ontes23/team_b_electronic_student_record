<?php
	include("sidebars.php");
	session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Teacher Final Marks</title>

    <!-- Bootstrap core CSS -->
    <link href="css/add_absence_form.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet" >


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Teacher Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-md-block bg-light sidebar">
            <div class="small_screen" >
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"]." ".$_SESSION["surname_u"]; ?></h4>
                            </div>
                        </div>
                    </li>
                    <?php teacher_print_sidebar("true"); ?>

                </ul>

            </div>
        </nav>

        <?php
        $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
        if (mysqli_connect_errno()) {
            mysqli_connect_error();
            $msg = 'Error connect';
            exit();
        }
        $sql = "SELECT c.cid, c.name
                                FROM class c, bridge_class_coordinator bcc
                                WHERE c.cid = bcc.cid
                                AND bcc.ssn_t = '" . $_SESSION['user_ssn'] . "'";
        if(!$result =mysqli_query($conn,$sql)) {
            die("Errore nell’inserimento del post, riprovare");
        }
        if($n = $result->num_rows == 1){
            echo "<main role='main' class='col-md-9 ml-sm-auto col-lg-10 px-4'>
		  <div class='wrapper wrapper--w790'>
            <div class='card card-5'>
                <div class='card-heading'>
                    <h2 class='title'>Final Marks for class ";

            while ($row = $result->fetch_assoc()) {
                $cname = $row['name'];
                $cid = $row['cid'];
                echo $cname;
            }

            echo "</h2>
                </div>
                <div class='card-body' style='text-align: center;'>

                <form action='final_grade_manager.php' method='POST' enctype='multipart/form-data'>

                    <table>
					<caption></caption>
                        <tr>
                            <th id='col' style='border:none;'>
                                    <div class='name'>Term:</div>
                                    <select class='custom-select mr-sm-2' name = 'term' id='term'>
                                        <option value='term_I'>I (Sept. - Jan.)</option>
                                        <option value='term_II'>II (Feb. - Jun.)</option>
                                    </select>
                            </th>
                            <td style='border:none;'>
                                <button type='submit' class='btn btn-dark align-right' style='height:100px; width: 300px;' name='submit' value='download'>
                                    <span data-feather='download'></span><br>
                                    Download template of class data
                                </button>
                            </td>
                        </tr>

                        <tr style='height: 100px;'></tr>

                        <tr style='background-color: white;'>
                            <td style='width:700px; border:none;'>
                                <div class='name'>Attachment:</div>
                                <input type='file' name='file' id='file' class='btn--red'>
                                <h4 id= 'msg' style='min-height: 50px;";

            if (isset($_GET['result'])){
                echo ' color: ';
                if($_GET['result']=='ok'){
                    echo 'green';
                } else{
                    echo 'red';
                }
                echo ';\'>';
                echo $_GET['message'];
            }
            else{
                echo '\'>';
            }

            echo "</h4
            </td>
                    <td style='border:none;'>
                        <button type='submit' class='btn btn-dark align-right' style='height:100px; width: 300px;' name='submit'  value='upload'>
                            <span data-feather='upload'></span><br>
                            Upload template with final marks
                        </button>
                    </td>
                </tr>
            </table>

                <input type='hidden' name='className' value='"
            . $cname . "'>
            <input type='hidden' name='classId' value='" . $cid . "'>
            
            </form>
                </div>
            </div>
        </div>
    </main>";
        }
        else{
            echo "<h1 style='color:red;'>Error: you do not have a class to coordinate.</h1>";
        }

        ?>

    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="js/dashboard.js"></script></body>
</html>
