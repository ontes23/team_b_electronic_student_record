<?php
	if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
if (mysqli_connect_errno()) {
    //echo "Connessione fallita: ".
    mysqli_connect_error();
    exit();
}
$ssn_t= $_SESSION['user_ssn'];
$hour = $_POST['hour'];
$day = $_POST['day'];

$sql = "DELETE FROM meetings WHERE ssn_t =? AND hour =? AND day=?";
$stmt = mysqli_stmt_init($conn);
if(!mysqli_stmt_prepare($stmt,$sql)){
    exit();
}else{

    mysqli_stmt_bind_param($stmt, "sss",$ssn_t,$hour,$day);
    mysqli_stmt_execute($stmt);
    echo "<p style='color: green'><strong>Delete successfully</strong></p>";
}
