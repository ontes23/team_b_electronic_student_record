<?php
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	$dir = "final_mark";
define("SUBMIT","submit");
	$conn = connect();
	if (isset($_POST[SUBMIT])){
			if ($_POST[SUBMIT]=="download"){
				# Call download
				download_file($dir);
			}else if ($_POST[SUBMIT]=="upload"){
				# Call upload
				upload_file($dir);
			}
	}else{ 
		die();
	}


	function download_file($dir)
	{
		# create file and send
		$cid = $_POST["classId"];
		$cname = $_POST["className"];
		$term_string = $_POST["term"];
		
		$conn = connect();

		# retrieve avg for the subject
		$stmt = $conn->prepare("SELECT AVG(mark) AS avg FROM marks WHERE ssn_s = ? AND subject = ?;");
		mysqli_stmt_bind_param($stmt, "ss", $ssn_s, $subject);
		
		# retrieve list of student
		$sql1 = "SELECT ssn, s.name, s.surname FROM students AS s, bridge_class_students AS bcs, class AS c 
						WHERE s.ssn = bcs.ssn_s AND bcs.cid = c.cid AND c.cid = '".$cid."' ORDER BY surname;";
		
		# retrieve list of subject
		$sql2 = "SELECT * FROM bridge_class_teachers AS bct WHERE bct.cid = '".$cid."' ORDER BY subject; ";
		
		# retrieve avg for the subject
		$stmt = $conn->prepare("SELECT AVG(mark) AS avg FROM marks WHERE ssn_s = ? AND subject = ?;");
		mysqli_stmt_bind_param($stmt, "ss", $ssn_s, $subject);
		
		if(!$result2 = mysqli_query($conn,$sql2)){
			$msg = "Errore nell'inserimento in sql2, riprovare";
			die($msg);
		}
		
		$subject_array = array();
		while($row = $result2->fetch_assoc()){
			array_push($subject_array, $row["subject"]);
		}
	

		$term_array = explode('_', $term_string);
		$term = $term_array[1];

		# open file
		$fileName = $cname . "_" . $term_string . "_final_grades.txt";
		$pathFile = $dir."/".$fileName;
		$file = fopen($pathFile, "w") or die("Unable to open file!");
		fwrite($file, "# TEAM B's template for final grades\n");
		fwrite($file, "Class ID: ".$cid."\n");
		fwrite($file, "Class name: ".$cname."\n");
		fwrite($file, "Term: ".$term."\n");

		fwrite($file, "\n# The first line of each paragraph shows the subject.\n");
		fwrite($file, "# Each of the following lines is filled with info about a student, in this format:\n");
		fwrite($file, "# [SSN];[Surname];[Name];[Arithmetic mean of grades];[Final grade (to be inserted)]\n");
		
		foreach ($subject_array as $sub){
			fwrite($file, "\n".$sub."\n");
			
			if(!$result1 = mysqli_query($conn,$sql1)){
				$msg = "Errore nell'inserimento in sql1, riprovare";
				die($msg);
			}
			
			while($row = $result1->fetch_assoc()){
				
				$ssn_s = $row['ssn'];
				$surname = $row['surname'];
				$name = $row['name'];
				
				
				# calcolo avg per ogni subject
				$sql3 = "SELECT AVG(mark) AS avg FROM marks WHERE ssn_s = '".$ssn_s."' AND subject = '".$sub."';";
			
				if(!$result3 = mysqli_query($conn,$sql3)){
					$msg = "Errore nell'inserimento in sql3, riprovare";
					die($msg);
				}
				while($row3 = $result3->fetch_assoc()){
					$avg = $row3["avg"];
					if ( empty($avg) ){
						$avg = "EMPTY";
					}else{
						$avg = number_format($avg, "3");
					}
				}
				echo "avg: ".$avg."<br>";
				# Cid, term, subject, ssn_s, surname, name, avg, mark
				$riga = $ssn_s.";".$surname.";".$name.";".$avg.";---\n";
				fwrite($file, $riga);
				echo $riga."<br>";
			}
		}
		fclose($file);

    if(file_exists($pathFile)){
				header("Content-Type: application/octet-stream");
				header('Content-Disposition: attachment; filename=' . $fileName);
				ob_clean();
				flush();
				readfile($pathFile);
				exit();
		}else{
				// Redirect to the previous page without any $_GET params (to avoid their propagation)
				header("location:".strtok($_SERVER['HTTP_REFERER'],'?')."?error=Error: the file " . $fileName . " does not exist!");
				die();
		}

	}		


	function upload_file($dir)
	{
		$filename = $_FILES['file']['name'];

		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		$target_dir = "final_mark/";
		if (!isset($_SESSION['file'])) {
			$target_file = $target_dir . $filename;
		} else {
			$target_file = $_SESSION['file'];
		}

		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
		if (mysqli_connect_errno()) {
			// Connection failed
			mysqli_connect_error();
			$msg = 'Error connect';
			myEnd($msg);
		}

		if (verify_post()) {

			if (!isset($_SESSION['test'])) {
				if (!is_uploaded_file($_FILES["file"]["tmp_name"])) {
					myEnd("result=ko&message=Error: no file selected.");
				}
				if (!move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
					myEnd("result=ko&message=Error during upload.");
				}
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if($ext!="txt"){
					myEnd("result=ko&message=Error: the file must be a .txt.");
				}
			}

			load_finalMarks($target_file, $conn);
			$msg = 'result=ok&message=The final marks have been successfully uploaded!';
		} else {
			$msg = 'result=ko&message=Empty post';
		}
		myEnd($msg);
	}


	function connect()
	{
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
		if (mysqli_connect_errno()) {
				echo "Connessione fallita: " . mysqli_connect_error();
				exit();
		}
		return $conn;
	}



	function myEnd($msg)
	{
		if (!isset($_SESSION['test'])) {
			header('Location: teacher_page_final_mark.php?' . $msg);
			die();
		}

	}

	function verify_post()
	{
		if (isset($_POST[SUBMIT])) {
			return true;
		}
		return false;

	}

	function load_finalMarks($target_file, $conn)
	{
		$subject = '';
		$term = '';
		$cid = '';
		$ssn_t = $_SESSION['user_ssn'];
		$error = '0';
		$n_line = 1;
		mysqli_begin_Transaction($conn);

		$handle = fopen($target_file, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				// Process the line read
				if($line[0]!='#' && $line!="\r\n" && $line!="\n"){		// Keep out comments and empty lines
					$line_array = explode(';', $line);
					if(sizeof($line_array)==1){							// Class, term or subject (CTS) info
							$cts_array = explode(':', $line);
							if(sizeof($cts_array)==2){					// Class or term info
								$cts_array[1] = preg_replace('/\s+/', '', $cts_array[1]);
								switch($cts_array[0]){

									case "Class ID":
										// Save class ID
										$cid = $cts_array[1];
										// Check the bond class-coordinator teacher
										$sql = "SELECT *
												FROM bridge_class_coordinator
												WHERE cid = '" . $cid . "'
												AND ssn_t = '" . $ssn_t . "'";
										if(!$result = mysqli_query($conn,$sql)) {
											die("An error occurred, please retry.");
										}
										if($result->num_rows!=1){
											myEnd("result=ko&message=Error (line " . $n_line . "): no match class-coordinator found.");
										}

										break;

									case "Class name":
										// Save class name
										$cname = $cts_array[1];
										// Check the bond class ID-class name
										$sql = "SELECT *
												FROM class
												WHERE cid = '" . $cid . "'
												AND name = '" . $cname . "'";
										if(!$result = mysqli_query($conn,$sql)) {
											mysqli_rollback();
											die(mysqli_error($conn));
										}
										if($result->num_rows!=1){
											myEnd("result=ko&message=Error (line " . $n_line . "): no match class ID-class name found.");
										}
										
										break;

									case "Term":
										// Save term
										$term = $cts_array[1];
										
										// Check if the term is correct
										if($term!='I' && $term!='II')
											myEnd("result=ko&message=Error (line " . $n_line . "): incorrect term (must be I or II).");
										break;

									default:
										myEnd("result=ko&message=Error (line " . $n_line . "): incorrect line format.");
										break;
								}
							}
							else{                      					// Subject info
								// Save subject
								$subject = rtrim($line);
								// Check the bond class ID-subject
								
								$sql = "SELECT *
										FROM bridge_class_teachers
										WHERE cid = '" . $cid . "'
										AND subject = '" . $subject . "'";
								if (!$result = mysqli_query($conn, $sql)) {
									mysqli_rollback();
									die(mysqli_error($conn));
								}
								if ($result->num_rows != 1) {
									
									die();
									myEnd("result=ko&message=Error (line " . $n_line . "): no match class-subject found.");
								}

							}
					}
					else if(sizeof($line_array)==5){		// Student info

						// Save student info
						$ssn_s = $line_array[0];
						$surname = $line_array[1];
						$name = $line_array[2];
						$mean = $line_array[3];
						$final = rtrim($line_array[4]);

						// Check student basic info
						$sql = "SELECT *
								FROM students
								WHERE ssn = '" . $ssn_s . "'
								AND surname = '" . $surname . "'
								AND name = '" . $name . "';";
						if(!$result = mysqli_query($conn,$sql)) {
							mysqli_rollback();
							die(mysqli_error($conn));
						}
						if($result->num_rows!=1){
							myEnd("result=ko&message=Error (line " . $n_line . "): incorrect student info.");
						}

						// Check correctness of final grade
						for($i=0; $i<strlen($final); $i++){
							if(!is_numeric($final[$i])){
								myEnd("result=ko&message=Error (line " . $n_line . "): the final grade must be an integer between 0 and 10.");
							}
						}
						$final_int = (int)$final;
						if($final_int<0 || $final_int>10){
							myEnd("result=ko&message=Error (line " . $n_line . "): the final grade must be an integer between 0 and 10.");
						}
						// Get greatest m_id from final_marks (only for the first time)
						if(!isset($m_id)) {
							$sql = "SELECT MAX(m_id) AS m_id
									FROM final_marks;";
							if(!$result = mysqli_query($conn,$sql)) {
								mysqli_rollback();
								die(mysqli_error($conn));
							}
							while($row = $result->fetch_assoc()){
								$m_id = $row['m_id'];
							}
							if(!isset($m_id)){
								$m_id = -1;
							}
						}
						$m_id++;

						// Check if a row already exists in final_marks
						$sql = "SELECT *
								FROM final_marks
								WHERE ssn_s = '" . $ssn_s . "'
								AND subject = '" . $subject . "'
								AND ssn_t = '" . $ssn_t . "'
								AND term = '" . $term . "';";
						if(!$result = mysqli_query($conn,$sql)) {
							mysqli_rollback();
							die(mysqli_error($conn));
						}
						if($result->num_rows==1){
							// UPDATE
							$sql = "UPDATE final_marks
									SET mark = '". $final_int ."'
									WHERE ssn_s = '" . $ssn_s . "'
									AND subject = '" . $subject . "'
									AND ssn_t = '" . $ssn_t . "'
									AND term = '" . $term . "';";
							if(!$result = mysqli_query($conn,$sql)) {
								mysqli_rollback();
								die(mysqli_error($conn));
							}
						}
						else{
							// INSERT
							$sql = "INSERT INTO final_marks(m_id, ssn_s, subject, mark, ssn_t, term)
 									VALUES ('".$m_id."','".$ssn_s."','".$subject."','".$final_int."','".$ssn_t."','".$term."')";
							if(!$result = mysqli_query($conn,$sql)) {
								mysqli_rollback();
								die(mysqli_error($conn));
							}
						}
					}
					else{
						myEnd("result=ko&message=Error (line " . $n_line . ")");
					}
				}
				$n_line++;
			}
			fclose($handle);
		} else {
			myEnd("result=ko&message=Error: could not read " . basename($target_file) . ".");
		}

		mysqli_commit($conn);
		return;

	}
?>
