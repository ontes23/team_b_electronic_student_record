<?php
session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">

	<meta name="viewport" content="width=device-width, initial-scale=1" />

</head>
<body data-spy="scroll" data-target="#navbar-example" style="background-color:#343a40!important;">

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">ESRMS</a>

</nav>



<div id="carousel" class="carousel slide" data-ride="carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <div class="item active" style="background-image: url(photos/school.jpg);">
      <div class="carousel-caption animated fadeInUp" >
            <form method = 'post' action = 'validation.php' class="form-signin"  style="text-align:center;">
			<br>
      <h1 class="h3 mb-3 font-weight-normal">Please Login</h1><br>
	   <?php
	 	define("ERRCREDENTIALS","error_credentials");
	   if(isset($_POST[ERRCREDENTIALS]) && $_POST[ERRCREDENTIALS]=="Wrong username and password"){
				
								echo "<label class= 'error-message'>".$_POST[ERRCREDENTIALS]."</label>";
							
							
	   }
		  ?>
      <label for="inputEmail" class="sr-only">Email address</label>
	  <div align="center">
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name = "email" required autofocus><br>
      </div>
	  <label for="inputPassword" class="sr-only">Password</label>
      <div align="center">
	  <input type="password" id="inputPassword" class="form-control" placeholder="Password" name = "password" required>
      </div>
	  <div class="checkbox mb-3">
      </div>
			<span>Choose your profile:</span><br><br>
			
				<div class="btn-group btn-group-toggle flex-wrap" data-toggle="buttons">
					
					<label class="btn btn-info active">
						<input type="radio" name="role" id="option1"  value="parents" autocomplete="off" checked> Parent
					</label><span> </span>
					

					<label class="btn btn-info">
						<input type="radio" name="role" id="option2"  value="teachers" autocomplete="off"> Teacher
					</label><span> </span>

					<label class="btn btn-info">
						<input type="radio" name="role" id="option3"  value="administrators" autocomplete="off"> Admin Officer 
						</label><span> </span>
					

					<label class="btn btn-info">
						<input type="radio" name="role" id="option4"  value="sysadmins" autocomplete="off"> SysAdmin  
					</label>
					
					
					
				</div>		
				<br><br>

      <button class="btn btn-success form-control" type="submit">Login</button>
     <br><br>
    </form>
      </div>
    </div>
  </div>

	

</div>




<script src="js/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script>
	$('body').scrollspy({ target: '#navbar-example' });

	$('[data-toggle="popover"]').popover();

	$(".navbar-collapse ul li a[href^='#']").on('click',function(e){

		target = this.hash;
		e.preventDefault();

		$('html,body').animate({
			scrollTop : $(this.hash).offset().top
		}, 600, function(){
			window.location.hash = target;
		});

	});

</script>

</body>
</html>
