<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <title>Administrative Officer Page</title>

<?php
define("SSNS","ssn_s");
define("NAMES","name_s");
define("SURNAMES","surname_s");
define("RESULT","result");
 include("sidebars.php");
 session_start();
 if(isset($_POST[SSNS])){
 $_SESSION[SSNS] = $_POST[SSNS];
 }
 if(isset($_POST[NAMES])){
 $_SESSION[NAMES] = $_POST[NAMES];
 }
 if(isset($_POST[SURNAMES])){
 $_SESSION[SURNAMES] = $_POST[SURNAMES];
 }
 global $class_id;
if(isset($_GET['class'])) {
    $class_id = $_GET['class'];
}

?>

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" >
      <link href="css/timetable_week.css" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
			
			table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
				
    </style>
	
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    
    <!-- Main CSS-->
    <link href="css/add_student_form.css" rel="stylesheet" media="all">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      
	
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		
		<script>

    function reloadPage_classSelected(){
        var classElement = document.getElementById("class");
        var className = classElement.options[classElement.selectedIndex].id;

        window.location.replace("administrator_add_timetable.php?class=" + className);
    }

</script>

  </head>
  <body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Admin Officer Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>

<div>

<div class="container-fluid">
  <div class="row">
   <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen" >
        <ul class="nav flex-column">
		 <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></h4>
																<small> </small>
                            </div>
                        </div>
           </li>
            <?php
                administrator_print_sidebar("true");
            ?>
        </ul>


      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
			<div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">load timetables</h2>
                </div>
                <div class="card-body">
																	 
									<form action="add_timetable.php" method='POST' enctype="multipart/form-data" id='add_timetablef'>
										<div class="form-row">
												<div class="name">Select a file:</div>
												
												<div class="value">
														<input type="file" name="file" id='file' class='btn--red'>
												</div>
										</div>
										
										<div>
											<h4 id= "msg" style="min-height: 50px;
                                            <?php
												if (isset($_GET[RESULT])){
												    echo " color: ";
												    if($_GET[RESULT]=="The timetables have been successfully uploaded!"){
                                                        echo "green";
                                                    }
												    else{
                                                        echo "red";
                                                    }
												    echo ";\">";
													echo $_GET[RESULT];
												}
												else{
                                                    echo "\">";
                                                }
											?></h4>
											<input type="submit" name='submit' class="btn btn-dark" value='Upload'>
										</div>
										
									</form>
								</div>
            </div>
      </div>
			
			<div class="pt-3 pb-2 mb-3 border-bottom">
                <h2>View timetables</h2>


                <form action = "insert_mark_record.php" method = "post">
                    <div class="form-row align-items-center">
                        <!-- SELECT CLASS -->
                        <div class="col-auto my-1">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Select Classroom</label>
                            <select class="custom-select mr-sm-2" name = "class" id="class" onchange="reloadPage_classSelected()">
                                <?php
																if (!isset($class_id)){
																	echo "<option selected>-</option>";
																}
                                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                                $sql = "SELECT DISTINCT c.name, c.cid
																		FROM class c;";
                               
                                if (!$result = mysqli_query($conn, $sql)) {
                                    $msg = "Errore nell’inserimento del post, riprovare";
                                }
                                $temp = mysqli_num_rows($result);

                                while ($row = $result->fetch_assoc()) {
                                    echo "<option id=" . $row['cid'];
                                    if ($row['cid'] == $class_id){
                                        echo " selected";
                                    }
                                    echo ">" . $row['name'] . "</option>";
                                }
                                echo "<input type='hidden' name='cid' value='".$class_id."'>";
                                ?>
                            </select>
                        </div>

                        

                    </div>







            </div>
			<table class="timetable_week">
			<caption></caption>
                <thead>
                    <?php
                        $weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                        $hours = ["08:00","09:00","10:00","11:00","12:00","13:00"];
						?>
                        <th scope="col"><?php
                        for($i=0; $i<count($weekdays); $i++){
                            ?><th scope="col"><?php echo $weekdays[$i]?></th><?php
                        }
						?></th>
                    
                </thead>
                <tbody>
                    <?php
                        $sql = "SELECT ts.surname as surname, t.hour, t.day, t.subject
                                FROM timetables t, class c, teachers ts
                                WHERE t.cid = c.cid AND t.cid='".$class_id."'
																	AND ts.ssn = t.ssn_t
																ORDER BY t.hour, t.day";

                        if (!$result = mysqli_query($conn, $sql)) {
                            $msg = "Errore nell’inserimento del post, riprovare";
                            die($msg);
                        }

                        $rows = array();

                        while ($row = $result->fetch_assoc()) {
                            
                            array_push($rows, $row);
                        }

                        $k = 0;

                        for($i=0; $i<6; $i++){
                            echo "<tr><td><strong>".$hours[$i]."</strong></td>";
                            for($j=0; $j<6; $j++){
                                echo "<td";
                               
                                    if($k < count($rows) && $rows[$k]['day']==$weekdays[$j] && $rows[$k]['hour']==$hours[$i]){
                                        echo " class='lesson'><strong>". $rows[$k]['subject'] . "</strong>";
                                        echo "<br>" . $rows[$k]['surname'] . "</td>";
                                        $k++;
                                    }
                                    else{
                                        echo "></td>";
                                    }
                            }
                            echo "</tr>";
                        }


                    ?>
                </tbody>
            </table>
						<br><br>
     
    </main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
