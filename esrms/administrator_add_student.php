<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    
    
    <title>Administrative Officer Page</title>

<?php
 include("sidebars.php");
 session_start();

?>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    
    <!-- Main CSS-->
    <link href="css/add_student_form.css" rel="stylesheet" media="all">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <script>

 

  $(document).ready(function() { 
	  $("#add_stud").submit(function(event) {
		  var studentSSNLen  = $("#studentSSN").val().length;
		  var parent1SSNLen  = $("#parent1SSN").val().length;
		  var parent2SSNLen = $("#parent2SSN").val().length;
		  var studentGenderLen = $("input:radio[name=studGender]:checked").val().length;
		  var studentNameLen = $("#studentName").val().length;
		  var studentSurnameLen  = $("#studentSurname").val().length;
		  var studentAdressLen  = $("#studentAdress").val().length;
		  var studentPhoneLen = $("#studentPhone").val().length;
		  var studentClassLen = $("#studentClass").val().length;

	      
	      var studentSSN = $("#studentSSN").val();
		  var parent1SSN = $("#parent1SSN").val();
		  var parent2SSN = $("#parent2SSN").val();
	      var studentName = $("#studentName").val();
	      var studentSurname  = $("#studentSurname").val();
	      var studentAdress = $("#studentAdress").val();
	      var studentPhone  = $("#studentPhone").val();
	      var studentClass = $("#studentClass").val();
		  var studentGender = $("input:radio[name=studGender]:checked").val();

	      var addstudent = $("#addstudent").val();

      event.preventDefault(); //break the signuppost,move on there 
      
      if(studentSSNLen == 0  || studentNameLen == 0 || studentSurnameLen == 0 || parent1SSNLen == 0 || studentClassLen == 0 ){
          $(".message").text("Please fill the form!");
          $(".message").addClass("error-message");
          
        }

      else{
      $(".message").load("add_student.php", { 
    	  studentSSN : studentSSN,
		  parent1SSN : parent1SSN,
		  parent2SSN : parent2SSN,
    	  studentName: studentName,
    	  studentSurname: studentSurname,
    	  studentAdress: studentAdress,
    	  studentPhone: studentPhone,
    	  studentGender: studentGender,
		  studentClass: studentClass,
    	  addstudent : addstudent
          });
      }
      
	  }); //when you click the login button
		
  });
  
	
	

  
  
  
  </script> 
  <noscript> Sorry: Your browser does not support or has disabled javascript </noscript>
 
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Admin Officer Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>

<div>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen" >
        <ul class="nav flex-column">
		 <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></h4>
                            </div>
                        </div>
           </li>
            <?php
                administrator_print_sidebar("true");
            ?>
        </ul>

      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		
				
			<div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Student Registration List</h2>
                </div>
                <div class="card-body">
																	 
									<form action="add_student_multi.php" method='POST' enctype="multipart/form-data" id='add_stud_multi'>
										<div class="form-row">
												<div class="name">Select a file:</div>
												
												<div class="value">
														<input type="file" name="myFile" id='myFile' class='btn--red'>
												</div>
										</div>
										
										<div>
											<h4 style="min-height: 50px;" id= "msg"><?php
												if (isset($_SESSION['msg'])){
													echo $_SESSION['msg'];
												}
											?></h4>
											<input type="submit" name='submit' class="btn btn-dark" value='Submit'>
										</div>
										
									</form>
								</div>
            </div>
      </div>
		
		
     <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">student Registration Form</h2>
                </div>
                <div class="card-body">
                    <form action="add_student.php" method="POST" id='add_stud'>
                       
                        <div class="form-row">
                            <div class="name" >Name:*</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" id="studentName">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Surname:*</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" id="studentSurname">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Address:</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5"  id="studentAdress">
                                </div>
                            </div>
                        </div>
                         <div class="form-row m-b-55">
                            <div class="name">SSN:*</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="studentSSN">
                                            <label class="label--desc">SSN Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-row m-b-55">
                            <div class="name">SSN parent 1:*</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="parent1SSN">
                                            <label class="label--desc">SSN Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-row m-b-55">
                            <div class="name">SSN parent 2:</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="parent2SSN">
                                            <label class="label--desc">SSN Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>						
                        <div class="form-row m-b-55">
                            <div class="name">Phone:</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="studentPhone">
                                            <label class="label--desc">Phone Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row p-t-20">
                            <label class="label label--block">Gender</label>
                            <div class="p-t-15">
                                <label class="radio-container m-r-55">Female
                                    <input type="radio" name="studGender" id="studentGender" value="F" checked>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="radio-container">Male
                                    <input type="radio" name="studGender" id="studentGender" value="M">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
						
                        <div class="form-row m-b-55">
                            <div class="name">Student Class:</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="studentClass">
                                            <label class="label--desc">Student Class</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
													<h4 style="min-height: 50px;" class= "message"></h4>
                            <button id="addstudent" class="btn btn-dark" type="submit">Submit</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
   
     
    </main>
  </div>
</div>

<!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>



      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
