<?php
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	if($_SESSION["test_in_action"]!='1'){
	include("sidebars.php");
 }
 
 
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>ESRMS SYSTEM</title>



    <!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/parent_homeworks.css" rel="stylesheet" />
		<link href='css/timetable.css' rel='stylesheet' />
		<link href='css/timetablegrid.css' rel='stylesheet' />
		<script src='js/timetable.js'></script>
		<script src='js/timetable2.js'></script>
		<script src='js/daygrid.js'></script>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
	  
			#calendar {
				width: auto;
				height: 800px;
				max-width: 1000px;
				margin: 0 auto;
			}
    </style>
		
		<script>

	document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      defaultDate: '2019-12-01',
      editable: true,
      eventLimit: true,
        events:
            [
        <?php
        define("SSNS","ssn_s");
        define("SSNT","ssn_t");
        define("MSG","Errore nell’inserimento del post, riprovare");
        $ssn_child = $_SESSION[SSNS];
        $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

        $sql = "SELECT * FROM bridge_class_students  WHERE ssn_s=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
                    // TO FILL
        } else {
            mysqli_stmt_bind_param($stmt, "s", $_SESSION[SSNS]);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result)) {
                $cid = $row['cid'];
            }
        }

        $sql = "SELECT * FROM homeworks WHERE cid = '" . $cid . "' ORDER BY deadline DESC  ";
        if (!$result = mysqli_query($conn, $sql)) {
            $msg = MSG;
        }
        $temp = mysqli_num_rows($result);

        while ($row = $result->fetch_assoc()) {
            $ssn_t = $row[SSNT];
            $current_date = date("Y-m-d");
            $deadline = $row["deadline"];
            $homework_description = $row["description"];
            $subject = $row["subject"];
            $ssn_t = $row[SSNT];
            $date = $row["date"];
            $is_green = false;
            if($current_date <= $deadline){
                $is_green = true;
            }else{
                $is_green = false;
            }
            $sql = "SELECT * FROM teachers WHERE ssn = '" . $ssn_t . "'  ";


            if (!$result2 = mysqli_query($conn, $sql)) {
                $msg = MSG;
            }
            while ($row2 = $result2->fetch_assoc()) {
                $teacher_name = $row2["name"];
                $teacher_surname = $row2["surname"];
            }


            echo"
                {
                  title: '$subject',
                  start: '$deadline'
                },";

        }

            echo"{}
            
              ]
            });";
    ?>


        $("#a").removeClass("fc-event,fc-event-dot");
       // $("td").removeClass(".fc-event-dot");
            $("td").addClass("green_homework");
        calendar.render();
  });

	</script>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

  </head>
  <body>

	
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Parent Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>


<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="small_screen">
        <ul class="nav flex-column">
					<li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_s"]." ".$_SESSION["surname_s"]; ?></h4>
                                <small><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></small>
                            </div>
                        </div>
          </li>
					<?php
						if($_SESSION["test_in_action"]!='1'){
							parent_print_sidebar("true");
						}
			
							
					?>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        <p id="error" style="font-weight: bold; color: red;">
            <?php
            if(isset($_GET['error'])){
                echo $_GET['error'];
            }
            ?>
        </p>
        <h2>Homework</h2>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 id="panel1" class="panel-title">
                        <a  id="titlee" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            List
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <table class="table table-striped table-sm">
							<caption></caption>


                            <?php

                            $ssn_child = $_SESSION[SSNS];
                            $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                            $sql = "SELECT * FROM bridge_class_students  WHERE ssn_s=?";
                            $stmt = mysqli_stmt_init($conn);

                            if(!mysqli_stmt_prepare($stmt,$sql)){
                                                                    //TO FILL
                            }   
                            else {
                                mysqli_stmt_bind_param($stmt, "s",$_SESSION[SSNS]);
                                mysqli_stmt_execute($stmt);
                                $result = mysqli_stmt_get_result($stmt);

                                if($row = mysqli_fetch_assoc($result))
                                {
                                    $cid=  $row['cid'];
                                }
                            }

                            $sql = "SELECT * FROM homeworks WHERE cid = '".$cid."' ORDER BY deadline DESC  ";
                            if(!$result = mysqli_query($conn,$sql)) {
                                $msg = MSG;
                            }
                            $temp = mysqli_num_rows($result);
                            ?><thead>
					            <tr>
					              <th scope="col">Date</th>
					              <th scope="col">Subject</th>
					               <th scope="col">Teacher</th>
					              <th scope="col">Description</th>
								  <th scope="col">Attachment</th>
					              <th scope="col">Deadline</th>
					            </tr>
					          </thead>
                            <tbody><?php
                            while($row = $result->fetch_assoc()) {
                                $ssn_t = $row[SSNT];
                                $current_date =  date("Y-m-d");
                                $deadline = $row["deadline"];
                                $homework_description = $row["description"];
                                $subject = $row["subject"];
                                $ssn_t = $row[SSNT];
                                $date = $row["date"];
                                $sql = "SELECT * FROM teachers WHERE ssn = '".$ssn_t."'  ";


                                if(!$result2 = mysqli_query($conn,$sql)) {
                                    $msg = MSG;
                                }
                                while($row2 = $result2->fetch_assoc()) {
                                    $teacher_name =  $row2["name"];
                                    $teacher_surname =  $row2["surname"];
                                }




                                echo "<tr>";
                                echo"<td>" . $date ."</td>
                                        <td>".$subject ."</td>
                                        <td>".  substr($teacher_name, 0, 1). ". " . $teacher_surname ."</td>
                                        <td>". $homework_description ."</td>
																				<td><a target='_blank' href='download.php?id=".$row['aid']."'>".$row['aname']."</a></td>
                                        <td style='font-weight:bold; color:";
                                            if($current_date <= $deadline){
                                                echo "green";
                                            }else{
                                                echo 'red';
                                            }
                                            echo"'>".$deadline."</td>";
                                echo "</tr>";


                            }


                            echo"  </tbody>";




                            ?>


                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 id="panel2" class="panel-title">
                        <a id="titlee" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Calendar
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
    
                        <div id='calendar'></div>
                        
                    </div>
                </div>
            </div>
        </div>

    </main>

	</div>
</div>
	

    

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="js/dashboard.js"></script>
	</body>
</html>
