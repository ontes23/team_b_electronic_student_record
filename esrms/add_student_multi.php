<?php
# read received file and for each line call add_student.php
echo "hello<br>";
session_start();
define("MYF","myFile");
define("TMP","tmp_name");
define("SITE","Location: signup.php?error=sqlerror");
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES[MYF]["name"]);
$uploadOk = 1;
$msg = "";
echo $target_file."<br>";
echo $_FILES[MYF]['type']."<br>";
echo $_FILES[MYF][TMP]."<br>";

$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
		if (mysqli_connect_errno()) {
			//echo "Connessione fallita: ".
			mysqli_connect_error();
			$msg = 'Error connect';
			exit();
		}
		
if (verify_post($_POST["submit"]))
{
	// read file
	
	if(!is_uploaded_file($_FILES[MYF][TMP]) || $_FILES[MYF]["error"]>0)  
  {  
		$msg = 'Problem during upload!';
		echo $msg;  
		myEnd($msg);
  } 
	if(!move_uploaded_file($_FILES[MYF][TMP], $target_file))  
  { 
		$msg = 'Error during upload';
    echo $msg;
		myEnd($msg);
  } 
	
	//
	if ( ($line = load_student($target_file, $conn) )  == 1)
	{
		$msg = 'ok';
		echo $msg;
	} else {
		$msg = 'Error during load in line '.$line;
		echo $msg;
	}
	
} else {
	$msg = 'empty post';
	echo $msg;
}
myEnd($msg);

##### functions #####################

function myEnd($msg)
{
	$_SESSION['msg'] = $msg;
	echo $msg;
	header('Location: administrator_add_student.php');
}

function verify_post($post)
{
	if (isset($_POST["submit"])){
		return true;
	}
		return false;
	
}

function load_student($target_file, $conn)
{
	$error = '0';
	$line = '0';
	$myfile = fopen($target_file, "r") or die("Unable to open file!");
		// Output one line until end-of-file
	mysqli_begin_Transaction($conn);
	while (($data = fgetcsv($myfile, 1000, ";")) !== FALSE  && $error == 0) 
  {
		$line++;
		print_r($data);
		echo "<br>";
					
		$studentName = $data[0];
		$studentSurname = $data[1];
		$studentAdress = $data[2];
		$studentSSN = $data[3];
		$parent1 = $data[4];
		$parent2 = $data[5];
		$studentPhone = $data[6];
		$studentGender = $data[7];
		$studentClass = $data[8];
		
		if (!verify_parent($parent2, $conn)){
			$parent2 = '';
		}
		if (verify_student($studentSSN, $conn) == 0 || verify_parent($parent1, $conn)== 0){ 	
			$error = '1';
			echo "esci";
		} else {
			// insert student
			$sql = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values(?,?,?,?,?,?)";
      $stmt = mysqli_stmt_init($conn);
     	if (!mysqli_stmt_prepare($stmt,$sql)) 
     	{
				header(SITE);
				exit();
     	} else {	
				mysqli_stmt_bind_param($stmt, "ssssss",$studentSSN,$studentSurname,$studentName,$studentAdress,$studentPhone,$studentGender); //s string we are passing string
				mysqli_stmt_execute($stmt);
		  }
			// Obtain the CID of the class
			
			$sql = "SELECT cid FROM `class` WHERE name = '$studentClass'";
			
			if(!$result =mysqli_query($conn,$sql)) {
				$msg = "Errore nell’inserimento del post, riprovare";
				echo $msg;
			}
			$r = mysqli_fetch_array($result);

			// insert into class
			$sql = "INSERT INTO bridge_class_students(cid,ssn_s) values(?,?)";
		  $stmt = mysqli_stmt_init($conn);
		  if (!mysqli_stmt_prepare($stmt,$sql)) 
		  {
				header(SITE);
				exit();
		  } else {
				mysqli_stmt_bind_param($stmt, "ss",$r['cid'],$studentSSN); //s string we are passing string
				mysqli_stmt_execute($stmt);		   
		  }
			
			// insert bridge parent_student
			$sql = "INSERT INTO bridge_parents_students(ssn_p,ssn_s) values(?,?)";
		  $stmt = mysqli_stmt_init($conn);
		  if (!mysqli_stmt_prepare($stmt,$sql)) 
		  {
				header(SITE);
				exit();
			} else {
				mysqli_stmt_bind_param($stmt, "ss",$parent1,$studentSSN); //s string we are passing string
				mysqli_stmt_execute($stmt);
				if($parent2 != ''){
					mysqli_stmt_bind_param($stmt, "ss",$parent2,$studentSSN); //s string we are passing string
					mysqli_stmt_execute($stmt);
				}
		   	  echo "<span>Success</span>";
			}
		}			
	}
	fclose($myfile);

	if($error == '0'){
		mysqli_commit($conn);
		return true;
	} else {
		mysqli_rollback($conn);
		return false;
	}
}

function verify_parent($ssn_p, $conn)
{
	$check = "SELECT * FROM parents WHERE ssn =  '".$ssn_p."' ";
    	
  if(!$result =mysqli_query($conn,$check)) {
    return false;
	}
	if (mysqli_num_rows($result) == 1){
		return true;	// parent exist
	}
	return false;	// parent not exist
}

function verify_student($ssn_s, $conn)
{
	$check = "SELECT * FROM students WHERE ssn =  '".$ssn_s."' ";
    	
  if(!$result =mysqli_query($conn,$check)) {
		return false;
	}
	if (mysqli_num_rows($result) == 1){
		return false;	// student already exist
	}
	return true;	// parent not exist
}




?>