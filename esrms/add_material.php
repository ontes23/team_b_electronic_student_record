<?php

if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
if (mysqli_connect_errno()) {
    //echo "Connessione fallita: ".
    mysqli_connect_error();
    exit();
}

if (isset($_POST['addMaterial'])){

    $name = $_FILES['file2']['name'];
    $type = $_FILES['file2']['type'];

    $ssn_t = $_SESSION['user_ssn'];
    $m_class = $_POST['hw_class'];
    $m_cid = $_POST['cid'];
    $m_subject = $_POST['subject'];


    /* Storing the attachment in local folder */

    // Create the folder materials/ if it does not exist
    if(!is_dir('materials')){
        mkdir(getcwd()."/materials", 0777, false);
    }
    // Create the folder homeworks/$hw_cid/ if it does not exist
    if(!is_dir('materials/'.$m_cid)){
        mkdir(getcwd()."/materials/".$m_cid, 0777, false);
    }
    // Create the folder homeworks/$hw_cid/$subject/ if it does not exist
    if(!is_dir('materials/'.$m_cid.'/'.$m_subject)){
        mkdir(getcwd()."/materials/".$m_cid.'/'.$m_subject, 0777, false);
    }
    $dstFile = getcwd()."/materials/".$m_cid.'/'.$m_subject."/".$name;
    move_uploaded_file($_FILES['file2']['tmp_name'], $dstFile);


    $emptyErrorClass = false;

    $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
    if (mysqli_connect_errno()) {
        echo "Connessione fallita: ". mysqli_connect_error();
        exit();
    }



    if(empty($m_class)){
        header("location:teacher_page_material.php?error=Error: no class selected!");
        $emptyErrorClass = true;
        die();
    }else if(empty($name)){
        header("location:teacher_page_material.php?error=Error: Please add a material!");
        die();
    }else{
        $m_subject =$_POST['subject'];
        $data = file_get_contents($_FILES['file2']['name']);
        $sql = "SELECT ssn FROM teachers WHERE name=? and surname=?";
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "<span>Error of ssn</span>";
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt, "ss",$_SESSION["name_u"],$_SESSION["surname_u"]); //we are passing string
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if($row = mysqli_fetch_assoc($result)) { //check if we get real thing or empty thing,get
                //get ssn_t
               
                $ssn_t = $row['ssn'];
            }
        }



        $sql = "INSERT INTO materials(aname, atype,atachment,cid,subject,ssn_t) VALUES(?,?,?,?,?,?)"; //for safety
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "<span>Error</span>";
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "ssssss",$name,$type,$data, $m_cid,$m_subject,$ssn_t); //we are passing string
            mysqli_stmt_execute($stmt);
			
            header("location:teacher_page_material.php?successful_op=The material have been successfully inserted");
			
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);
    }
}else{
    header("location:teacher_page_material.php?error=Error: the size of the attachment is greater than 8 MB!");
    die();

}
?>
