﻿<?php
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	if($_SESSION["test_in_action"]!='1'){
	include("sidebars.php");
 }
 
 
?>

<!doctype html>
<html lang="en">
  <head>
  
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard Template · Bootstrap</title>

      <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

  </head>
  <body>

	
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Parent Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
      <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>


<div class="container-fluid">
  <div class="row">
	
    <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen">
        <ul class="nav flex-column">
		 <li class="nav-item">
			<div class="user-info">
					<div class="image"><img src="photos/user.png" alt="User"></div>
					<div class="detail">
							<h4><?php echo $_SESSION["name_s"]." ".$_SESSION["surname_s"]; ?></h4>
							<small><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></small>
					</div>
			</div>
     </li>
            <?php
					if($_SESSION["test_in_action"]!='1'){
					parent_print_sidebar("true");
				 }
 
            ?>
          </ul>
      </div>
    </nav>

    <main role="main" class="col">
		
      <div class="pt-3 pb-2 mb-3">
      <h2>Attendance</h2>
      <div>
		<?php include ('calendar_handle/table_attendance.php'); ?>
         
      </div>
	   </div>
<?php
include('parent_check_attendance_summary.php');
?>
<h2> Summary absences </h2><br>
<table class="table">
<caption></caption>
<thead><th scope="col">Summary info</th><th scope="col">Number of absences</th>
</thead>
  <tbody>
    <tr>
      <td >Total number of <strong>absences</strong></td>
      <td><strong><?php echo $number_absences; ?></strong></td>
    </tr>
    <tr>
      <td >Total number of <strong>Late entries</strong></td>
      <td><strong><?php echo $number_enters_late; ?></strong></td>
 
    </tr>
    <tr>
      <td >Total number of <strong>Early exits</strong></td>
      <td><strong><?php echo $number_exits_early; ?></strong></td>

    </tr>
  </tbody>
</table>
	   
	   
    </main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
