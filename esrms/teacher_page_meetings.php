<?php
include("sidebars.php");
define("TEST","during_test");
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
global $class_id;
if(isset($_GET['class'])) {
    $class_id = $_GET['class'];
}
if(isset($_GET['subject'])) {
    $subject = $_GET['subject'];
}

?>
<!Doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Teacher Record Marks</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    <link href="css/timetable_week.css" rel="stylesheet" >
    <script type="text/javascript" src="js/timetable_functions.js"></script>


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }


    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>


<script>

    var slots = new Array();


    function reloadPage_classOrSubjectSelected(){
        var classElement = document.getElementById("class");
        var className = classElement.options[classElement.selectedIndex].id;
        var currentClassName = getUrlVars()['class'];

        if(className != currentClassName)
            window.location.replace("teacher_page_timetable.php?class=" + className +"&subject=");
        else {
            //var subjectElement = document.getElementById("subject2");
            //var subjectName = subjectElement.options[subjectElement.selectedIndex].id;
            //window.alert(document.getElementById("subject2").options[document.getElementById("subject2").selectedIndex].id);
            window.location.replace("teacher_page_timetable.php?class=" + className + "&subject=" + document.getElementById("subject2").options[document.getElementById("subject2").selectedIndex].id);
        }
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
	
function fullTimeSlot(el){
   alert("You cannot choose this slot!");
}
function reserveTimeSlot(el) {

    // Get hour and day from ID
    var id = el.getAttribute("id");
    var data = id.split("_");
    var hour = data[0];
    var day = data[1];


    if (el.getAttribute("class") == 'orange') {
        var response_delete = confirm("Are you sure to delete this slot?");
        if (response_delete == true) {
            $(".meeting_output").load("delete_meeting.php", { hour :hour , day : day },function(response,status,xhr){
                el.setAttribute("class", "green");
                el.innerHTML = "Free".bold();
                deleteSlot(hour, day);
            });
        } else {

        }
    }
    else if (el.getAttribute("class") == 'green') {
        $(".meeting_output").load("add_meeting.php", { hour :hour , day : day },function(response,status,xhr){

            if(response == "This time slot is not free!"){
                el.setAttribute("class", "green");
                el.innerHTML = "Available".bold();
                deleteSlot(hour, day);
            }
            else{
                el.setAttribute("class", "orange");
                el.innerHTML = "Available for meetings".bold();
                slots.push([hour, day]);
            }
        });

        // go to post -ajax

    }
}
function getCoordinate(e1) {
        if (e1 != undefined)
            return e1.getAttribute("id").split("-").slice(1,3);
        else
            return undefined;
    }

function deleteSlot(hour, day){
        for(i=0; i<slots.length; i++){
            if(slots[i][0]==hour && slots[i][1]==day) {
                slots.splice(i, 1);
                break;
            }
        }
}



</script>


<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Teacher Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-md-block bg-light sidebar">
            <div class="small_screen" >
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"]." ".$_SESSION["surname_u"]; ?></h4>
                            </div>
                        </div>
                    </li>
                    <?php
                        if(!isset($_SESSION[TEST])){
                            teacher_print_sidebar("true");
                        }
                    ?>
                </ul>

            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h2>Manage Meetings</h2>
                <form action = "" method = "post">

            </div>
            <div class ="assign_meeting">
                Please click the time slots to change their state.
            </div>
            <table class="timetable_week">
			<caption></caption>
                <thead>
                <?php
                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
                $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                $weekdays_shorthand = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                $hours = ["08:00","09:00","10:00","11:00","12:00","13:00"];
                echo "<th>";
                for($i=0; $i<count($weekdays); $i++){
                    ?><th id="col"><?php echo $weekdays[$i]; ?></th><?php
                }
                ?>
                </thead>
                <tbody>
                <?php

                // Retrieve all the slots reserved for lessons
                $sql = "SELECT *
                        FROM timetables t, class c
                        WHERE t.cid = c.cid
                        AND t.ssn_t = '". $_SESSION["user_ssn"] ."'";
                if($class_id!=''){
                    $sql .= " AND t.cid='".$class_id."'";
                }
                if(isset($subject) && $subject!=''){
                    $sql .= " AND t.subject='".$subject."'";
                }
                $sql .= " ORDER BY t.hour, t.day";

                if (!$result = mysqli_query($conn, $sql)) {
                    $msg = "Errore nell’inserimento del post, riprovare";
                    die($msg);
                }
                $rows_classes = array();
                while ($row = $result->fetch_assoc()) {
                    array_push($rows_classes, $row);
                }

                // Retrieve all the slots reserved for meetings
                $sql = "SELECT *
                        FROM meetings
                        WHERE ssn_t = '". $_SESSION["user_ssn"] ."' ORDER BY hour,day;";
                if (!$result = mysqli_query($conn, $sql)) {
                    $msg = "Errore nell’inserimento del post, riprovare";
                    die($msg);
                }
                $rows_meetings = array();
                $rows_ssn = array();
                while ($row = $result->fetch_assoc()) {
     
                    array_push($rows_meetings, $row);
                }
                $k = 0;
                $l = 0;

                for($i=0; $i<6; $i++){
                    if(isset($_SESSION[TEST]) && $_SESSION[TEST]=='1' ){
                       
                            $temp = mysqli_num_rows($result);
                       
                    }
                    echo "<tr><td class='hours'>".$hours[$i]."</td>";

                    for($j=0; $j<6; $j++){
                        echo "<td";
                        if($k<count($rows_classes) && $rows_classes[$k]['day']==$weekdays_shorthand[$j] && $rows_classes[$k]['hour']==$hours[$i]){
                            echo " id='". $hours[$i] . "_" . $weekdays[$j] . "' class='red' onclick='fullTimeSlot(this)'><strong>".$rows_classes[$k]['subject']."</strong>";
                            echo "<br>" . $rows_classes[$k]['name']."</td>";
                            $k++;
                        }
                        else if($l<count($rows_meetings) && $rows_meetings[$l]['day']==$weekdays[$j] && $rows_meetings[$l]['hour']==$hours[$i] ){
                            if(!empty($rows_meetings[$l]['p_name']) && !empty($rows_meetings[$l]['p_surname'])){


                                echo " id='". $hours[$i] . "_" . $weekdays[$j] . "' class='red' onclick='fullTimeSlot(this)'><strong>Meeting with ".$rows_meetings[$l]['p_name']." ".$rows_meetings[$l]['p_surname']."</strong>";
                                echo "</td>";

                            }
                            else{  //If there is no reservation for parent
                                echo " id='". $hours[$i] . "_" . $weekdays[$j] . "' class='orange' onclick='reserveTimeSlot(this)'><strong>Available for meetings</strong>";
                                echo "</td>";
                            }
                            $l++;
                        }

                        else{
                            echo " id='". $hours[$i] . "_" . $weekdays[$j] . "' class='green' onclick='reserveTimeSlot(this)'><strong>Free</strong>";
							echo "</td>";
                        }

                    }
                    echo "</tr>";

                }

                ?>
                </tbody>
            </table>

            <div>
                    <p class= "meeting_output"></p>

            </div>

            </div>
        </main>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="js/dashboard.js"></script></body>
</html>
