<?php
# read received file and for each line call add_student.php
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
$myFile="file";
$target_dir = "uploads/";
if(!isset($_SESSION['file'])){
    $target_file = $target_dir . basename($_FILES[$myFile]["name"]);
}else{
	$target_file = $_SESSION['file'];
}
$uploadOk = 1;
$msg = "The timetables have been successfully uploaded!";


$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
		if (mysqli_connect_errno()) {
			// Connection failed
			mysqli_connect_error();
			$msg = 'Error connect';
			myEnd($msg);
		}

if (verify_post())
{
	// read file

    if(!isset($_SESSION['test'])) {
        if (!is_uploaded_file($_FILES[$myFile]["tmp_name"]) || $_FILES[$myFile]["error"] > 0) {
            myEnd("Error: no file selected!");
        }

        if (!move_uploaded_file($_FILES[$myFile]["tmp_name"], $target_file)) {
            myEnd("Error during upload!");
        }
    }

	if  ($line = load_timetable($target_file, $conn) ) {
		//empty
	} else {
        myEnd("Error in line " . $line);
	}
	
} else {
	$msg = 'empty post';
	
}
myEnd($msg);
##### functions #####################

function myEnd($msg)
{
	if(!isset($_SESSION['test'])){
        header('Location: administrator_add_timetable.php?result='.$msg);
        die();
    }

}

function verify_post()
{
	if (isset($_POST["submit"])){
		return true;
	}
		return false;
	
}


/*
	load_timetable verify the presence of already existing timetable for class and delete it
*/
function load_timetable($target_file, $conn)
{
	$error = '0';
	$line = '0';
	$myfile = fopen($target_file, "r") or die("Unable to open file!");
		// Output one line until end-of-file
	mysqli_begin_Transaction($conn);
	while (($data = fgetcsv($myfile, 1000, ";")) !== FALSE  && $error == 0) 
  {
		$line++;

		$num = count($data);			

		
		if ($num == 5){
			$classId = $data[0];
			$ssn_t = $data[1];
			$subject = $data[2];
			$hour = $data[3];
			$day = $data[4];
		
			
			if (verify_presence($classId, $day, $hour, $conn) == 1){
				// time already present -> update
			
				if (update($classId, $ssn_t, $subject, $hour, $day, $conn) == 0){
					// errore in update
					$msg = "Error during update in line: ".$line;
					echo $msg;
				}
			} else {
				// new time -> insert
				if (insert($classId, $ssn_t, $subject, $hour, $day, $conn) == 0){
					// errore in update
					$msg = "Error during insert in line: ".$line;
					echo $msg;
				}
				
			}
		}
	}
	fclose($myfile);

	if($error == '0'){
	
		mysqli_commit($conn);
		return true;
	} else {

		mysqli_rollback($conn);
		return false;
	}
}

function verify_presence($classId, $day, $hour, $conn)
{
	$check = "SELECT * FROM timetables WHERE cid =  '".$classId."' AND day = '".$day."' AND hour = '".$hour."' ;";

  if(!$result=mysqli_query($conn,$check)) {
    return false;
	}
	if (mysqli_num_rows($result) == 1){
		return true;	// parent exist
	}
	return false;	// parent not exist
}


function update($classId, $ssn_t, $subject, $hour, $day, $conn ){
	$sql = "UPDATE timetables SET cid = '".$classId."', ssn_t = '".$ssn_t."', subject = '".$subject."', hour = '".$hour."', day = '".$day."'
					WHERE cid =  '".$classId."' AND day = '".$day."' AND hour = '".$hour."';";
	
	if(!mysqli_query($conn,$sql)) {
    return false;
	}
	return true;	// update succes
}

function insert($classId, $ssn_t, $subject, $hour, $day, $conn ){
	$sql = "INSERT INTO timetables (ttid, cid, ssn_t, subject, hour, day) 
					VALUES ('', '".$classId."','".$ssn_t."','".$subject."','".$hour."','".$day."');";
	
	if(!mysqli_query($conn,$sql)) {
		
    return false;
	}
	
	return true;	// update succes
}

?>