﻿<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Teacher Page</title>
    

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    
    <!-- Main CSS-->
    <link href="css/add_homework_form.css" rel="stylesheet" media="all">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<?php
 include("sidebars.php");
 session_start();
 
global $class_id;
if(isset($_GET['class'])) {
    $class_id = $_GET['class'];
}

?>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" >


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

 </head>
 <body>



 <script>

     function reloadPage_classSelected(){
         var classElement = document.getElementById("hw_class");
         var className = classElement.options[classElement.selectedIndex].id;
         window.location.replace("teacher_page_material.php?class=" + className);
     }

     /*  $(document).ready(function() {
            $("#add_hw").submit(function(event) {
                alert($("#hw_deadline").val());
                var hw_fileLen  = $("#hw_file").val().length;
                var hw_descriptionLen = $("#hw_description").val().length;
                var hw_classLen  = $("#hw_class").val().length;
                var hw_subjectLen = $("#hw_subject").val().length;
                var hw_deadlineLen  = $("#hw_deadline").val().length;


                var hw_file = $("#hw_file").val();
                var hw_description = $("#hw_description").val();
                var hw_class  = $("#hw_class").val();
                var hw_subject = $("#hw_subject").val();
                var hw_deadline= $("#hw_deadline").val();
                var addHomework= $("#addHomework").val();

                event.preventDefault();

                $(".message").load("add_homework.php", {
                    hw_file : hw_file,
                    hw_description: hw_description,
                    hw_class: hw_class,
                    hw_subject : hw_subject,
                    hw_deadline: hw_deadline,
                    addHomework : addHomework
                });

                /*if(hw_descriptionLen == 0 ){
                    $(".message").text("Please fill the form!");
                    $(".message").addClass("error-message");

                }

                else{
                    $(".message").load("add_homework.php", {
                        hw_file : hw_file,
                        hw_description: hw_description,
                        hw_class: hw_class,
                        hw_subject : hw_subject,
                        hw_deadline: hw_deadline,
                        parentPhone: parentPhone,
                        addHomework : addHomework
                    });

                }

            });




        });*/

  /*
   $(document).ready(function() {
       $("#add_hw").submit(function(event) {


           var hw_descriptionLen = $("#hw_description").val().length;



           var hw_file = $("#hw_file").val();
           var hw_description = $("#hw_description").val();
           var hw_class  = $("#hw_class").val();
           var hw_subject = $("#hw_subject").val();
           var hw_deadline= $("#hw_deadline").val();
           var addHomework = $("#addHomework").val();



           event.preventDefault(); //break the signuppost,move on there

           if(hw_descriptionLen == 0 ){
               $(".message").text("Please fill the form!");
               $(".message").addClass("error-message");

           }

           else{
               $(".message").load("add_homework.php", {
                   hw_file : hw_file,
                   hw_description: hw_description,
                   hw_class: hw_class,
                   hw_subject : hw_subject,
                   hw_deadline: hw_deadline,
                   addHomework : addHomework
               });

           }
       });

   });



*/



 </script>
 <noscript> Sorry: Your browser does not support or has disabled javascript </noscript>
 
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Teacher Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen" >
        <ul class="nav flex-column">
		 <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"]." ".$_SESSION["surname_u"]; ?></h4>
                            </div>
                        </div>
           </li>
            <?php
                teacher_print_sidebar("true");
            ?>
         
        </ul>

      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <?php 
	 if(isset($_GET["successful_op"])){
		 echo "<h1 style='color:green'>".$_GET["successful_op"]."</h1>";
	 }
	 ?>  
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		 <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Add new material</h2>
                </div>
                <div class="card-body">

                        <form action="add_material.php" method="POST" id='add_hw' enctype="multipart/form-data">
                            <div class="form-row m-b-55">
                                <div class="name">Class:*</div>
                                <div class="value">
                                    <div class="row row-refine">
                                        <div class="col-9">
                                            <div class="input-group-desc">
                                                <select class="custom-select mr-sm-2" name="hw_class" id="hw_class" onchange="reloadPage_classSelected()">
                                                    <?php
                                                    if($class_id == "")
                                                        echo "<option selected hidden></option>";
                                                    $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                                                    $sql = "SELECT DISTINCT c.name, c.cid
																		FROM class c, bridge_class_teachers bct
																		WHERE c.cid = bct.cid
																		AND bct.ssn_t = '" . $_SESSION["user_ssn"] . "'";
                                                

                                                    if (!$result = mysqli_query($conn, $sql)) {
                                                        $msg = "Errore nell’inserimento del post, riprovare";
                                                    }
                                                    $temp = mysqli_num_rows($result);

                                                    while ($row = $result->fetch_assoc()) {
                                                       
                                                        echo "<option id=" . $row['cid'];
                                                        if ($row['cid'] == $class_id){
                                                            echo " selected";
                                                        }
                                                        echo ">" . $row['name'] . "</option>";
                                                    }
                                                    echo "<input type='hidden' name='cid' value='".$class_id."'>";
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row m-b-55">
                                <div class="name">Subject:*</div>
                                <div class="value">
                                    <div class="row row-refine">
                                        <div class="col-9">
                                            <div class="input-group-desc">
                                                <select class="custom-select mr-sm-2" name = "subject" id="hw_class">
                                                    <?php
                                                    if($class_id!="") {
                                                        $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                                                        $sql = "SELECT subject
                                        FROM bridge_class_teachers
                                        WHERE ssn_t = '" . $_SESSION['user_ssn'] . "'
                                        AND cid = '" . $class_id . "'";
                                               

                                                        if (!$result = mysqli_query($conn, $sql)) {
                                                            $msg = "Errore nell’inserimento del post, riprovare";
                                                        }
                                                        $temp = mysqli_num_rows($result);

                                                        while ($row = $result->fetch_assoc()) {
                                                            echo "<option>" . $row['subject'] . "</option>";
                                                        }
                                                  
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="form-row">
                            <div class="name" >Attachment:*</div>
                            <div class="value">
                                <div class="input-group">
                                    <input type="file" name="file2" id='hw_file' class='btn--red'>
                                </div>
                            </div>
                            <div>[Max. size: 8 MB]</div>
                        </div>


                        <div>
                            <p id ="messagee" class= "message"></p>
                            <button id="addMaterial"  name="addMaterial" class="btn btn-dark" type="submit">Submit</button>
                        </div>

                    </form>
                    <p id="message" class="error-message">
                    <?php
                    if(isset($_GET['error'])){
                        echo $_GET['error'];
                    }
                    ?>
                    </p>
                </div>
            </div>
        </div>
    </main>



	 
    </main>
  </div>
</div>
    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
