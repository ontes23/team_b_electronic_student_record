-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 27, 2019 alle 14:07
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esrms`
--
CREATE DATABASE IF NOT EXISTS `esrms` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `esrms`;

-- --------------------------------------------------------

--
-- Struttura della tabella `absences_presences`
--

DROP TABLE IF EXISTS `absences_presences`;
CREATE TABLE `absences_presences` (
  `abs_id` varchar(255) NOT NULL,
  `ssn_s` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(64) NOT NULL,
  `state` varchar(16) NOT NULL,
  `description` varchar(64) NOT NULL,
  `ssn_t` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `cid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `administrators`
--

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `ssn` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(16) DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `administrators`
--

INSERT INTO `administrators` (`ssn`, `name`, `surname`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('1234', 'lala', 'lala', 'lala', '1', NULL, 'superpiero23@hotmail.it', 'fb07963dbe8cdf2293247afc4fc2a05bfef915555a7ce4af438da084f2e066f10d7847e4452120e8754749effab5d3348bde8fd841d8bfd056015f7e15b32058', '194595'),
('12345', 'cac', 'cac', 'cac', '1', NULL, 'superpiero23@hotmail.it', 'd7896c7a83058f998b9053b60ca45c6ef9c00538cf3a93ae9eacacfc3f95230ecbceced4b76b42d6c4fd8d7d003428fec358286878e8662c44d8e20c0f478143', '601263'),
('4AAAAAAAAAAAAAAA', 'Lucia', 'Guiso', 'Corso Cristoforo Colombo 12', NULL, NULL, 'adminA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('fefe', 'efe', 'fefe', 'fefe', 'ee', NULL, 'fefe@dwd.it', 'c152aafc5c1f36770da4b1c749887328a0a8b3bc174e73b978c99ce02e3a69cdcea0725cfab1580c742538e11ee2a80138c45d9a82f763420a9e54ad8bcb09e4', '509145');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_class_students`
--

DROP TABLE IF EXISTS `bridge_class_students`;
CREATE TABLE `bridge_class_students` (
  `cid` varchar(64) NOT NULL,
  `ssn_s` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_class_students`
--

INSERT INTO `bridge_class_students` (`cid`, `ssn_s`) VALUES
('C1', '1AAAAAAAAAAAAAAA'),
('C1', '1CCCCCCCCCCCCCCC'),
('C1', '1DDDDDDDDDDDDDDD'),
('C1', '1EEEEEEEEEEEEEEE'),
('C2', '1BBBBBBBBBBBBBBB'),
('C3', '1FFFFFFFFFFFFFFF');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_class_teachers`
--

DROP TABLE IF EXISTS `bridge_class_teachers`;
CREATE TABLE `bridge_class_teachers` (
  `ssn_t` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_class_teachers`
--

INSERT INTO `bridge_class_teachers` (`ssn_t`, `cid`, `subject`) VALUES
('3AAAAAAAAAAAAAAA', 'C1', 'matematica'),
('3AAAAAAAAAAAAAAA', 'C2', 'fisica'),
('3AAAAAAAAAAAAAAA', 'C3', 'apa'),
('3BBBBBBBBBBBBBBB', 'C2', 'filosofia');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_parents_students`
--

DROP TABLE IF EXISTS `bridge_parents_students`;
CREATE TABLE `bridge_parents_students` (
  `ssn_p` varchar(16) NOT NULL,
  `ssn_s` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_parents_students`
--

INSERT INTO `bridge_parents_students` (`ssn_p`, `ssn_s`) VALUES
('2AAAAAAAAAAAAAAA', '1AAAAAAAAAAAAAAA'),
('2AAAAAAAAAAAAAAA', '1BBBBBBBBBBBBBBB');

-- --------------------------------------------------------

--
-- Struttura della tabella `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `cid` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `class`
--

INSERT INTO `class` (`cid`, `name`) VALUES
('C1', '1A'),
('C2', '3B'),
('C3', '5C');

-- --------------------------------------------------------

--
-- Struttura della tabella `lessons`
--

DROP TABLE IF EXISTS `lessons`;
CREATE TABLE `lessons` (
  `lid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(16) NOT NULL,
  `description` varchar(512) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `timestamp` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `lessons`
--

INSERT INTO `lessons` (`lid`, `cid`, `ssn_t`, `date`, `time`, `description`, `subject`, `timestamp`) VALUES
('0', 'C2', '3AAAAAAAAAAAAAAA', '2019-11-16', '08:00', 'Algoritmo di Dijkstra', 'matematica', 'Nov,16,2019 05:06:02 PM'),
('2', 'C1', '3AAAAAAAAAAAAAAA', '2019-11-16', '09:00', 'bella', 'matematica', 'Nov,21,2019 05:06:18 PM'),
('3', 'C1', '3AAAAAAAAAAAAAAA', '2019-11-16', '10:00', 'equazione', 'matematica', 'Nov,21,2019 05:09:48 PM');

-- --------------------------------------------------------

--
-- Struttura della tabella `marks`
--

DROP TABLE IF EXISTS `marks`;
CREATE TABLE `marks` (
  `m_id` int(11) NOT NULL,
  `ssn_s` varchar(16) NOT NULL,
  `subject` varchar(32) NOT NULL,
  `mark` decimal(4,2) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `marks`
--

INSERT INTO `marks` (`m_id`, `ssn_s`, `subject`, `mark`, `ssn_t`, `date`, `time`) VALUES
(0, '1AAAAAAAAAAAAAAA', 'Natural science', '7.50', '3AAAAAAAAAAAAAAA', '2019-11-03', '14:22:34');

-- --------------------------------------------------------

--
-- Struttura della tabella `parents`
--

DROP TABLE IF EXISTS `parents`;
CREATE TABLE `parents` (
  `ssn` varchar(16) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(16) DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `parents`
--

INSERT INTO `parents` (`ssn`, `surname`, `name`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('1234', 'luca', 'luca', 'luca', '1', NULL, 'superpiero23@hotmail.it', 'c506afb8bd268a9c8924f14a92e6d2ad45567f882a7572bc42e0a1b53dcae8bc09bd6a8abfa864277de5ef41aaba54add1806973d84295ae062fae802d9b0f17', '356047'),
('2AAAAAAAAAAAAAAA', 'Rossi', 'Mario', 'Via Giulio Cesare 21', '0123456789', '0123456789', 'parA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('2BBBBBBBBBBBBBBB', 'Verdi', 'Luigi', 'Corso Garibaldi 34', '9876543210', '9876543210', 'parB@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('2ZZZZZZZZZZZZZZZ', 'Buffo', 'Matteo', '', '3317668592', NULL, 'matteobuf@gmail.com', 'f9c19cd7aedc76600f781d785fb25be893dd3f446dd1be1237f2196fbe26dee2614a41a683e14bf1964d1d263d26eda8b5581990604a12564ca64aa6abadf3aa', '313128');

-- --------------------------------------------------------

--
-- Struttura della tabella `principals`
--

DROP TABLE IF EXISTS `principals`;
CREATE TABLE `principals` (
  `ssn` varchar(64) NOT NULL,
  `surname` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `homephone` varchar(64) NOT NULL,
  `cellphone` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `principals`
--

INSERT INTO `principals` (`ssn`, `surname`, `name`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('fefe', 'fefe', 'efe', 'fefe', 'ee', '', 'fefe@dwd.it', 'b2b66f5063abf2ce4e68d057d4c348a1cf29323c6a69672a91aea9c109a25e9ad04a918fa272afcb6bbfef3b2346b245d1729e76c98535fceb3e80c04743edc4', '697650'),
('22', 'fefe', 'efe', 'fefe', 'ee', '', 'superpiero23@hotmail.it', '04227d05849b854e194eb4678e00536e061e5bfb4a3cef62b6fec632efc60b762c809d004ca3e4d16aa2992e6b3f9d22647de3e5093279ba76d9d1fb1ba6227b', '159411'),
('123456', 'gangemi', 'piero', 'via 2', '1', '', 'superpiero23@hotmail.it', 'fa127092450d644dc5f29e2d37a7b4ea24dba101d3f87a4fd4e26dd72b0ec84eb40e2f21ee53dd900670550305e22bcd480948d668cebdd874a66d05b31fcd91', '178064');

-- --------------------------------------------------------

--
-- Struttura della tabella `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `ssn` varchar(16) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` varchar(128) NOT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `gender` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `students`
--

INSERT INTO `students` (`ssn`, `surname`, `name`, `address`, `cellphone`, `gender`) VALUES
('1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'Via Giulio Cesare 21', NULL, 'm'),
('1BBBBBBBBBBBBBBB', 'Rossi', 'Angela', 'Via Giulio Cesare 21', NULL, 'f'),
('1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'Corso Garibaldi 34', NULL, 'm'),
('1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'Via Leonardo 102', NULL, 'm'),
('1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', '', NULL, 'm'),
('1FFFFFFFFFFFFFFF', 'Neri', 'Marina', '', NULL, 'f');

-- --------------------------------------------------------

--
-- Struttura della tabella `sysadmins`
--

DROP TABLE IF EXISTS `sysadmins`;
CREATE TABLE `sysadmins` (
  `ssn` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `sysadmins`
--

INSERT INTO `sysadmins` (`ssn`, `name`, `surname`, `email`, `password`, `salt`) VALUES
('1A', 'Amministratore', 'Supremo', 'sysadmin@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456');

-- --------------------------------------------------------

--
-- Struttura della tabella `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `ssn` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(512) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `teachers`
--

INSERT INTO `teachers` (`ssn`, `email`, `password`, `name`, `surname`, `salt`, `address`, `homephone`, `cellphone`) VALUES
('123456', 'superpiero23@hotmail.it', '54dc13a11ebdf8bf379c21b4c702a72208b923da50d7fe013cb0c32568e42a66c6201a408540641222f18a9ae81fbee3f3825078fa18ef2eeeecfe65d35e56de', 'luigi', 'luca', '460836', 'via 2', '1', ''),
('3AAAAAAAAAAAAAAA', 'teachA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Gianpiero', 'Cabodi', '123456', 'Via Dijkstra 10', '', ''),
('3BBBBBBBBBBBBBBB', 'teachB@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Paolo Enrico', 'Camurati', '123456', 'Via Kernighan 70', '', '');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `absences_presences`
--
ALTER TABLE `absences_presences`
  ADD PRIMARY KEY (`abs_id`);

--
-- Indici per le tabelle `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `bridge_class_students`
--
ALTER TABLE `bridge_class_students`
  ADD PRIMARY KEY (`cid`,`ssn_s`),
  ADD KEY `ssn_s` (`ssn_s`);

--
-- Indici per le tabelle `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `cid` (`cid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
