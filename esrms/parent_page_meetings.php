<?php
define("TEST","during_test");
define("NAMEU","name_u");
define("SURNAMEU","surname_u");
define("MSG","Errore nell’inserimento del post, riprovare");
include("sidebars.php");
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
global $teacher_id;
if(isset($_GET['class'])) {
    $teacher_id = $_GET['class'];
}
if(isset($_GET['subject'])) {
    $subject = $_GET['subject'];
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Parent Timetable</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/timetable_week.css" rel="stylesheet">



    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>
<script>

    function reloadPage_classOrSubjectSelected(){
        var classElement = document.getElementById("teacher");
        var className = classElement.options[classElement.selectedIndex].id;
        var currentClassName = getUrlVars()['teacher'];

        if(className != currentClassName)
            window.location.replace("parent_page_meetings.php?class=" + className);
        else {
    
            window.location.replace("parent_page_meetings.php?class2=" + className);
        }
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    function fullTimeSlot(el){
        alert("You can not choose this slot!");
    }
    async function reserveTimeSlot(el) {

        // Get hour and day from ID
        var id = el.getAttribute("id");
        var data = id.split("_");
        var hour = data[0];
        var day = data[1];
        var ssn_t = data[2];


        if (el.getAttribute("class") == 'green') {
            var response_delete = confirm("Do you want to reserve this time slot?");
            if (response_delete == true) {
                $(".reservation_output").load("add_reservations.php", { hour :hour , day : day,ssn_t:ssn_t }, async function(response,status,xhr){
                    if(response == "Meeting successfully booked."){
                        el.setAttribute("class", "orange");
                        el.innerHTML = "Reserved ".bold();
                        deleteSlot(hour, day);
                    }
                    await new Promise(r => setTimeout(r, 1000));
                    location.reload();

                });
            } else {

            }
        }
        else if (el.getAttribute("class") == 'orange') {
            var response_delete = confirm("Do you want to delete this time slot?");
            if (response_delete == true) {
                $(".reservation_output").load("delete_reservation.php", { hour :hour , day : day,ssn_t:ssn_t },async function(response,status,xhr){
                    if(response == "Meeting undone successfully."){
                        el.setAttribute("class", "green");
                        el.innerHTML = "Available".bold();
                        slots.push([hour, day]);

                    }
                    await new Promise(r => setTimeout(r, 1000));
                    location.reload();

                });
            } else {

            }

        }
    }

</script>

<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Parent Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
                <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-md-block bg-light sidebar">
            <div class="small_screen" >
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_s"]." ".$_SESSION["surname_s"]; ?></h4>
                                <small><?php echo $_SESSION[NAMEU] ." ". $_SESSION[SURNAMEU]; ?></small>
                            </div>
                        </div>
                    </li>
                    <?php
                    if(!isset($_SESSION[TEST])){
                        parent_print_sidebar("true");
                    }
                    ?>

                </ul>

            </div>
        </nav>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h2>Meetings</h2>
                <div class="form-row align-items-center">
                    <!-- SELECT TEACHER -->
                    <div class="col-auto my-1">
                        <label class="mr-sm-2" for="inlineFormCustomSelect">Select Teacher</label>
                        <select class="custom-select mr-sm-2" name = "teacher" id="teacher" onchange="reloadPage_classOrSubjectSelected()" onload="createTimetable()">
                            <?php
                            if(!isset($teacher_id))
                                echo "<option selected></option>";
                            $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
                            global $teacher_id;
                            $sql = "SELECT cid
                                    FROM bridge_class_students
                                    WHERE ssn_s = '". $_SESSION['ssn_s'] . "'";
                            if (!$result = mysqli_query($conn, $sql)) {
                                $msg = MSG;
                                die($msg);
                            }
                            while ($row = $result->fetch_assoc()){
                                $cid = $row['cid'];
                            }
                            $sql2 = "SELECT t.surname, t.name ,t.ssn, bct.subject
                                        FROM  teachers t,bridge_class_teachers bct 
                                        WHERE t.ssn = bct.ssn_t
                                         AND bct.cid = '$cid' ";

                            if (!$result = mysqli_query($conn, $sql2)) {
                                $msg = MSG;
                            }
                            $temp = mysqli_num_rows($result);

                            while($row = $result->fetch_assoc()) {
                                echo "<option id=" . $row['ssn'];
                                if ($row['ssn'] == $teacher_id){
                                    echo " selected";
                                }
                                echo ">" . substr($row['name'], 0, 1). ". "
                                    . $row['surname']
                                    . " - " . $row['subject']
                                    . " </option>";
                            }
                        
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class ="assign_meeting">
                Please click the time slots to reserve/undo meetings with teachers.
            </div>
            <table class="timetable_week">
			<caption></caption>
                <thead>
                <?php
                $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                $weekdays_shorthand = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                $hours = ["08:00","09:00","10:00","11:00","12:00","13:00"];
                echo "<th>";
                for($i=0; $i<count($weekdays); $i++){
                    ?><th id='col'><?php echo $weekdays[$i];?></th><?php
                }
                ?>
                </thead>
                <tbody>
                <?php

                // Retrieve all the slots reserved for meetings
                $sql = "SELECT *
                        FROM meetings
                        WHERE ssn_t = '". $teacher_id ."' ORDER BY hour,day;";
                if (!$result = mysqli_query($conn, $sql)) {
                    $msg = MSG;
                    die($msg);
                }
                $rows_meetings = array();
                $rows_ssn = array();
                while ($row = $result->fetch_assoc()) {
                    
                    array_push($rows_meetings, $row);
                }
                $k = 0;
                $l = 0;



                for($i=0; $i<6; $i++){
                    if(isset($_SESSION[TEST])){
                        if($_SESSION[TEST]=='1'){
                            $temp = mysqli_num_rows($result);
                        }
                    }
                    echo "<tr><td class='hours'>".$hours[$i]."</td>";

                    for($j=0; $j<6; $j++){
                        echo "<td";
                        if($l<count($rows_meetings) && $rows_meetings[$l]['day']==$weekdays[$j] && $rows_meetings[$l]['hour']==$hours[$i] ){
                            if(!empty($rows_meetings[$l]['p_name']) && !empty($rows_meetings[$l]['p_surname']) && $rows_meetings[$l]['p_name'] == $_SESSION[NAMEU] && $rows_meetings[$l]['p_surname'] == $_SESSION[SURNAMEU]){
                                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
                                if (mysqli_connect_errno()) {
                                    //echo "Connessione fallita: ".
                                    mysqli_connect_error();
                                    exit();
                                }

                                echo " id='". $hours[$i] . "_" . $weekdays[$j] ."_" . $rows_meetings[$l]['ssn_t'] . "' class='orange' onclick='reserveTimeSlot(this)'><strong>Reserved</strong>";
                                echo "</td>";

                            }
                            else if(!empty($rows_meetings[$l]['p_name']) && !empty($rows_meetings[$l]['p_surname']) && $rows_meetings[$l]['p_name'] != $_SESSION[NAMEU] && $rows_meetings[$l]['p_surname'] != $_SESSION[SURNAMEU]){  //If there is no reservation for parent
                                echo " id='". $hours[$i] . "_" . $weekdays[$j] ."_" . $rows_meetings[$l]['ssn_t'] . "' class='red' onclick='fullTimeSlot(this)'><strong>Meeting with ".$rows_meetings[$l]['p_name']." ".$rows_meetings[$l]['p_surname']."</strong>";
                                echo "</td>";
                            }
                            else if(empty($rows_meetings[$l]['p_name']) && empty($rows_meetings[$l]['p_surname'])){  //If there is no reservation for parent
                                echo " id='". $hours[$i] . "_" . $weekdays[$j] ."_" . $rows_meetings[$l]['ssn_t'] . "' class='green' onclick='reserveTimeSlot(this)'><strong>Available</strong>";
                                echo "</td>";
                            }
                            $l++;
                        }

                        else{
                            echo " id='". $hours[$i] . "_" . $weekdays[$j] . "' class='' onclick='fullTimeSlot(this)'><strong></strong>";
                            echo "</td>";
                        }

                    }
                    echo "</tr>";

                }


                ?>
                </tbody>
            </table>

            <div>
                <p class= "reservation_output"></p>

            </div>

        </main>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="js/dashboard.js"></script></body>
</html>
