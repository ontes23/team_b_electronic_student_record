<?php
class StackTest extends PHPUnit_Framework_TestCase{# la roba commentata gi� � necessaria per far partire i test anche se ho headers
	/**
 * @test
 * @runInSeparateProcess
	**/
   public function test_enter_composition_class(){
	    #I connect into the database
	session_start();
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
	    #Child informations
					$ssn_s = "123456";
					$ssn_s2 = "123";
					$ssn_s3 = "234";
					$ssn_s4 = "567";
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$homephone = '1234';
					$cellphone ='1234';
					$address = 'via 2';
					$gender = 'f';
					$classid = 'Ctest';
					$nameclassid = 'Ctest';
		#Parent informations
					$ssn = 'testtest';
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
		#DELETION PART
					# I delete the bridge table parent child
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the parent of the child
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the classroom
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s3';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s4';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the row from the bridge table between student and class
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s3';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s4';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
		#INSERTION PART
				   #I must insert the classroom in the class table
					$sqltest = "INSERT INTO class(cid,name) values('$classid','$nameclassid');";
						//die($sql);
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					$pswhashed = hash("sha512",$password_base);
					// echo $pswhashed."\n";
						$psw_with_salt = $pswhashed . $salt;
					// echo $psw_with_salt."\n";
						$hashed = hash("sha512",$psw_with_salt);
						//echo $role;
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO parents(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn','$email','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
				
		
		
					#I insert manually the students in the class
					$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s','$surname','$name','$address','$cellphone','$gender')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s2','$surname','$name','$address','$cellphone','$gender')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s3','$surname','$name','$address','$cellphone','$gender')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s4','$surname','$name','$address','$cellphone','$gender')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					#I insert each student inside the same class
					$sqltest = "INSERT INTO bridge_class_students(cid,ssn_s) values('$nameclassid','$ssn_s')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "INSERT INTO bridge_class_students(cid,ssn_s) values('$nameclassid','$ssn_s2')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "INSERT INTO bridge_class_students(cid,ssn_s) values('$nameclassid','$ssn_s3')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "INSERT INTO bridge_class_students(cid,ssn_s) values('$nameclassid','$ssn_s4')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
		#Now I check how many students are selected
		
					$_POST['classe'] = $nameclassid;  
					$_SESSION["name_u"] = "luigi";
					$_SESSION["surname_u"] = "riva";
					$_SESSION["test_in_action"] = '1';
					include('../administrator_view_selected_class.php');
					
					$this->assertTrue($temp == 4);
					

		#DELETION PART
					# I delete the row from the bridge table between student and class
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s3';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_class_students WHERE ssn_s = '$ssn_s4';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}				
					
					# I delete the bridge table parent child
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the parent of the child
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the classroom
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s3';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s4';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					
	}
}
?>		




