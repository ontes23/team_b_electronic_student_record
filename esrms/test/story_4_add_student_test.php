<?php
class StackTest extends PHPUnit_Framework_TestCase{# la roba commentata gi� � necessaria per far partire i test anche se ho headers
	/**
 * @test
 * @runInSeparateProcess
	**/
   public function test_add_student_single_parent(){
	    #I connect into the database
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
	    #Child informations
					$ssn_s = "123456";
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$homephone = '1234';
					$cellphone ='1234';
					$address = 'via 2';
					$gender = 'f';
					$classid = 'Ctest';
					$nameclassid = 'Ctest';
		#Parent informations
					$ssn = 'testtest';
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
		#DELETION PART
					# I delete the bridge table parent child
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the parent of the child
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the classroom
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
		#INSERTION PART
				   #I must insert the classroom in the class table
					$sqltest = "INSERT INTO class(cid,name) values('$classid','$nameclassid');";
						//die($sql);
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					$pswhashed = hash("sha512",$password_base);
					// echo $pswhashed."\n";
						$psw_with_salt = $pswhashed . $salt;
					// echo $psw_with_salt."\n";
						$hashed = hash("sha512",$psw_with_salt);
						//echo $role;
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO parents(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn','$email','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
						//die($sql);
					#dopo aver inserito quella teacher, testo se funziona	
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
		
		
	    $_POST['addstudent'] = '1'; /*if the user click the add parent buttom */
		$_POST['parent1SSN'] = $ssn;
		$_POST['studentSSN'] = $ssn_s;
		$_POST['studentName'] = $name;
		$_POST['studentSurname'] = $surname;
		$_POST['studentAdress'] = $address;
		$_POST['studentPhone'] = $cellphone;
		$_POST['studentClass'] = $classid;
		$_POST['studentGender'] = $gender;

    	#To test if the student has been added, after the call of function I read the database to see if it exists. The child has already been deleted from database
		include('../add_student.php');
		
		#Now I verify what has been inserted in the database, and if all is correct, student has been added
	
					$sql = "SELECT * FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$numlessons = mysqli_num_rows($result);
					if ($numlessons == 1 ) { 
						$r = mysqli_fetch_array($result);
						$this->assertTrue($name == $r["name"]);						
						$this->assertTrue($surname== $r["surname"]);
						$this->assertTrue($address== $r["address"]);
						$this->assertTrue($cellphone== $r["cellphone"]);
						$this->assertTrue($gender== $r["gender"]);
					}
					else{
						$this->assertTrue(false);
					}
		#DELETION PART
					# I delete the bridge table parent child
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the parent of the child
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the classroom
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
	}
	
	
	
	
	
	
		/**
 * @test
 * @runInSeparateProcess
	**/
   public function test_add_student_two_parents(){
	    #I connect into the database
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
	    #Child informations
					$ssn_s = "123456";
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$homephone = '1234';
					$cellphone ='1234';
					$address = 'via 2';
					$gender = 'f';
					$classid = 'Ctest';
					$nameclassid = 'Ctest';
		#Parent informations
					$ssn1 = 'testtest';
					$ssn2 = 'testest2';
					$email1 = 'test@test.it';
					$email2 = 'test2@test.it';
					$password_base = '123456789';
					$salt = 'abd';
		#DELETION PART
					# I delete the bridge table parent child
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn1' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the table
					$sqltest = "DELETE FROM bridge_class_students WHERE cid = '$classid' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn2' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the parent of the child
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn1';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the classroom
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					$sqltest = "ALTER TABLE students NOCHECK CONSTRAINT all";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
		#INSERTION PART
				   #I must insert the classroom in the class table
					$sqltest = "INSERT INTO class(cid,name) values('$classid','$nameclassid');";
						//die($sql);
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					$pswhashed = hash("sha512",$password_base);
					// echo $pswhashed."\n";
						$psw_with_salt = $pswhashed . $salt;
					// echo $psw_with_salt."\n";
						$hashed = hash("sha512",$psw_with_salt);
						//echo $role;
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO parents(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn1','$email1','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO parents(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn2','$email2','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
		
		
	    $_POST['addstudent'] = '1'; /*if the user click the add parent buttom */
		$_POST['parent1SSN'] = $ssn1;
		$_POST['parent2SSN'] = $ssn2;
		$_POST['studentSSN'] = $ssn_s;
		$_POST['studentName'] = $name;
		$_POST['studentSurname'] = $surname;
		$_POST['studentAdress'] = $address;
		$_POST['studentPhone'] = $cellphone;
		$_POST['studentClass'] = $classid;
		$_POST['studentGender'] = $gender;

    	#To test if the student has been added, after the call of function I read the database to see if it exists. The child has already been deleted from database
		include('../add_student.php');
		
		#Now I verify what has been inserted in the database, and if all is correct, student has been added
	
					$sql = "SELECT * FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$numlessons = mysqli_num_rows($result);
					if ($numlessons == 1 ) { 
						$r = mysqli_fetch_array($result);
						$this->assertTrue($name == $r["name"]);						
						$this->assertTrue($surname== $r["surname"]);
						$this->assertTrue($address== $r["address"]);
						$this->assertTrue($cellphone== $r["cellphone"]);
						$this->assertTrue($gender== $r["gender"]);
					}
					else{
						$this->assertTrue(false);
					}
		#DELETION PART
					# I delete the bridge table parent child
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn1' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = '$ssn2' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the table
					$sqltest = "DELETE FROM bridge_class_students WHERE cid = '$classid' AND ssn_s = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the parent of the child
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn1';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$sqltest = "DELETE FROM parents WHERE ssn = '$ssn2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the classroom
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					# I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
	}
}
?>		




