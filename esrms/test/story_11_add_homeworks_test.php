<?php
	
	// include "../composer/vendor/bin";
	class StackTest extends PHPUnit_Framework_TestCase
	{

	/**
 * @test
 * @runInSeparateProcess
	**/
    public function test_add_homework()
    {
					$ssn = 'testtest';
			
					$subject = 'testsubject';
					$classid = 'ClassTest';
					$classname = 'Class_name_test';
					$description = "test descrizione";
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$address = 'via 2';
					$homephone = '3339484';
					$cellphone = '3948284';
					$date = "2020-12-15";
					
					$hour = "08:00";
					$day = "Tue";
        #I connect into the database
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
		#I delete the teacher inserted
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					} 
		#I delete from timetables
					$sqltest = "DELETE FROM timetables WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the teacher inserted
					$sqltest = "DELETE FROM homeworks WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					} 	
		#I must insert the classroom in the class table
					$sqltest = "INSERT INTO timetables(ttid,cid,ssn_t,subject,hour,day) values('999','$classid','$ssn','$subject','$hour','$day');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		
		
		#I insert the teacher
					$pswhashed = hash("sha512",$password_base);
					// echo $pswhashed."\n";
						$psw_with_salt = $pswhashed . $salt;
					// echo $psw_with_salt."\n";
						$hashed = hash("sha512",$psw_with_salt);
						//echo $role;
					
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO teachers(ssn,email,password,name,surname,salt,address, homephone, cellphone) values('$ssn','$email','$hashed','$name','$surname','$salt','$address', '$homephone','$cellphone')";
						//die($sql);
					#dopo aver inserito quella teacher, testo se funziona	
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
    	#principal informations
		session_start();

				$_POST['addHomework'] = '1';
				$_FILES['file']['name']='';
				$_FILES['file']['type']='';
				$_FILES['file']['tmp_name']='';
				$_SESSION['user_ssn'] = $ssn;
				$_POST['hw_class'] = $classname;
				$_POST['cid'] = $classid;
				$_POST['hw_description'] = $description;
				$_POST['hw_deadline'] = $date;
				$_POST['subject'] = $subject;
				include('../add_homework.php');

		#Now I verify what has been inserted in the database, and if all is correct, student has been added
					$sql = "SELECT * FROM homeworks WHERE cid = '".$classid."' AND ssn_t = '".$ssn."';";
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
					if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					$numprincipals = mysqli_num_rows($result);
					if ($numprincipals == 1 ) { 
						$r = mysqli_fetch_array($result);
						$this->assertTrue($ssn == $r["ssn_t"]);						
						$this->assertTrue($date == $r["deadline"]); #anche se c'è il field date, io l'ho usato come deadline quel nome
						$this->assertTrue($description == $r["description"]);
					}
					else{
						$this->assertTrue(false);
					}	


					
	    #I delete from timetables
					$sqltest = "DELETE FROM timetables WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}	
		#I delete the teacher inserted
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					} 			
		#I delete the teacher inserted
					$sqltest = "DELETE FROM homeworks WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					} 	
	}

   
}

	
	
?>