<?php
	
	// include "../composer/vendor/bin";
	class StackTest extends PHPUnit_Framework_TestCase
	{
		/**
 * @test
 * @runInSeparateProcess
	**/
    public function test_insert_absence()
    {
					$ssn = 'testtest';
					$ssn_s = "123456";
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$subject = 'testsubject';
					$address = 'via 2';
					$classid = 'ClassTest';
					$classid2 = 'ClassTest2';
					$classname = 'Class_name_test';
					$classname2 = 'Class_name_test2';
					$homephone = '3339484';
					$cellphone = '3948284';
					$gender = 'f';

        #I connect into the database
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
		# I delete the absence
					$sqltest = "DELETE FROM notes WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}			
		#I delete the class inserted
					$sqltest = "DELETE FROM class WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}			
		#I delete the teacher inserted
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the bridge class student
					$sqltest = "DELETE FROM bridge_class_students WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}			
					
		# I insert informations
					#I insert the class
					$sqltest = "INSERT INTO class (cid,name) values ($classid,$classname)";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}	
					#I insert the teacher
					$pswhashed = hash("sha512",$password_base);
					// echo $pswhashed."\n";
						$psw_with_salt = $pswhashed . $salt;
					// echo $psw_with_salt."\n";
						$hashed = hash("sha512",$psw_with_salt);
						//echo $role;
					
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO teachers(ssn,email,password,name,surname,salt,address, homephone, cellphone) values('$ssn','$email','$hashed','$name','$surname','$salt','$address', '$homephone','$cellphone')";
						//die($sql);
					#dopo aver inserito quella teacher, testo se funziona	
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
					
					#I insert in the bridge table class teacher
					$sqltest = "INSERT INTO bridge_class_teachers(ssn_t,cid,subject) values('$ssn','$classid','$subject');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
					#I insert manually the students in the class
					$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s','$surname','$name','$address','$cellphone','$gender')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
					#I insert the bridge_class_students
					$sqltest = "INSERT INTO bridge_class_students(cid,ssn_s) values('$classid','$ssn_s')";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
					
    	#principal informations
				$descrip = "student test is noisy in class";
	
				$_POST['ssn_t'] = $ssn;     // Or $_POST['ssn_t']
				$_POST['cid'] = $classid;
				$_POST['class'] = $classname;
				$_POST['description_123456']  = $descrip;

				include('../insert_note.php');

		#Now I verify what has been inserted in the database, and if all is correct, student has been added
					$sql = "SELECT * FROM notes WHERE ssn_s = '".$ssn_s."' AND ssn_t = '".$ssn."';";
					if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					$numprincipals = mysqli_num_rows($result);
					if ($numprincipals == 1 ) { 
						$r = mysqli_fetch_array($result);
						$this->assertTrue($ssn_s == $r["ssn_s"]);						
						$this->assertTrue($descrip == $r["description"]);
						$this->assertTrue($ssn == $r["ssn_t"]);
					}
					else{
						$this->assertTrue(false);
					}				
		# I delete the absence
					$sqltest = "DELETE FROM notes WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}			
		#I delete the class inserted
					$sqltest = "DELETE FROM class WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}			
		#I delete the teacher inserted
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the student
					$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
		#I delete the bridge class student
					$sqltest = "DELETE FROM bridge_class_students WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}	
					
	
	}	
		
	

   
}

	
	
?>