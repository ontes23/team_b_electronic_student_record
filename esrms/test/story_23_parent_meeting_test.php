<?php
class StackTest extends PHPUnit_Framework_TestCase{# la roba commentata giù è necessaria per far partire i test anche se ho headers
	/**
 * @test
 * @runInSeparateProcess
	**/
  public function test_add_reservation_parent(){
	    #I connect into the database
					
        session_start(); 
     					
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
					
	# test executed on classId = (testX)  
	
		$classid = "TestID";
		$classname = "TestName";
#Teacher informations
					$ssn = 'testtest';
					
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					
					$address = 'via 2';

					$homephone = '3339484';
					$cellphone = '3948284';
					$hour = '08:00';
					$day = 'Monday';
					$date = "12/09/2019";
					$description = "bellissima lezione";
#Child informations
					$ssn_s = "123456";
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$homephone = '1234';
					$cellphone ='1234';
					$address = 'via 2';
					$gender = 'f';
#Parent informations
					$p_name = "Luigi";
					$p_surname = "Carinucci";
					$p_name2 = "Luigino";
					$p_surname2 = "Carinuccietto";					
		$subject = 'testsubject';
	
		#DELETION PART
		include('delete_functions/delete_meetings.php');
		include('delete_functions/delete_class.php');
		include('delete_functions/delete_teachers.php');
		include('delete_functions/delete_bridge_class_teachers.php');	
				
		#INSERTION PART		
		include('insert_functions/insert_class.php');
		include('insert_functions/insert_teachers.php');
		include('insert_functions/insert_bridge_class_teachers.php');
		
	
		
		#INSERT new data
		$_SESSION['user_ssn'] = $ssn;
		$_POST['hour'] = $hour;
		$_POST['day'] = $day;
		$_SESSION["name_u"] = $p_name;
		$_SESSION["surname_u"] = $p_surname;
		$_POST['ssn_t']	= $ssn;	
		
		include("../add_meeting.php");
		include("../add_reservations.php");

		#This will add
		$_POST['day'] = $day = "Tuesday";
		include("../add_meeting.php");
		include("../add_reservations.php");		


		
		
		#CHECK how many rows are present
			$sql = "SELECT COUNT(*) as count FROM meetings WHERE ssn_t LIKE 'test%' AND p_name = '$p_name' AND p_surname = '$p_surname';";
			$result = mysqli_query($conn,$sql);
			$row = $result->fetch_assoc();
			$count = $row["count"];
			$this->assertTrue($count == "2");
			
		
		#DELETION PART
		include('delete_functions/delete_meetings.php');
		include('delete_functions/delete_class.php');
		include('delete_functions/delete_teachers.php');
		include('delete_functions/delete_bridge_class_teachers.php');	

	}
	
	/**
 * @test
 * @runInSeparateProcess
	**/
  public function test_delete_reservation_parent(){
	    #I connect into the database
					
        session_start(); 
     					
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
					
	# test executed on classId = (testX)  
	
		$classid = "TestID";
		$classname = "TestName";
#Teacher informations
					$ssn = 'testtest';
					
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					
					$address = 'via 2';

					$homephone = '3339484';
					$cellphone = '3948284';
					$hour = '08:00';
					$day = 'Monday';
					$date = "12/09/2019";
					$description = "bellissima lezione";
#Child informations
					$ssn_s = "123456";
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$homephone = '1234';
					$cellphone ='1234';
					$address = 'via 2';
					$gender = 'f';
#Parent informations
					$p_name = "Luigi";
					$p_surname = "Carinucci";
					$p_name2 = "Luigino";
					$p_surname2 = "Carinuccietto";					
		$subject = 'testsubject';
	
		#DELETION PART
		include('delete_functions/delete_meetings.php');
		include('delete_functions/delete_class.php');
		include('delete_functions/delete_teachers.php');
		include('delete_functions/delete_bridge_class_teachers.php');	
				
		#INSERTION PART		
		include('insert_functions/insert_class.php');
		include('insert_functions/insert_teachers.php');
		include('insert_functions/insert_bridge_class_teachers.php');
		
	
		
		#INSERT new data
		$_SESSION['user_ssn'] = $ssn;
		$_POST['hour'] = $hour;
		$_POST['day'] = $day;
		$_SESSION["name_u"] = $p_name;
		$_SESSION["surname_u"] = $p_surname;
		$_POST['ssn_t']	= $ssn;	
		
		include("../add_meeting.php");
		include("../add_reservations.php");
		include("../delete_reservation.php");

		#This will add
		$_POST['day'] = $day = "Tuesday";
		include("../add_meeting.php");
		include("../add_reservations.php");		


		
		
		#CHECK how many rows are present
			$sql = "SELECT COUNT(*) as count FROM meetings WHERE ssn_t LIKE 'test%' AND p_name = '$p_name' AND p_surname = '$p_surname';";
			$result = mysqli_query($conn,$sql);
			$row = $result->fetch_assoc();
			$count = $row["count"];
			$this->assertTrue($count == "1");
			
		
		#DELETION PART
		include('delete_functions/delete_meetings.php');
		include('delete_functions/delete_class.php');
		include('delete_functions/delete_teachers.php');
		include('delete_functions/delete_bridge_class_teachers.php');	

	}
}

?>		




