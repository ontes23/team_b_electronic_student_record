<?php
class StackTest extends PHPUnit_Framework_TestCase{# la roba commentata gi� � necessaria per far partire i test anche se ho headers
	/**
 * @test
 * @runInSeparateProcess
	**/
   public function test_insert_record_lesson(){
	  #I connect to the database
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
	  #Teacher informations
					$ssn = 'testtest';
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$subject = 'testsubject';
					$address = 'via 2';
					$classid = 'ClassTest';
					$classid2 = 'ClassTest2';
					$classname = 'Class_name_test';
					$classname2 = 'Class_name_test2';
					$homephone = '3339484';
					$cellphone = '3948284';
					$hour = '08:00';
					$day = 'Mon';
					$date = "12/09/2019";
					$description = "bellissima lezione";
	  #DELETION PART
					#I delete the teacher inserted
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the class inserted
					$sqltest = "DELETE FROM class WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM timetables WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM timetables WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
	  #INSERTION PART
					
					#I insert the teacher
					$pswhashed = hash("sha512",$password_base);
					// echo $pswhashed."\n";
						$psw_with_salt = $pswhashed . $salt;
					// echo $psw_with_salt."\n";
						$hashed = hash("sha512",$psw_with_salt);
						//echo $role;
					
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO teachers(ssn,email,password,name,surname,salt,address, homephone, cellphone) values('$ssn','$email','$hashed','$name','$surname','$salt','$address', '$homephone','$cellphone')";
						//die($sql);
					#dopo aver inserito quella teacher, testo se funziona	
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO class(cid,name) values('$classid','$classname');";
						//die($sql);
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO class(cid,name) values('$classid2','$classname2');";
						//die($sql);
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO bridge_class_teachers(ssn_t,cid,subject) values('$ssn','$classid','$subject');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO bridge_class_teachers(ssn_t,cid,subject) values('$ssn','$classid2','$subject');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO timetables(ttid,cid,ssn_t,subject,hour,day) values('998','$classid','$ssn','$subject','$hour','$day');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO timetables(ttid,cid,ssn_t,subject,hour,day) values('999','$classid2','$ssn','$subject','$hour','$day');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					
					
	  #ACTUAL TEST
					$_POST['classe'] = $classname2;
					$_POST["ssn_t"] = $ssn;
					$_POST["date"] = $date;
					$_POST["time"] = $hour;
					$_POST["description"] = $description;
					$_POST["subject"] = $subject;
					$_POST["SSNU"] = $ssn;
					
					include("../insert_lesson_record.php");
	  
	  # Now I verify that the correct row has been inserted
					$sql = "SELECT * FROM lessons WHERE lid = '$temp';";
					if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					$numlessons = mysqli_num_rows($result);
					if ($numlessons == 1 ) { 
						$r = mysqli_fetch_array($result);
						$this->assertContains($classid2, $r["cid"]);
						$this->assertContains($ssn, $r["ssn_t"]);
						$this->assertContains($date, $r["date"]);
						$this->assertContains($hour, $r["time"]);
						$this->assertContains($description, $r["description"]);	
					}
					else{
						$this->assertTrue(false);
					}
	  # Now I can delete previous things
					#I delete the lesson inserted
					$sqltest = "DELETE FROM lessons WHERE lid = '$temp';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the teacher inserted
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the class inserted
					$sqltest = "DELETE FROM class WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the class inserted
					$sqltest = "DELETE FROM class WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					}
					
					#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM bridge_class_teachers WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM timetables WHERE cid = '$classid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
					#I delete the class inserted
					$sqltest = "DELETE FROM timetables WHERE cid = '$classid2';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell�inserimento del post, riprovare";
					} 
   }
}
?>		




