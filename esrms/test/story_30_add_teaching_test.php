<?php
	
	// include "../composer/vendor/bin";
	class StackTest extends PHPUnit_Framework_TestCase
	{
	/**
 * @test
 * @runInSeparateProcess
	**/
    public function test_add_teaching()
    {
					$ssn = 'testtest';
					$cid = 'testcid';
					$classname = 'testclassname';
					$subject = 'testsubject';

        #I connect into the database
					$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
					if (mysqli_connect_errno()) {
						#echo "Connessione fallita: ".
						mysqli_connect_error();
						exit();
					}
					
					
					# I delete the teaching of the child
					$sqltest = "DELETE FROM bridge_class_teachers WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}	
					
					# I delete the teacher of the child
					$sqltest = "DELETE FROM teachers WHERE ssn = '$ssn';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
					
					# I delete the class of the teacher
					$sqltest = "DELETE FROM class WHERE cid = '$cid';";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
				
					#I pre insert the informations
					
					
					#I need to insert the teacher
					
					$email = 'test@test.it';
					$password_base = '123456789';
					$salt = 'abd';
					$name = 'ADELAIDETESTINGNAME';
					$surname = 'parolini';
					$subject = 'storia';
					$address = 'via 2';
					$cellphone = '33399933';
					$homephone = '30000933';
					
					$pswhashed = hash("sha512",$password_base);
					$psw_with_salt = $pswhashed . $salt;
					$hashed = hash("sha512",$psw_with_salt);
				
					# I must insert a parent in the parent table
					 $sqltest = "INSERT INTO teachers(ssn,email,password,name,surname,salt,address,homephone,cellphone) values('$ssn','$email','$hashed','$name','$surname','$salt','$address','$homephone','$cellphone')";
					#dopo aver inserito quella teacher, testo se funziona	
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
					#I need to insert the class
					
					#I must insert the classroom in the class table
					$sqltest = "INSERT INTO class(cid,name) values('$cid','$classname');";
					if(!$result =mysqli_query($conn,$sqltest)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					
    	#teaching informations
					$_POST['addteaching'] = '1';
					$_POST['teacherSSN'] = $ssn;
					$_POST['class_name'] = $classname;
					$_POST['subject'] = $subject;
	
	
					include('../add_teaching.php');
					
		#Now I verify what has been inserted in the database, and if all is correct, student has been added
	
					$sql = "SELECT * FROM bridge_class_teachers WHERE ssn_t = '$ssn';";
					if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell’inserimento del post, riprovare";
					}
					$numteachings = mysqli_num_rows($result);
					if ($numteachings ==  1) { 
						$r = mysqli_fetch_array($result);
						$this->assertTrue($ssn == $r["ssn_t"]);						
						$this->assertTrue($cid== $r["cid"]);
						$this->assertTrue($subject== $r["subject"]);
					}
					else{
						$this->assertTrue(false);
					}				
	
		# I delete the teaching of the child
					
	
	
	
	
	
	
	
	}

   
}

	
	
?>