<?php

class StackTest extends PHPUnit_Framework_TestCase{# la roba commentata gi� � necessaria per far partire i test anche se ho headers
	
	
	/**
 * @test
 * @runInSeparateProcess
	**/
    public function testloginwrong()
    {

      #carico dati fittizi per vedere se mi ritorna cose giuste
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
			
		if (mysqli_connect_errno()) {
			#echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM teachers WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
		$ssn = 'testtest';
		$email = 'test@test.it';
		$password_base = '123456789';
		$salt = 'abd';
		
		$pswhashed = hash("sha512",$password_base);
		// echo $pswhashed."\n";
			$psw_with_salt = $pswhashed . $salt;
		// echo $psw_with_salt."\n";
			$hashed = hash("sha512",$psw_with_salt);
		
		$name = 'ADELAIDETESTINGNAME';
		$surname = 'parolini';
		$address = 'via 2';
		$homephone =  '091432849';
		$cellphone = '321434545';
			//echo $role;
     	 $sqltest = "INSERT INTO teachers(ssn,email,password,name,surname,salt,address,homephone,cellphone) values('$ssn','$email','$hashed','$name','$surname','$salt','$address','$homephone','$cellphone')";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$_POST['$name']="ADELAIDETESTINGNAME";
		$_POST['role'] = 'teachers';
		$_POST['email'] = 'test@test.it';
		$_POST['password'] = 'passwdsbagliata';
		#ob_start();
	    include('../validation.php');
		#ob_end_clean();
		$this->assertContains('location:wrong_credentials.php', $header_selected);
		
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM teachers WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
    }
	
	
	
	/**
 * @test
 * @runInSeparateProcess
	**/
    public function testloginteacher()
    {

      #carico dati fittizi per vedere se mi ritorna cose giuste
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
			
		if (mysqli_connect_errno()) {
			#echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM teachers WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
		$ssn = 'testtest';
		$email = 'test@test.it';
		$password_base = '123456789';
		$salt = 'abd';
		
		$pswhashed = hash("sha512",$password_base);
		// echo $pswhashed."\n";
			$psw_with_salt = $pswhashed . $salt;
		// echo $psw_with_salt."\n";
			$hashed = hash("sha512",$psw_with_salt);
		
		$name = 'ADELAIDETESTINGNAME';
		$surname = 'parolini';
		$subject = 'storia';
		$address = 'via 2';
		$homephone = '09149234';
		$cellphone = '32109432';
			//echo $role;
     	 $sqltest = "INSERT INTO teachers(ssn,email,password,name,surname,salt,address,homephone,cellphone) values('$ssn','$email','$hashed','$name','$subject','$salt','$address','$homephone','$cellphone')";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$_POST['$name']="ADELAIDETESTINGNAME";
		$_POST['role'] = 'teachers';
		$_POST['email'] = 'test@test.it';
		$_POST['password'] = '123456789';
		#ob_start();
	    include('../validation.php');
		#ob_end_clean();
		$this->assertContains('location:teacher_page.php', $header_selected);
		
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM teachers WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
    }
	
	/**
 * @test
 * @runInSeparateProcess
	**/
	
	    public function testloginadministrators()
    {

      #carico dati fittizi per vedere se mi ritorna cose giuste
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
			
		if (mysqli_connect_errno()) {
			#echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM administrators WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
		$ssn = 'testtest';
		$email = 'test@test.it';
		$password_base = '123456789';
		$salt = 'abd';
		
		$pswhashed = hash("sha512",$password_base);
		// echo $pswhashed."\n";
			$psw_with_salt = $pswhashed . $salt;
		// echo $psw_with_salt."\n";
			$hashed = hash("sha512",$psw_with_salt);
		
		$name = 'ADELAIDETESTINGNAME';
		$surname = 'parolini';
		$homephone ='1234';
		$cellphone ='1234';
		$address = 'via 2';
			//echo $role;
     	 $sqltest = "INSERT INTO administrators(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn','$email','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$_POST['$name']="ADELAIDETESTINGNAME";
		$_POST['role'] = 'administrators';
		$_POST['email'] = 'test@test.it';
		$_POST['password'] = '123456789';
		#ob_start();
	    include('../validation.php');
		#ob_end_clean();
		$this->assertContains('location:administrator_page.php', $header_selected);
		
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM administrators WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
    }
	
	
		/**
 * @test
 * @runInSeparateProcess
	**/
	
	    public function testloginparent1child()
    {

      #carico dati fittizi per vedere se mi ritorna cose giuste
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
			
		if (mysqli_connect_errno()) {
			#echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
		$ssn_s = "123456";

		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM parents WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
			#I delete previous child inserted
		$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		#I delete previous child inserted
		$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = 'testtest' AND ssn_s = '123456';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		}
		###
		
		$ssn = 'testtest';
		$email = 'test@test.it';
		$password_base = '123456789';
		$salt = 'abd';
		
		$pswhashed = hash("sha512",$password_base);
		// echo $pswhashed."\n";
			$psw_with_salt = $pswhashed . $salt;
		// echo $psw_with_salt."\n";
			$hashed = hash("sha512",$psw_with_salt);
		
		$name = 'ADELAIDETESTINGNAME';
		$surname = 'parolini';
		$homephone ='1234';
		$cellphone ='1234';
		$address = 'via 2';
			//echo $role;
		$sqltest = "INSERT INTO parents(ssn, surname, name, address, homephone, cellphone, email, password, salt) values('$ssn','$surname','$name','$address','$homephone','$cellphone','$email','$hashed','$salt')";
     	 //$sqltest = "INSERT INTO parents(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn','$email','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$_POST['$name']="ADELAIDETESTINGNAME";
		$_POST['role'] = 'parents';
		$_POST['email'] = 'test@test.it';
		$_POST['password'] = '123456789';
		
		#I must insert a child in the bridge table
		$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s','$surname','$name','$address','$cellphone','m');";
		//$sqltest = "INSERT INTO students(ssn,name,surname,cellphone,address,gender) values('$ssn_s','$name','$surname','$cellphone','$address','m');";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
	    $sqltest = "INSERT INTO bridge_parents_students(ssn_p,ssn_s) values('$ssn','$ssn_s');";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		#ob_start();
	    include('../validation.php');
		#ob_end_clean();
		$this->assertContains('location:one_child.php', $header_selected);
		
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = 'testtest' AND ssn_s = '$ssn_s';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$sqltest = "DELETE FROM parents WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
    }
	
			/**
 * @test
 * @runInSeparateProcess
	**/
	
	public function testloginparent2child()
    {

      #carico dati fittizi per vedere se mi ritorna cose giuste
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
			
		if (mysqli_connect_errno()) {
			#echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
		$ssn_s1 = "123456";
		$ssn_s2 = "78910";

		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM parents WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
			#I delete previous child inserted
			$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s1';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
			#I delete the other child
			$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s2';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
			$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = 'testtest' AND ssn_s = '$ssn_s1';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
			$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = 'testtest' AND ssn_s = '$ssn_s2';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		
		###
		
		$ssn = 'testtest';
		$email = 'test@test.it';
		$password_base = '123456789';
		$salt = 'abd';
		
		$pswhashed = hash("sha512",$password_base);
		// echo $pswhashed."\n";
			$psw_with_salt = $pswhashed . $salt;
		// echo $psw_with_salt."\n";
			$hashed = hash("sha512",$psw_with_salt);
		
		$name = 'ADELAIDETESTINGNAME';
		$surname = 'parolini';
		$homephone ='1234';
		$cellphone ='1234';
		$address = 'via 2';
			//echo $role;
		$sqltest = "INSERT INTO parents(ssn, surname, name, address, homephone, cellphone, email, password, salt) values('$ssn','$surname','$name','$address','$homephone','$cellphone','$email','$hashed','$salt')";
		//$sqltest = "INSERT INTO parents(ssn,email,password,name,surname,homephone,cellphone,salt,address) values('$ssn','$email','$hashed','$name','$surname','$homephone','$cellphone','$salt','$address')";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$_POST['$name']="ADELAIDETESTINGNAME";
		$_POST['role'] = 'parents';
		$_POST['email'] = 'test@test.it';
		$_POST['password'] = '123456789';
		
		#I must insert the first child 1 in the bridge table
		$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s1','$surname','$name','$address','$cellphone','m');";
		//$sqltest = "INSERT INTO students(ssn,name,surname,cellphone,address,gender) values('$ssn_s1','$name','$surname','$cellphone','$address','m');";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell'inserimento del post, riprovare";
		}
		#I must insert the second child 1 in the bridge table
		$sqltest = "INSERT INTO students(ssn,surname,name,address,cellphone,gender) values('$ssn_s2','$surname','$name','$address','$cellphone','m');";
		//$sqltest = "INSERT INTO students(ssn,name,surname,cellphone,address,gender) values('$ssn_s2','$name','$surname','$cellphone','$address','m');";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
	    $sqltest = "INSERT INTO bridge_parents_students(ssn_p,ssn_s) values('$ssn','$ssn_s1');";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$sqltest = "INSERT INTO bridge_parents_students(ssn_p,ssn_s) values('$ssn','$ssn_s2');";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		#ob_start();
	    include('../validation.php');
		#ob_end_clean();
		$this->assertContains('location:parent_selection_child.php', $header_selected);

		$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = 'testtest' AND ssn_s = '$ssn_s1';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$sqltest = "DELETE FROM bridge_parents_students WHERE ssn_p = 'testtest' AND ssn_s = '$ssn_s2';";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM parents WHERE ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s1'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$sqltest = "DELETE FROM students WHERE ssn = '$ssn_s2'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}

		###
	}
	

		/**
 * @test
 * @runInSeparateProcess
	**/
	
	public function testloginsysadmin()
    {

      #carico dati fittizi per vedere se mi ritorna cose giuste
		$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
			
		if (mysqli_connect_errno()) {
			#echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM sysadmins WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
		$ssn = 'testtest';
		$email = 'test@test.it';
		$password_base = '123456789';
		$salt = 'abd';
		
		$pswhashed = hash("sha512",$password_base);
		// echo $pswhashed."\n";
			$psw_with_salt = $pswhashed . $salt;
		// echo $psw_with_salt."\n";
			$hashed = hash("sha512",$psw_with_salt);
		
		$name = 'ADELAIDETESTINGNAME';
		$surname = 'parolini';
		
			//echo $role;
     	 $sqltest = "INSERT INTO sysadmins(ssn,email,password,name,surname,salt) values('$ssn','$email','$hashed','$name','$surname','$salt')";
			//die($sql);
		#dopo aver inserito quella teacher, testo se funziona	
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		$_POST['$name']="ADELAIDETESTINGNAME";
		$_POST['role'] = 'sysadmins';
		$_POST['email'] = 'test@test.it';
		$_POST['password'] = '123456789';
		#ob_start();
	    include('../validation.php');
		#ob_end_clean();
		$this->assertContains('location:sysadmin_page.php', $header_selected);
		
		#cancello la row precedentemente messa in altri test
		$sqltest = "DELETE FROM sysadmins WHERE email='test@test.it' AND ssn = 'testtest'; ";
		if(!$result =mysqli_query($conn,$sqltest)) {
			$msg = "Errore nell�inserimento del post, riprovare";
		}
		###
    }
}
?>		




