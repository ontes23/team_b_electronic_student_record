<!doctype html>
<html lang="en">
  <head>
  <title>Parent page</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
      <link href="css/parent_page.css" rel="stylesheet" />

<?php
    include("sidebars.php");
	if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
	
    define("SSNS","ssn_s");
    define("NAMES","name_s");
    define("SURNAMES","surname_s");
    define("TEST","test_in_action");
    define("FALSE","false");
    if(isset($_POST[SSNS])){
    $_SESSION[SSNS] = $_POST[SSNS];
    }

    if(isset($_POST[NAMES])){
    $_SESSION[NAMES] = $_POST[NAMES];
    }
    if(isset($_POST[SURNAMES])){
    $_SESSION[SURNAMES] = $_POST[SURNAMES];
    }
	if($_SESSION[TEST]!='1'){
		$previousPage = basename($_SERVER['HTTP_REFERER']);
	
    global $enabled;
    if($previousPage=="login.php" && (!isset($_POST['name_s']) || !isset($_POST['surname_s']))){
        $enabled = FALSE;
    }
    else{
        $enabled = "true";
    }
	}
?>

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" >


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

 </head>
 <body>
 
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Parent Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded=FALSE aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
      <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>



<div class="container-fluid">
  <div class="row" >
    <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen" >
        <ul class="nav flex-column">
		 <li class="nav-item">
			<div class="user-info">
					<div class="image"><img src="photos/user.png" alt="User"></div>
					<div class="detail">
							<?php
								if($_SESSION[TEST]!='1'){

									if($enabled == FALSE){
											echo "<font color='red'><b>" . "Oops! No children to show..." . "</b></font>";
									}
									else{
											echo "<h4>" . $_SESSION[NAMES]." ".$_SESSION[SURNAMES] . "</h4>".
													"<small>" . $_SESSION["name_u"] . " " . $_SESSION["surname_u"]."</small>";
									}
								}
							?>
					</div>
			</div>
		 </li>
			<?php
				if($_SESSION[TEST]!='1'){

					if($enabled == TRUE){
						parent_print_sidebar("true");
          }
				}
			?>
				</ul>

      </div>
    </nav>
		

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
       


<div class="container" style="min-width:100%"><br>


        <div class="mt-5 mb-5">
            <div class="row">
                <div class="col-md-6">
                    <table  style="width: 100%; height: 100%;">
					<caption></caption>
                        <thead>
                        <tr>
                            <th style="border: none;" scope="col"><img style="width:75px;height:75px; " src="photos/notes_icon.png" alt="icon"></th>
                            <th scope="col"style="border: none;" scope="col"><h1>Recent news</h1></th>
                        </tr>
                        </thead>
                    </table>

                    <ul class="timeline">
                        <?php
                        if(isset($_SESSION["ssn_s"])){
						$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
						
                        $sql = "SELECT * FROM bridge_class_students WHERE ssn_s = '".$_SESSION["ssn_s"]."'  ";
                        if(!$result = mysqli_query($conn,$sql)) {
                            $msg = "Errore nell’inserimento del post, riprovare";
                        }
                        while($row = $result->fetch_assoc()) {
                            $classid = $row["cid"];
                        }
                        $sql = "SELECT * FROM communications WHERE cid = '".$classid."' ORDER BY publication_date DESC ";
                        if(!$result = mysqli_query($conn,$sql)) {
                            $msg = "Errore nell’inserimento del post, riprovare";
                        }

						$num_rows = mysqli_num_rows($result); 
                        while($row = $result->fetch_assoc()) {
                            $object =  $row["object"];
                            $publication_date = $row["publication_date"];
                            $description = $row["description"];


                            echo "<li>";
                            echo "<p class='title'>$object</p>";
                            echo "<p class='float-right date' >$publication_date</p>";
                            echo "<p class='description'>$description</p>";
                            echo "</li>";
                        }
                        echo "</ul>";
						}
                        ?>


                </div>
            </div>
        </div>

        <div class="text-muted mt-5 mb-5 text-center small"></div>



    </main>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
