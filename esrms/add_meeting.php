<?php
	if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
if (mysqli_connect_errno()) {
    //echo "Connessione fallita: ".
    mysqli_connect_error();
    exit();
}
$ssn_t= $_SESSION['user_ssn'];
$hour = $_POST['hour'];
$day = $_POST['day'];
$sql = "SELECT * From timetables where hour=? AND day=? AND ssn_t=?";
$stmt = mysqli_stmt_init($conn);
if(!mysqli_stmt_prepare($stmt,$sql)){
    //header("Location: myHome.php?error=sqlfailed");
    exit();
}else{
    mysqli_stmt_bind_param($stmt, "sss",$hour, $day,$ssn_t);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);


    if ($row = mysqli_fetch_assoc($result))
    {
        echo "This time slot is not free!!!";
        exit();
    }
    else {
        $sql = "SELECT * From meetings where ssn_t=? AND hour=? AND day=?";
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            //header("Location: myHome.php?error=sqlfailed");
            exit();
        }else{
            mysqli_stmt_bind_param($stmt, "sss",$ssn_t, $hour,$day);
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);


            if ($row = mysqli_fetch_assoc($result))
            {
                echo "<p style='color: red'><strong>This time slot is not free!!!</strong></p>";
            }
            else {

                $sql2 = "INSERT INTO meetings(ssn_t,hour,day) VALUES(?,?,?);"; //for safety
                $stmt = mysqli_stmt_init($conn);
                if(!mysqli_stmt_prepare($stmt,$sql2)){
                    exit();
                }else{
                    mysqli_stmt_bind_param($stmt, "sss",$ssn_t,$hour,$day);
                    mysqli_stmt_execute($stmt);
                    echo "<p style='color: green'><strong>Assigned successfully</strong></p>";
                }

            }
        }



    }
}




