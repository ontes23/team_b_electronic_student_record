<?php
define("CALENDAR","calendar");
define("PHPSELF","PHP_SELF");
define("FALSE1","false");
define("INACTIVE","inactive");
define("ACTIVE","active");
define("USERPLUS","user-plus");
function parent_print_sidebar($enabled)
{
    /* --> EDIT THIS WHEN ADDING A NEW PAGE FOR THE PARENT <--
       $sidebarPages is an associative array where:
        key is the current page
        val is
            a string (referred mainpage) if key is subpage
            an array of 2 elements (label, icon) if key is a mainpage
    */
    $sidebarPages = array(
        "parent_page.php" => array("Home", "home"),
        "parent_page_marks.php" => array("Marks", "edit"),
        "parent_check_attendance.php" => array("Attendance", "check-square"),
        "parent_page_homework.php" => array("Homework", "book-open"),
        "parent_support_material.php" => array("Support Material", "book"),
        "parent_page_timetable.php" => array("Timetable", CALENDAR),
        "parent_page_meetings.php" => array("Meetings", "users"),
        "parent_page_notes.php" => array("Notes", "check-square")
    );

    $currentPage = basename($_SERVER[PHPSELF]);
    $currentPage_referredPage = $sidebarPages[$currentPage][0];

    foreach($sidebarPages as $key => $val){
        if(count($val) > 1) {
            echo "<li class='nav-item'>
                    <a class='nav-link ";

            if($enabled == FALSE1){
                echo INACTIVE;
            } else {
                if ($key==$currentPage || $key==$currentPage_referredPage){
                    echo ACTIVE;
                }
                echo "' href='" . $key;
            }
            echo "'><span data-feather='" . $val[1] . "'></span>" .
                $val[0] . "<span class='sr-only'>(current)</span>
                    </a>
                  </li>";
        }
    }
}

function teacher_print_sidebar($enabled)
{
    /* --> EDIT THIS WHEN ADDING A NEW PAGE FOR THE TEACHER <--
       $sidebarPages is an associative array where:
        key is the current page
        val is
            a string (referred mainpage) if key is subpage
            an array of 2 elements (label, icon) if key is a mainpage
    */
    $sidebarPages = array(
        "teacher_page.php" => array("Home", "home"),
        "teacher_page_lecture.php" => array("Record Lecture", "edit"),
        "teacher_page_marks.php" => array("Record Marks", "edit"),
        "teacher_page_absences.php" => array("Record Absence", "edit"),
		"teacher_page_notes.php" => array("Write a note", "edit"),
		"teacher_page_homework.php" => array("Add Homework", "file-plus"),
        "teacher_page_material.php" => array("Add Material", "file-plus"),
        "teacher_page_timetable.php" => array("View Timetable", CALENDAR),
        "teacher_page_meetings.php" => array("Manage Meetings", "users"),
		"teacher_page_final_mark.php" => array("Final Marks", "edit")
    );

    $currentPage = basename($_SERVER[PHPSELF]);
    $currentPage_referredPage = $sidebarPages[$currentPage][0];

    foreach($sidebarPages as $key => $val){
        if(count($val) > 1) {
            echo "<li class='nav-item'>
                    <a class='nav-link ";

            if($enabled == FALSE1){
                echo INACTIVE;
            }else {
                if ($key==$currentPage || $key==$currentPage_referredPage){
                    echo ACTIVE;
                }
                echo "' href='" . $key;
            }
            echo "'><span data-feather='" . $val[1] . "'></span>" .
                $val[0] . "<span class='sr-only'>(current)</span>
                    </a>
                  </li>";
        }
    }
}

function administrator_print_sidebar($enabled)
{
    /* --> EDIT THIS WHEN ADDING A NEW PAGE FOR THE ADMINISTRATIVE OFFICER <--
       $sidebarPages is an associative array where:
        key is the current page
        val is
            a string (referred mainpage) if key is subpage
            an array of 2 elements (label, icon) if key is a mainpage
    */
    $sidebarPages = array(
        "administrator_page.php" => array("Home", "home"),
        "administrator_add_parent.php" => array("Add Parent", USERPLUS),
        "administrator_add_student.php" => array("Add Student", USERPLUS),
        "administrator_enter_class.php" => array("Enter Class Composition", "sidebar"),
        "administrator_add_timetable.php" => array("Manage Timetables", CALENDAR),
        "administrator_add_communication.php" => array("Add Communication", "message-square"),
        "administrator_view_selected_class.php" => array("administrator_enter_class.php"),
    );

    $currentPage = basename($_SERVER[PHPSELF]);
    $currentPage_referredPage = $sidebarPages[$currentPage][0];

    foreach($sidebarPages as $key => $val){
        if(count($val) > 1) {
            echo "<li class='nav-item'>
                    <a class='nav-link ";

            if($enabled == FALSE1){
                echo INACTIVE;
             } else {
                if ($key==$currentPage || $key==$currentPage_referredPage){
                    echo ACTIVE;
                }
                echo "' href='" . $key;
            }
            echo "'><span data-feather='" . $val[1] . "'></span>" .
                $val[0] . "<span class='sr-only'>(current)</span>
                    </a>
                  </li>";
        }
    }
}

function systemadmin_print_sidebar($enabled)
{
	$sidebarPages = array(
        "sysadmin_page.php" => array("Home", "home"),
        "system_administrator_add_officer.php" => array("Add Officer", USERPLUS),
        "system_administrator_add_teacher.php" => array("Add Teacher", USERPLUS),
		"system_administrator_add_teaching.php" => array("Add Teaching", USERPLUS),
        "system_administrator_add_principal.php" => array("Add Principal", USERPLUS),
    );	
	 $currentPage = basename($_SERVER[PHPSELF]);
    $currentPage_referredPage = $sidebarPages[$currentPage][0];
	
    foreach($sidebarPages as $key => $val){
        if(count($val) > 1) {
            echo "<li class='nav-item'>
                    <a class='nav-link ";

            if($enabled == FALSE1){
                echo INACTIVE;
            }else {
                if ($key==$currentPage || $key==$currentPage_referredPage){
                    echo ACTIVE;
                }
                echo "' href='" . $key;
            }
            echo "'><span data-feather='" . $val[1] . "'></span>" .
                $val[0] . "<span class='sr-only'>(current)</span>
                    </a>
                  </li>";
        }
    }
	
}

?>