<?php
session_start();

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">

	<meta name="viewport" content="width=device-width, initial-scale=1" />

</head>
<body style="background-color:#343a40!important;">

<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Parent Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
			<button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>


<div style="padding-top:5%;">
<table class="table" style ="background-color:white;">
<caption></caption>
<?php
	$servername = "localhost";
	$username = "teamb";
	$pswd = "teamb";
	$dbname = "esrms";

	// Create connection
	$conn = new mysqli($servername, $username, $pswd, $dbname);
	// Check connection
	if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
	}
	$sql = "SELECT ssn, name, surname FROM students AS s, bridge_parents_students AS bps WHERE bps.ssn_p = '" . $_SESSION['user_ssn'] . "' AND bps.ssn_s = s.ssn";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
?>
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Surname</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    
<?php
	$i=1;
    // output data of each row
    while($row = $result->fetch_assoc()) {
?>
		<tr>
	  <th scope="row"><?php echo $i?></th>
      
<?php
        echo "<td>" . $row["name"]. "</td><td>" . $row["surname"]."</td><br>";
?>
		<td><form action="./parent_page.php" method="post"><input type='hidden' name='ssn_s' value=<?php echo $row['ssn'];?>> <input type='hidden' name='name_s' value=<?php echo $row['name'];?>> <input type='hidden' name='surname_s' value=<?php echo $row['surname'];?>><button  type="submit" class="btn btn-success">Select</button></form></td>
		</tr>
<?php
		$i=$i+1;
		 
    }
	} else {
    echo "0 results";
	}
	$conn->close();
?>
   
  </tbody>
</table>
 </div>
<script src="js/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script>
	$('body').scrollspy({ target: '#navbar-example' });

	$('[data-toggle="popover"]').popover();

	$(".navbar-collapse ul li a[href^='#']").on('click',function(e){

		target = this.hash;
		e.preventDefault();

		$('html,body').animate({
			scrollTop : $(this.hash).offset().top
		}, 600, function(){
			window.location.hash = target;
		});

	});

</script>

</body>
</html>
