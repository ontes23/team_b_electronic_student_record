<?php
include 'email_manager.php';

session_start();

$conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");
		if (mysqli_connect_errno()) {
			//echo "Connessione fallita: ".
			mysqli_connect_error();
			exit();
		}
	
function check_duplication($conn,$email){
	$sql = "SELECT * from parents where email = '".$email."';";
	if(!$result =mysqli_query($conn,$sql)) {
						$msg = "Errore nell’inserimento del post, riprovare";
						die();
					}
	$nummails = mysqli_num_rows($result);
					if ($nummails >0 ) {
						return true;
					}
					return false;
}
if (isset($_POST['addParent'])){ /*if the user click the add parent buttom */
	$parentSSN =$_POST['parentSSN'];
	$parentName =$_POST['parentName'];
	$parentSurname =$_POST['parentSurname'];
	$parentEmail =$_POST['parentEmail'];
	$parentAdress =$_POST['parentAdress'];
	$parentPhone =$_POST['parentPhone'];

	
    	//	Connect to the database
     	$sql = "SELECT ssn FROM parents WHERE ssn=?;";
     	$stmt = mysqli_stmt_init($conn);
			
     	if (!mysqli_stmt_prepare($stmt,$sql)) 
     	{
     
     	 exit();
     	} else {
				mysqli_stmt_bind_param($stmt, "s",$parentSSN); //s string we are passing string
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt); //stroes variables from databases
				$resultCheck = mysqli_stmt_num_rows($stmt);
				if($resultCheck > 0) {
					echo "<span>User already exist!!</span>";
				} else {
					if (check_duplication($conn,$parentEmail) == true){
						 echo "<span>User already exists</span>";
						exit();
					}
					$sql = "INSERT INTO parents(ssn,surname,name,address,homephone,email,password,salt) values(?,?,?,?,?,?,?,?);";
					$stmt = mysqli_stmt_init($conn);
					
					if (!mysqli_stmt_prepare($stmt,$sql)) 
					{
						echo "err<br>";
						
						header("Location: signup.php?error=sqlerror");
						exit();
					} else {	
						$salt = rand(1000, 1000000);
						$clear_psw = rand(10000000, 100000000);
						$psw = hash('sha512',$clear_psw);
						$psw_with_salt = $psw.$salt;
						$psw = hash("sha512",$psw_with_salt);
						mysqli_stmt_bind_param($stmt,"ssssssss", $parentSSN,$parentSurname,$parentName,$parentAdress,$parentPhone,$parentEmail,$psw,$salt); //s string we are passing string
						mysqli_stmt_execute($stmt);
						echo "<span>Success</span><br>";

						$body = "Hello from Team B's ESRMS!<br>Parent: ".$parentName." ".$parentSurname."<br>";
						$body = $body."Username: ".$parentEmail."<br>";
						$body = $body."Password: ".$clear_psw."<br>";
					
						
						send_mail($parentEmail, $body);
					}
				}
     	}
} else {
 	 echo "<span>Error</span>";
} 

?>