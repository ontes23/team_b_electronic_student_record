<?php
define ("TEAMB","teamb");

if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
$conn = mysqli_connect("localhost", TEAMB, TEAMB, "esrms");
if (mysqli_connect_errno()) {
    //echo "Connessione fallita: ".
    mysqli_connect_error();
    exit();
}

if (isset($_POST['addHomework'])){

    $name = $_FILES['file']['name'];
    $type = $_FILES['file']['type'];

    $ssn_t = $_SESSION['user_ssn'];
    $hw_class = $_POST['hw_class'];
    $hw_cid = $_POST['cid'];
    $hw_description = $_POST['hw_description'];
    $hw_deadline = $_POST['hw_deadline'];
    $hw_subject = $_POST['subject'];

    /* Check of valid date (e.g. today is 24/11/2019) */
    $now = time();
    $now_date = date('d-m-Y', $now);
    $now_date = strtotime($now_date);                       // 24/11/2019, 00:00

    $deadline_date = strtotime($hw_deadline);               // [input_date], 00:00

    if($deadline_date <= $now_date){					    // Past deadline
        header("location:teacher_page_homework.php?error=Error: you cannot choose this deadline!");
        die();
    }

    /* Check the constraint of the bond class-teacher-day-subject here!!! */
    $deadline_dayofweek = date('D', strtotime($hw_deadline));
    $deadline_dayofweek_long = date('l', strtotime($hw_deadline));

    $sql = "SELECT day
            FROM timetables
            WHERE cid = '".$hw_cid."'
            AND ssn_t = '".$ssn_t."'
            AND subject = '".$hw_subject."'
            AND day = '".$deadline_dayofweek."'";

    if (!$result = mysqli_query($conn, $sql)) {
        $msg = "Errore nell’inserimento del post, riprovare";
        die($msg);
    }

    if(mysqli_num_rows($result)==0){
        header("location:teacher_page_homework.php?error=Error: class ".$hw_class. " is not scheduled on " .$deadline_dayofweek_long . "!");
        die();
    }
		
		/* Storing the attachment in local folder */

    // Create the folder homeworks/ if it does not exist
    if(!is_dir('homeworks')){
        mkdir(getcwd()."/homeworks", 0777, false);
    }
    // Create the folder homeworks/$hw_cid/ if it does not exist
    if(!is_dir('homeworks/'.$hw_cid)){
        mkdir(getcwd()."/homeworks/".$hw_cid, 0777, false);
    }
    // Create the folder homeworks/$hw_cid/$subject/ if it does not exist
    if(!is_dir('homeworks/'.$hw_cid.'/'.$hw_subject)){
        mkdir(getcwd()."/homeworks/".$hw_cid.'/'.$hw_subject, 0777, false);
    }
    $dstFile = getcwd()."/homeworks/".$hw_cid.'/'.$hw_subject."/".$name;
    move_uploaded_file($_FILES['file']['tmp_name'], $dstFile);


    $emptyErrorClass = false;

    $conn = mysqli_connect("localhost", TEAMB, TEAMB, "esrms");
    if (mysqli_connect_errno()) {
        echo "Connessione fallita: ". mysqli_connect_error();
        exit();
    }



    if(empty($hw_class)){
        header("location:teacher_page_homework.php?error=Error: no class selected!");
        $emptyErrorClass = true;
        die();
    }else if(empty($name)){
        $sql = "SELECT ssn FROM teachers WHERE name=? and surname=?";
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "<span>Error of ssn</span>";
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt, "ss",$_SESSION["name_u"],$_SESSION["surname_u"]); //we are passing string
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if($row = mysqli_fetch_assoc($result)) { //check if we get real thing or empty thing,get
                //get ssn_t
                $ssn_t = $row['ssn'];
            }
        }



        $sql = "INSERT INTO homeworks(description,cid,subject,ssn_t,deadline) VALUES(?,?,?,?,?)"; //for safety
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "<span>Error</span>";
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "sssss",$hw_description, $hw_cid,$hw_subject,$ssn_t,$hw_deadline); //we are passing string
            mysqli_stmt_execute($stmt);
            header("location:teacher_page_homework.php?successful_op=The homework have been successfully inserted");
           

        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);
    }else{
        $hw_subject =$_POST['subject'];
        $data = file_get_contents($_FILES['file']['name']);
        $sql = "SELECT ssn FROM teachers WHERE name=? and surname=?";
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "<span>Error of ssn</span>";
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt, "ss",$_SESSION["name_u"],$_SESSION["surname_u"]); //we are passing string
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if($row = mysqli_fetch_assoc($result)) { //check if we get real thing or empty thing,get
                //get ssn_t
                
                $ssn_t = $row['ssn'];
            }
        }



        $sql = "INSERT INTO homeworks(aname, atype,atachment,description,cid,subject,ssn_t,deadline) VALUES(?,?,?,?,?,?,?,?)"; //for safety
        $stmt = mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
            echo "<span>Error</span>";
            exit();
        }
        else {
            mysqli_stmt_bind_param($stmt, "ssssssss",$name,$type,$data,$hw_description, $hw_cid,$hw_subject,$ssn_t,$hw_deadline); //we are passing string
            mysqli_stmt_execute($stmt);
			
            header("location:teacher_page_homework.php?successful_op=The homework have been successfully inserted with attachment");
           
			
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);





    }
}else{
    header("location:teacher_page_homework.php?error=Error: the size of the attachment is greater than 8 MB!");
    die();

}
?>
