<?php

define("TEST","during_test");
include("sidebars.php");
if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
global $class_id;
if(isset($_GET['class'])) {
    $class_id = $_GET['class'];
}
if(isset($_GET['subject'])) {
    $subject = $_GET['subject'];
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Parent Timetable</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/timetable_week.css" rel="stylesheet">



    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Parent Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-md-block bg-light sidebar">
            <div class="small_screen" >
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
					<div class="detail">
							<h4><?php echo $_SESSION["name_s"]." ".$_SESSION["surname_s"]; ?></h4>
							<small><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></small>
					</div>
                        </div>
                    </li>
                    <?php
							if(!isset($_SESSION[TEST])){
									parent_print_sidebar("true");
							}
							?>

                </ul>

            </div>
        </nav>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="pt-3 pb-2 mb-3 border-bottom">
                <h2>Timetable</h2>






            </div>


            <table class="timetable_week">
			<caption></caption>
                <thead>
                <?php
                $weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                $weekdays_shorthand = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                $hours = ["08:00","09:00","10:00","11:00","12:00","13:00"];
                echo "<th>";
                for($i=0; $i<count($weekdays); $i++){
                    ?><th id ='col'><?php echo $weekdays[$i]; ?></th><?php
                }
                ?>
                </thead>
                <tbody>
                <?php
                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                $sql = "SELECT cid
                        FROM bridge_class_students
                        WHERE ssn_s = '". $_SESSION['ssn_s'] . "'";
             
                if (!$result = mysqli_query($conn, $sql)) {
                    $msg = "Errore nell’inserimento del post, riprovare";
                    die($msg);
                }
                while ($row = $result->fetch_assoc()){
                    $cid = $row['cid'];
                }
                
                $sql = "SELECT *
                        FROM timetables t, class c, teachers ts
                        WHERE t.cid = c.cid
                        AND t.ssn_t = ts.ssn
                        AND t.cid = '". $cid ."'
                        ORDER BY t.hour, t.day";
                if (!$result = mysqli_query($conn, $sql)) {
                    $msg = "Errore nell’inserimento del post, riprovare";
                    die($msg);
                }
						if(isset($_SESSION[TEST]) && $_SESSION[TEST]=='1'){
								
									$temp = mysqli_num_rows($result);
							
							}
                $rows = array();

                while ($row = $result->fetch_assoc()) {
                   
                    array_push($rows, $row);
                }

                $k = 0;
                for($i=0; $i<6; $i++){
                    echo "<tr><td><strong>".$hours[$i]."</strong></td>";
                    for($j=0; $j<6; $j++){
                        echo "<td";
                       
                        if($k < count($rows) && $rows[$k]['day']==$weekdays_shorthand[$j] && $rows[$k]['hour']==$hours[$i]){
                            echo " class='lesson'><strong>". $rows[$k]['subject'] . "</strong></td>";
                            $k++;
                        }
                        else{
                            echo "></td>";
                        }
                    }
                }


                ?>
                </tbody>
            </table>

        </main>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="js/dashboard.js"></script></body>
</html>
