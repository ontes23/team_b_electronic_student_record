<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    
    
    <title>Administrative Officer Page</title>

<?php
    include("sidebars.php");
    session_start();

?>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    
    <!-- Main CSS-->
    <link href="css/add_parent_form.css" rel="stylesheet" media="all">
     <link href="css/errors.css" rel="stylesheet" media="all">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <script>

 

  $(document).ready(function() { 
	  $("form").submit(function(event) {
		  var parentSSNLen  = $("#parentSSN").val().length;
		  var parentNameLen = $("#parentName").val().length;
		  var parentSurnameLen  = $("#parentSurname").val().length;
		  var parentEmailLen = $("#parentEmail").val().length;
		  var parentAdressLen  = $("#parentAdress").val().length;
		  var parentPhoneLen = $("#parentPhone").val().length;
	     
	      
	      var parentSSN = $("#parentSSN").val();
	      var parentName = $("#parentName").val();
	      var parentSurname  = $("#parentSurname").val();
	      var parentEmail = $("#parentEmail").val();
	      var parentAdress = $("#parentAdress").val();
	      var parentPhone  = $("#parentPhone").val();
	      var addParent = $("#addParent").val();

      event.preventDefault(); //break the signuppost,move on there 
      
      if(parentSSNLen == 0  || parentNameLen == 0 || parentSurnameLen == 0 || parentEmailLen == 0 ){
          $(".message").text("Please fill the form!");
          $(".message").addClass("error-message");
          
        }

      else{
      $(".message").load("add_parent.php", { 
    	  parentSSN : parentSSN, 
    	  parentName: parentName,
    	  parentSurname: parentSurname,
    	  parentEmail : parentEmail, 
    	  parentAdress: parentAdress,
    	  parentPhone: parentPhone,
    	  addParent : addParent
          });
      }
      
	  }); //when you click the login button
  });
  

  
  
  
  </script> 
  <noscript> Sorry: Your browser does not support or has disabled javascript </noscript>
 
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
  <a class="navbar-brand" href="#">Admin Officer Account</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
			</li>
		</ul>
	</div>
</nav>

<div>

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-md-block bg-light sidebar">
      <div class="small_screen" >
        <ul class="nav flex-column">
		 <li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></h4>
                            </div>
                        </div>
           </li>
            <?php
                administrator_print_sidebar("true");
            ?>
        </ul>

      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
     <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Parent Registration Form</h2>
                </div>
                <div class="card-body">
                    <form action="add_parent.php" method="post">
                       
                        <div class="form-row">
                            <div class="name" >Name:*</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" id="parentName">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Surname:*</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" id="parentSurname">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Address:</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5"  id="parentAdress">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Email:*</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="email" id="parentEmail">
                                </div>
                            </div>
                        </div>
                         <div class="form-row m-b-55">
                            <div class="name">SSN:*</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="parentSSN">
                                            <label class="label--desc">SSN Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Phone:</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" id="parentPhone">
                                            <label class="label--desc">Phone Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
						<h4 style="min-height: 50px;" class= "message"></h4>
                            <button id="addParent" class="btn btn-dark" type="submit">Submit</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
   
     
    </main>
  </div>
</div>

<!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>



      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script></body>
</html>
