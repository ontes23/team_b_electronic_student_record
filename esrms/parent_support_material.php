<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if($_SESSION["test_in_action"]!='1'){
    include("sidebars.php");
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>ESRMS SYSTEM</title>



    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    <link href="css/parent_homeworks.css" rel="stylesheet">
    <link href='css/timetable.css' rel='stylesheet' />
    <link href='css/timetablegrid.css' rel='stylesheet' />
    <script src='js/daygrid.js'></script>




    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        #calendar {
            width: auto;
            height: 800px;
            max-width: 1000px;
            margin: 0 auto;
        }


    </style>

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
</head>
<body>


<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="#">Parent Account</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item align-left">
            <button type="button" class="btn btn-danger" onclick="location.href='logout_post.php';">Sign out</button>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        
				<nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
					<li class="nav-item">
                        <div class="user-info">
                            <div class="image"><img src="photos/user.png" alt="User"></div>
                            <div class="detail">
                                <h4><?php echo $_SESSION["name_s"]." ".$_SESSION["surname_s"]; ?></h4>
                                <small><?php echo $_SESSION["name_u"] ." ". $_SESSION["surname_u"]; ?></small>
                            </div>
                        </div>
          </li>
					<?php
						if($_SESSION["test_in_action"]!='1'){
							parent_print_sidebar("true");
						}
			
							
					?>
        </ul>
      </div>
    </nav>
		
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

            <p id="error" style="font-weight: bold; color: red;">
                <?php
                if(isset($_GET['error'])){
                    echo $_GET['error'];
                }
                ?>
            </p>
            <h2>Support Material</h2>
						
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php

                $ssn_child = $_SESSION['ssn_s'];
                $conn = mysqli_connect("localhost", "teamb", "teamb", "esrms");

                $sql = "SELECT * FROM bridge_class_students WHERE ssn_s = '".$ssn_child."'  ";
                if(!$result = mysqli_query($conn,$sql)) {
                    $msg = "Errore nell’inserimento del post, riprovare";
                }
                $temp = mysqli_num_rows($result);
								
								
								if($temp!=1){
									echo "error in selection of class";
								}


                $row = $result->fetch_assoc();
                $cid = $row["cid"];								# select the classId
								
                
								$sql = "SELECT * FROM bridge_class_teachers WHERE cid = '".$cid."'  ";


								if(!$result2 = mysqli_query($conn,$sql)) {
										$msg = "Errore nell’inserimento del post, riprovare";
								}
								$collapseId = "1";
								while($row =  $result2->fetch_assoc()){
									$subject = $row["subject"];
									# print result of query print_r(row);
									# retrieve document
									
									echo" <div class='panel panel-default'>
													<div class='panel-heading' role='tab' id='headingOne'>
															<h4 id='panel1' class='panel-title'>
																	<a  id='titlee' data-toggle='collapse' data-parent='#accordion' href='#collapse".$collapseId."' aria-expanded='true' aria-controls='collapseOne'>
															$subject
																	</a>
															</h4>
													</div>
													<div id='collapse".$collapseId."' class='panel-collapse collapse in' role='tabpanel' aria-labelledby='headingOne'>
															<div class='panel-body'>
																	<table class='table table-striped table-sm'>";
																	
									$doc = "SELECT * FROM materials WHERE cid='".$cid."' AND subject='".$subject."' ORDER BY date DESC;";
									if(!$res_doc = mysqli_query($conn,$doc)) {
										$msg = "Error retrieve document";
									}
									if ( mysqli_num_rows($res_doc) == 0){
										echo "<p style='color:red;'>No material present</p>";
									}else{
                                        echo "<table class='table table-striped table-sm'>";
                                        echo" <thead>
					            <tr>
					              <th>Date</th>
					              <th>Attachment</th>
					            </tr>
					          </thead>";
                                        echo "<tbody>" ;
										while($supp_doc =  $res_doc->fetch_assoc()){


                                            echo "<tr>";
                                            echo"<td>".$supp_doc['date'] ."</td>";
                                            echo"<td><a target='_blank' href='download_material.php?id=".$supp_doc['aid']."'>".$supp_doc['aname']."</a></td>";
                                            echo"  </tbody>";
																	
										}
									}										
									echo "					</table>
															</div>
													</div>
												</div>";
									$collapseId++;
								}
                  /**/


                


                ?>


            </div>
						
						 
						

        </main>

	</div>
</div>




        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="js/dashboard.js"></script>
</body>
</html>
