-- [PARENT] Read a child's marks, starting from their name and surname
SELECT 
FROM marks
WHERE ssn_s = (SELECT ssn
               FROM students
               WHERE surname = "Buffo"
               AND name = "Matteo");


-- [PARENT] Read a child's marks, starting from their SSN
SELECT 
FROM marks
WHERE ssn_s = /*SSN passed as parameter*/;    


-- [PARENT] Read who is/are my child/children
SELECT s.name, s.surname
FROM students s, parents p, bridge_parents_students bps
WHERE p.ssn = bps.ssn_p
AND s.ssn = bps.ssn_s;