-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 14, 2020 alle 15:36
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esrms`
--
CREATE DATABASE IF NOT EXISTS `esrms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `esrms`;

-- --------------------------------------------------------

--
-- Struttura della tabella `absences_presences`
--

DROP TABLE IF EXISTS `absences_presences`;
CREATE TABLE `absences_presences` (
  `abs_id` int(11) NOT NULL,
  `ssn_s` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(64) NOT NULL,
  `state` varchar(16) NOT NULL,
  `description` varchar(64) NOT NULL,
  `ssn_t` varchar(255) NOT NULL,
  `cid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `absences_presences`
--

INSERT INTO `absences_presences` (`abs_id`, `ssn_s`, `date`, `time`, `state`, `description`, `ssn_t`, `cid`) VALUES
(1, '1AAAAAAAAAAAAAAA', '2020-01-09', '08:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(2, '1AAAAAAAAAAAAAAA', '2020-01-09', '09:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(3, '1AAAAAAAAAAAAAAA', '2020-01-09', '10:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(4, '1AAAAAAAAAAAAAAA', '2020-01-09', '11:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(5, '1AAAAAAAAAAAAAAA', '2020-01-09', '12:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(6, '1AAAAAAAAAAAAAAA', '2020-01-09', '13:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(7, '1AAAAAAAAAAAAAAA', '2020-01-09', '14:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(8, '1AAAAAAAAAAAAAAA', '2020-01-02', '08:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(9, '1AAAAAAAAAAAAAAA', '2020-01-02', '09:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(10, '1AAAAAAAAAAAAAAA', '2020-01-02', '10:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(11, '1AAAAAAAAAAAAAAA', '2020-01-02', '11:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(12, '1AAAAAAAAAAAAAAA', '2020-01-02', '12:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(13, '1AAAAAAAAAAAAAAA', '2020-01-02', '13:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(14, '1AAAAAAAAAAAAAAA', '2020-01-02', '14:00', 'A', 'Absent', '3AAAAAAAAAAAAAAA', 'C1'),
(15, '1AAAAAAAAAAAAAAA', '2020-01-07', '08:00', 'A', '-', '3AAAAAAAAAAAAAAA', 'C1'),
(16, '1AAAAAAAAAAAAAAA', '2020-01-07', '09:00', 'A', '-', '3AAAAAAAAAAAAAAA', 'C1'),
(17, '1AAAAAAAAAAAAAAA', '2020-01-07', '10:00', 'A', '-', '3AAAAAAAAAAAAAAA', 'C1'),
(18, '1AAAAAAAAAAAAAAA', '2020-01-07', '11:00', 'A', '-', '3AAAAAAAAAAAAAAA', 'C1'),
(19, '1AAAAAAAAAAAAAAA', '2020-01-07', '12:00', 'PI', 'Enter late', '3AAAAAAAAAAAAAAA', 'C1'),
(20, '1AAAAAAAAAAAAAAA', '2019-12-27', '12:00', 'PO', 'exited early for flu', '3AAAAAAAAAAAAAAA', 'C1'),
(21, '1AAAAAAAAAAAAAAA', '2019-12-27', '13:00', 'A', '-', '3AAAAAAAAAAAAAAA', 'C1'),
(22, '1AAAAAAAAAAAAAAA', '2019-12-27', '14:00', 'A', '-', '3AAAAAAAAAAAAAAA', 'C1');

-- --------------------------------------------------------

--
-- Struttura della tabella `administrators`
--

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `ssn` varchar(16) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(16) DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `administrators`
--

INSERT INTO `administrators` (`ssn`, `name`, `surname`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('4AAAAAAAAAAAAAAA', 'Lucia', 'Guiso', 'Corso Cristoforo Colombo 12', NULL, NULL, 'adminA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_class_coordinator`
--

DROP TABLE IF EXISTS `bridge_class_coordinator`;
CREATE TABLE `bridge_class_coordinator` (
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_class_coordinator`
--

INSERT INTO `bridge_class_coordinator` (`cid`, `ssn_t`) VALUES
('C1', '3AAAAAAAAAAAAAAA');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_class_students`
--

DROP TABLE IF EXISTS `bridge_class_students`;
CREATE TABLE `bridge_class_students` (
  `cid` varchar(64) NOT NULL,
  `ssn_s` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_class_students`
--

INSERT INTO `bridge_class_students` (`cid`, `ssn_s`) VALUES
('C1', '1AAAAAAAAAAAAAAA'),
('C1', '1CCCCCCCCCCCCCCC'),
('C1', '1DDDDDDDDDDDDDDD'),
('C1', '1EEEEEEEEEEEEEEE'),
('C2', '1BBBBBBBBBBBBBBB'),
('C3', '1FFFFFFFFFFFFFFF');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_class_teachers`
--

DROP TABLE IF EXISTS `bridge_class_teachers`;
CREATE TABLE `bridge_class_teachers` (
  `ssn_t` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_class_teachers`
--

INSERT INTO `bridge_class_teachers` (`ssn_t`, `cid`, `subject`) VALUES
('3AAAAAAAAAAAAAAA', 'C1', 'matematica'),
('3AAAAAAAAAAAAAAA', 'C2', 'fisica'),
('3AAAAAAAAAAAAAAA', 'C3', 'apa'),
('3BBBBBBBBBBBBBBB', 'C2', 'filosofia'),
('3BBBBBBBBBBBBBBB', 'C1', 'italiano'),
('3CCCCCCCCCCCCCC', 'C1', 'storia'),
('3DDDDDDDDDDDDDD', 'C1', 'fisica'),
('3EEEEEEEEEEEEEE', 'C1', 'arte'),
('3FFFFFFFFFFFFFF', 'C1', 'diritto'),
('123456', 'C1', 'disegno');

-- --------------------------------------------------------

--
-- Struttura della tabella `bridge_parents_students`
--

DROP TABLE IF EXISTS `bridge_parents_students`;
CREATE TABLE `bridge_parents_students` (
  `ssn_p` varchar(16) NOT NULL,
  `ssn_s` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `bridge_parents_students`
--

INSERT INTO `bridge_parents_students` (`ssn_p`, `ssn_s`) VALUES
('2AAAAAAAAAAAAAAA', '1AAAAAAAAAAAAAAA'),
('2AAAAAAAAAAAAAAA', '1BBBBBBBBBBBBBBB'),
('2AAAAAAAAAAAAAAA', '1DDDDDDDDDDDDDDD');

-- --------------------------------------------------------

--
-- Struttura della tabella `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `cid` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `class`
--

INSERT INTO `class` (`cid`, `name`) VALUES
('C1', '1A'),
('C2', '3B'),
('C3', '5C');

-- --------------------------------------------------------

--
-- Struttura della tabella `communications`
--

DROP TABLE IF EXISTS `communications`;
CREATE TABLE `communications` (
  `cid` varchar(64) NOT NULL,
  `ssn_a` varchar(64) NOT NULL,
  `object` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `publication_date` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `final_marks`
--

DROP TABLE IF EXISTS `final_marks`;
CREATE TABLE `final_marks` (
  `m_id` int(11) NOT NULL,
  `ssn_s` varchar(16) NOT NULL,
  `subject` varchar(32) NOT NULL,
  `mark` int(11) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `term` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `final_marks`
--

INSERT INTO `final_marks` (`m_id`, `ssn_s`, `subject`, `mark`, `ssn_t`, `term`) VALUES
(0, '1DDDDDDDDDDDDDDD', 'arte', 8, '3AAAAAAAAAAAAAAA', 'I'),
(1, '1EEEEEEEEEEEEEEE', 'arte', 4, '3AAAAAAAAAAAAAAA', 'I'),
(2, '1AAAAAAAAAAAAAAA', 'arte', 5, '3AAAAAAAAAAAAAAA', 'I'),
(3, '1CCCCCCCCCCCCCCC', 'arte', 3, '3AAAAAAAAAAAAAAA', 'I'),
(4, '1DDDDDDDDDDDDDDD', 'diritto', 2, '3AAAAAAAAAAAAAAA', 'I'),
(5, '1EEEEEEEEEEEEEEE', 'diritto', 4, '3AAAAAAAAAAAAAAA', 'I'),
(6, '1AAAAAAAAAAAAAAA', 'diritto', 6, '3AAAAAAAAAAAAAAA', 'I'),
(7, '1CCCCCCCCCCCCCCC', 'diritto', 5, '3AAAAAAAAAAAAAAA', 'I'),
(8, '1DDDDDDDDDDDDDDD', 'fisica', 7, '3AAAAAAAAAAAAAAA', 'I'),
(9, '1EEEEEEEEEEEEEEE', 'fisica', 6, '3AAAAAAAAAAAAAAA', 'I'),
(10, '1AAAAAAAAAAAAAAA', 'fisica', 8, '3AAAAAAAAAAAAAAA', 'I'),
(11, '1CCCCCCCCCCCCCCC', 'fisica', 6, '3AAAAAAAAAAAAAAA', 'I'),
(12, '1DDDDDDDDDDDDDDD', 'italiano', 5, '3AAAAAAAAAAAAAAA', 'I'),
(13, '1EEEEEEEEEEEEEEE', 'italiano', 7, '3AAAAAAAAAAAAAAA', 'I'),
(14, '1AAAAAAAAAAAAAAA', 'italiano', 8, '3AAAAAAAAAAAAAAA', 'I'),
(15, '1CCCCCCCCCCCCCCC', 'italiano', 6, '3AAAAAAAAAAAAAAA', 'I'),
(16, '1DDDDDDDDDDDDDDD', 'matematica', 6, '3AAAAAAAAAAAAAAA', 'I'),
(17, '1EEEEEEEEEEEEEEE', 'matematica', 7, '3AAAAAAAAAAAAAAA', 'I'),
(18, '1AAAAAAAAAAAAAAA', 'matematica', 7, '3AAAAAAAAAAAAAAA', 'I'),
(19, '1CCCCCCCCCCCCCCC', 'matematica', 8, '3AAAAAAAAAAAAAAA', 'I'),
(20, '1DDDDDDDDDDDDDDD', 'storia', 10, '3AAAAAAAAAAAAAAA', 'I'),
(21, '1EEEEEEEEEEEEEEE', 'storia', 6, '3AAAAAAAAAAAAAAA', 'I'),
(22, '1AAAAAAAAAAAAAAA', 'storia', 8, '3AAAAAAAAAAAAAAA', 'I'),
(23, '1CCCCCCCCCCCCCCC', 'storia', 10, '3AAAAAAAAAAAAAAA', 'I'),
(32, '1DDDDDDDDDDDDDDD', 'disegno', 2, '3AAAAAAAAAAAAAAA', 'I'),
(33, '1EEEEEEEEEEEEEEE', 'disegno', 4, '3AAAAAAAAAAAAAAA', 'I'),
(34, '1AAAAAAAAAAAAAAA', 'disegno', 6, '3AAAAAAAAAAAAAAA', 'I'),
(35, '1CCCCCCCCCCCCCCC', 'disegno', 5, '3AAAAAAAAAAAAAAA', 'I');

-- --------------------------------------------------------

--
-- Struttura della tabella `homeworks`
--

DROP TABLE IF EXISTS `homeworks`;
CREATE TABLE `homeworks` (
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `aid` int(11) NOT NULL,
  `aname` varchar(255) NOT NULL,
  `atype` varchar(255) NOT NULL,
  `atachment` blob NOT NULL,
  `description` varchar(255) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `deadline` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `homeworks`
--

INSERT INTO `homeworks` (`date`, `aid`, `aname`, `atype`, `atachment`, `description`, `cid`, `subject`, `ssn_t`, `deadline`) VALUES
('2019-12-16 20:05:57', 1, 'integral.pdf', 'application/pdf', '', 'integral exercise 1 added!!!!', 'C1', 'matematica', '3AAAAAAAAAAAAAAA', '2019-12-19'),
('2019-12-16 20:09:17', 2, 'integral-2.docx', 'application/octet-stream', '', 'integral 2 exercises - please do your solution to the paper ', 'C1', 'matematica', '3AAAAAAAAAAAAAAA', '2019-12-23');

-- --------------------------------------------------------

--
-- Struttura della tabella `lessons`
--

DROP TABLE IF EXISTS `lessons`;
CREATE TABLE `lessons` (
  `lid` varchar(64) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(64) NOT NULL,
  `date` varchar(64) NOT NULL,
  `time` varchar(16) NOT NULL,
  `description` varchar(512) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `timestamp` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `marks`
--

DROP TABLE IF EXISTS `marks`;
CREATE TABLE `marks` (
  `m_id` int(11) NOT NULL,
  `ssn_s` varchar(16) NOT NULL,
  `subject` varchar(32) NOT NULL,
  `mark` decimal(4,2) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `materials`
--

DROP TABLE IF EXISTS `materials`;
CREATE TABLE `materials` (
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `aid` int(11) NOT NULL,
  `aname` varchar(255) NOT NULL,
  `atype` varchar(255) NOT NULL,
  `atachment` blob NOT NULL,
  `cid` varchar(64) NOT NULL,
  `subject` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `materials`
--

INSERT INTO `materials` (`date`, `aid`, `aname`, `atype`, `atachment`, `cid`, `subject`, `ssn_t`) VALUES
('2019-12-17 13:01:22', 5, 'topics.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', '', 'C1', 'matematica', '3AAAAAAAAAAAAAAA'),
('2020-01-10 11:23:44', 6, 'README.md', 'application/octet-stream', '', 'C1', 'matematica', '3AAAAAAAAAAAAAAA');

-- --------------------------------------------------------

--
-- Struttura della tabella `meetings`
--

DROP TABLE IF EXISTS `meetings`;
CREATE TABLE `meetings` (
  `metID` int(11) NOT NULL,
  `ssn_t` varchar(64) NOT NULL,
  `hour` varchar(6) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') NOT NULL,
  `p_name` varchar(64) NOT NULL,
  `p_surname` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `meetings`
--

INSERT INTO `meetings` (`metID`, `ssn_t`, `hour`, `day`, `p_name`, `p_surname`) VALUES
(2, '3AAAAAAAAAAAAAAA', '09:00', 'Tuesday', 'Luigi', 'Verdi'),
(3, '3AAAAAAAAAAAAAAA', '10:00', 'Tuesday', '', ''),
(4, '3AAAAAAAAAAAAAAA', '11:00', 'Tuesday', '', ''),
(5, '3AAAAAAAAAAAAAAA', '12:00', 'Tuesday', '', ''),
(8, '3AAAAAAAAAAAAAAA', '11:00', 'Monday', '', ''),
(18, '3BBBBBBBBBBBBBBB', '11:00', 'Tuesday', '', ''),
(19, '3BBBBBBBBBBBBBBB', '11:00', 'Wednesday', '', ''),
(20, '3BBBBBBBBBBBBBBB', '12:00', 'Wednesday', '', ''),
(21, '3BBBBBBBBBBBBBBB', '11:00', 'Friday', '', ''),
(22, '3BBBBBBBBBBBBBBB', '12:00', 'Friday', '', ''),
(23, '3BBBBBBBBBBBBBBB', '13:00', 'Friday', '', ''),
(26, '3AAAAAAAAAAAAAAA', '08:00', 'Wednesday', '', ''),
(27, '3AAAAAAAAAAAAAAA', '09:00', 'Wednesday', '', ''),
(28, '3AAAAAAAAAAAAAAA', '09:00', 'Thursday', 'Mario', 'Rossi'),
(29, '3AAAAAAAAAAAAAAA', '08:00', 'Thursday', 'Mario', 'Rossi');

-- --------------------------------------------------------

--
-- Struttura della tabella `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `note_id` int(11) NOT NULL,
  `ssn_s` varchar(255) NOT NULL,
  `ssn_t` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `date` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notes`
--

INSERT INTO `notes` (`note_id`, `ssn_s`, `ssn_t`, `description`, `date`) VALUES
(1, '1AAAAAAAAAAAAAAA', '3AAAAAAAAAAAAAAA', 'Didn\'t complete the homworks', '2020-01-14'),
(2, '1AAAAAAAAAAAAAAA', '3AAAAAAAAAAAAAAA', 'Bad behaviour in classroom', '2020-01-14'),
(3, '1AAAAAAAAAAAAAAA', '3AAAAAAAAAAAAAAA', 'Screams during classroom', '2020-01-14');

-- --------------------------------------------------------

--
-- Struttura della tabella `parents`
--

DROP TABLE IF EXISTS `parents`;
CREATE TABLE `parents` (
  `ssn` varchar(16) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(16) DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `parents`
--

INSERT INTO `parents` (`ssn`, `surname`, `name`, `address`, `homephone`, `cellphone`, `email`, `password`, `salt`) VALUES
('2AAAAAAAAAAAAAAA', 'Rossi', 'Mario', 'Via Giulio Cesare 21', '0123456789', '0123456789', 'parA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456'),
('2BBBBBBBBBBBBBBB', 'Verdi', 'Luigi', 'Corso Garibaldi 34', '9876543210', '9876543210', 'parB@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456');

-- --------------------------------------------------------

--
-- Struttura della tabella `principals`
--

DROP TABLE IF EXISTS `principals`;
CREATE TABLE `principals` (
  `ssn` varchar(64) NOT NULL,
  `surname` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `homephone` varchar(64) NOT NULL,
  `cellphone` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `ssn` varchar(16) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` varchar(128) NOT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `gender` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `students`
--

INSERT INTO `students` (`ssn`, `surname`, `name`, `address`, `cellphone`, `gender`) VALUES
('1AAAAAAAAAAAAAAA', 'Rossi', 'Alberto', 'Via Giulio Cesare 21', NULL, 'm'),
('1BBBBBBBBBBBBBBB', 'Rossi', 'Angela', 'Via Giulio Cesare 21', NULL, 'f'),
('1CCCCCCCCCCCCCCC', 'Verdi', 'Michele', 'Corso Garibaldi 34', NULL, 'm'),
('1DDDDDDDDDDDDDDD', 'Bianchi', 'Carlo', 'Via Leonardo 102', NULL, 'm'),
('1EEEEEEEEEEEEEEE', 'Bianchi', 'Paolo', '', NULL, 'm'),
('1FFFFFFFFFFFFFFF', 'Neri', 'Marina', '', NULL, 'f');

-- --------------------------------------------------------

--
-- Struttura della tabella `sysadmins`
--

DROP TABLE IF EXISTS `sysadmins`;
CREATE TABLE `sysadmins` (
  `ssn` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `sysadmins`
--

INSERT INTO `sysadmins` (`ssn`, `name`, `surname`, `email`, `password`, `salt`) VALUES
('1A', 'Luca', 'Bitta', 'sysadmin@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', '123456');

-- --------------------------------------------------------

--
-- Struttura della tabella `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `ssn` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(512) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `address` varchar(64) NOT NULL,
  `homephone` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `teachers`
--

INSERT INTO `teachers` (`ssn`, `email`, `password`, `name`, `surname`, `salt`, `address`, `homephone`, `cellphone`) VALUES
('3AAAAAAAAAAAAAAA', 'teachA@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Gianpiero', 'Cabodi', '123456', 'Via Dijkstra 10', '', ''),
('3BBBBBBBBBBBBBBB', 'teachB@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Paolo Enrico', 'Camurati', '123456', 'Via Kernighan 70', '', ''),
('3CCCCCCCCCCCCCC', 'teachC@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Lucio', 'Massa', '123456', 'via asd 25', '123456', '123456'),
('3DDDDDDDDDDDDDD', 'teachD@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Diego', 'Catto', '123456', 'via coseno 314', '123456', '123456'),
('3EEEEEEEEEEEEEE', 'teachE@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Clara', 'Dadda', '123456', 'p.zza Castello 15', '123456', '123456'),
('3FFFFFFFFFFFFFFF', 'teachF@gmail.com', '1dd3ba7ee768823464723e29a4b686b92d82357312c4ba70a340e6bf7b001da947ba6124765bbb8637194ad4a6a457c1829b8b8b4f9a6ed06cc4a38fadce752b', 'Paola', 'Lanza', '123456', 'via qwerty 123', '123', '123');

-- --------------------------------------------------------

--
-- Struttura della tabella `timetables`
--

DROP TABLE IF EXISTS `timetables`;
CREATE TABLE `timetables` (
  `ttid` int(11) NOT NULL,
  `cid` varchar(64) NOT NULL,
  `ssn_t` varchar(16) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `hour` varchar(6) NOT NULL,
  `day` enum('Mon','Tue','Wed','Thu','Fri','Sat') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `timetables`
--

INSERT INTO `timetables` (`ttid`, `cid`, `ssn_t`, `subject`, `hour`, `day`) VALUES
(1, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Mon'),
(2, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Mon'),
(3, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Mon'),
(4, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Mon'),
(5, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Mon'),
(6, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Mon'),
(7, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Tue'),
(8, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Tue'),
(9, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Tue'),
(10, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Tue'),
(11, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Tue'),
(12, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Tue'),
(13, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Wed'),
(14, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Wed'),
(15, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Wed'),
(16, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Wed'),
(17, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Wed'),
(18, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Wed'),
(19, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Thu'),
(20, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Thu'),
(21, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Thu'),
(22, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Thu'),
(23, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Thu'),
(24, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Thu'),
(25, 'C1', '3FFFFFFFFFFFFFFF', 'diritto', '08:00', 'Fri'),
(26, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '09:00', 'Fri'),
(27, 'C1', '3BBBBBBBBBBBBBBB', 'italiano', '10:00', 'Fri'),
(28, 'C1', '3CCCCCCCCCCCCCC', 'storia', '11:00', 'Fri'),
(29, 'C1', '3DDDDDDDDDDDDDD', 'fisica', '12:00', 'Fri'),
(30, 'C1', '3AAAAAAAAAAAAAAA', 'matematica', '13:00', 'Fri'),
(31, 'C2', '3FFFFFFFFFFFFFFF', 'diritto', '13:00', 'Mon'),
(32, 'C2', '3BBBBBBBBBBBBBBB', 'italiano', '11:00', 'Mon'),
(33, 'C2', '3BBBBBBBBBBBBBBB', 'italiano', '12:00', 'Mon'),
(34, 'C2', '3CCCCCCCCCCCCCC', 'storia', '09:00', 'Mon'),
(35, 'C2', '3DDDDDDDDDDDDDD', 'fisica', '10:00', 'Mon'),
(36, 'C2', '3AAAAAAAAAAAAAAA', 'fisica', '08:00', 'Mon');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `absences_presences`
--
ALTER TABLE `absences_presences`
  ADD PRIMARY KEY (`abs_id`);

--
-- Indici per le tabelle `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `bridge_class_students`
--
ALTER TABLE `bridge_class_students`
  ADD PRIMARY KEY (`cid`,`ssn_s`),
  ADD KEY `ssn_s` (`ssn_s`);

--
-- Indici per le tabelle `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `cid` (`cid`);

--
-- Indici per le tabelle `final_marks`
--
ALTER TABLE `final_marks`
  ADD PRIMARY KEY (`m_id`);

--
-- Indici per le tabelle `homeworks`
--
ALTER TABLE `homeworks`
  ADD PRIMARY KEY (`aid`);

--
-- Indici per le tabelle `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`lid`);

--
-- Indici per le tabelle `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`m_id`);

--
-- Indici per le tabelle `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`aid`);

--
-- Indici per le tabelle `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`metID`);

--
-- Indici per le tabelle `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indici per le tabelle `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `principals`
--
ALTER TABLE `principals`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `sysadmins`
--
ALTER TABLE `sysadmins`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`ssn`);

--
-- Indici per le tabelle `timetables`
--
ALTER TABLE `timetables`
  ADD PRIMARY KEY (`ttid`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `homeworks`
--
ALTER TABLE `homeworks`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `materials`
--
ALTER TABLE `materials`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `meetings`
--
ALTER TABLE `meetings`
  MODIFY `metID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT per la tabella `timetables`
--
ALTER TABLE `timetables`
  MODIFY `ttid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1036;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
